from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from plugins.base.utils import init_plugins, update_plugins_urls


init_plugins()


urlpatterns = patterns(
    '',
    url(r'^', include('apps.accounts.urls')),
    url(r'^', include('apps.dashboard.urls')),
    url(r'^hosts/', include('apps.hosts.urls')),
    url(r'^applications/', include('apps.applications.urls')),
    url(r'^sshkeys/', include('apps.sshkeys.urls')),
    url(r'^plugins/', include('plugins.base.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url('^robots\.txt$', TemplateView.as_view(
        template_name='robots.txt', content_type='text/plain')),
    url('^humans\.txt$', TemplateView.as_view(
        template_name='humans.txt', content_type='text/plain')),
)

# Load plugins urls
urlpatterns = update_plugins_urls(urlpatterns)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url('^demo-email\.html$', TemplateView.as_view(
            template_name='email/base.html')),
        url('^demo-email\.txt$', TemplateView.as_view(
            template_name='email/base.txt', content_type='text/plain')),
    )

    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA

    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
