'use strict';

$(function() {
    $(document).on("click", ".__action__confirm", function() {
        var confirmation_text = $(this).data("confirmation-text");
        if(confirm(confirmation_text)) {
            return true;
        } else {
            return false;
        }
    });
});
