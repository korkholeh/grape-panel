# Basic settings for cranelocator project
import os
import dj_database_url
from django.utils.crypto import get_random_string


gettext = lambda s: s
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


SECRET_KEY = os.environ.get(
    "SECRET_KEY",
    get_random_string(
        50, "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)"))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'flat',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'crispy_forms',
    'easy_select2',
    'codemirror',
    'djcelery',

    'apps.core',
    'apps.dashboard',
    'apps.passwords',
    'apps.accounts',
    'apps.sshkeys',
    'apps.hosts',
    'apps.containers',
    'apps.applications',

    'plugins.base',
    'plugins.haproxy',
    'plugins.postgresql',
    'plugins.envvariables',
    'plugins.passenger',
    'plugins.cmdtop',
    'plugins.nginxconfig',
    'plugins.linkedftps',
    'plugins.backups',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'apps.core.middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'grpanel.urls'

WSGI_APPLICATION = 'grpanel.wsgi.application'


DATABASES = {
    'default': dj_database_url.config(default=os.environ['DATABASE_URL']),
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Amsterdam'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', gettext('en')),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'grpanel', 'static'),
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'grpanel', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
            ],
        },
    },
]


AUTH_USER_MODEL = 'accounts.User'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

DEFAULT_FROM_EMAIL = 'notify@grape-project.com'
INVITATION_INVITE_ONLY = True

CRISPY_TEMPLATE_PACK = 'bootstrap3'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

SELECT2_USE_BUNDLED_JQUERY = False


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

PROJECT_NAME = gettext('Grape Panel')
PROJECT_DOMAIN = os.environ.get('PROJECT_DOMAIN', 'http://localhost:8000')

CODEMIRROR_PATH = 'vendor/codemirror'
CODEMIRROR_THEME = 'default'

# Set here the ssh key from the your host panel container
MAINTAINABLE_SSH_KEY = os.environ.get(
    'MAINTAINABLE_SSH_KEY', '')
MAINTAINABLE_SSH_KEY_FILE_PATH = os.environ.get(
    'MAINTAINABLE_SSH_KEY_FILE_PATH',
    'id_dsa.pub')
HOST_PANEL_DOMAIN = os.environ.get(
    'HOST_PANEL_DOMAIN',
    'http://localhost:8000')

BROKER_URL = os.environ.get(
    'CELERY_BROKER_URL',
    'redis://localhost:6379/0')
CELERY_RESULT_BACKEND = os.environ.get(
    'CELERY_RESULT_BACKEND',
    'redis://localhost:6379/0')
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

INSTALLED_PLUGINS = (
    'plugins.haproxy.plugin.HAProxyPlugin',
    'plugins.postgresql.plugin.PostgreSQLPlugin',
    'plugins.envvariables.plugin.EnvVariablesPlugin',
    'plugins.passenger.plugin.PassengerPlugin',
    'plugins.cmdtop.plugin.CmdTopPlugin',
    'plugins.nginxconfig.plugin.NginxConfigPlugin',
    'plugins.linkedftps.plugin.LinkedFTPsPlugin',
    'plugins.backups.plugin.BackupPlugin',
)

# Redis connection settings
GRAPE_REDIS_HOST = os.environ.get('GRAPE_REDIS_HOST', 'localhost')
GRAPE_REDIS_PORT = os.environ.get('GRAPE_REDIS_PORT', '6379')
GRAPE_REDIS_DB = os.environ.get('GRAPE_REDIS_DB', '1')

# Docker API client certificates

DOCKER_TLS_DIR = os.path.join(BASE_DIR, 'cert')
DOCKER_TLS_CA = os.environ.get(
    'DOCKER_TLS_CA', os.path.join(BASE_DIR, 'cert', 'ca.pem'))
DOCKER_TLS_KEY = os.environ.get(
    'DOCKER_TLS_KEY', os.path.join(BASE_DIR, 'cert', 'key.pem'))
DOCKER_TLS_CERT = os.environ.get(
    'DOCKER_TLS_CERT', os.path.join(BASE_DIR, 'cert', 'cert.pem'))
DOCKER_API_VERSION = '1.19'


CELERY_TIMEZONE = TIME_ZONE
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
