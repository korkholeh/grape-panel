# Settings for development environment
from .base import *

DEBUG = False

ALLOWED_HOSTS = (
    '*',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

MEDIA_ROOT = '/home/app/webapp/public/media'
STATIC_ROOT = '/home/app/webapp/public/static'
