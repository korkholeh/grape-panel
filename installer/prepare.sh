
#!/bin/bash
echo "Save manageable key"
mkdir ~/.ssh && chmod 700 ~/.ssh
touch ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys
echo "" | tee -a ~/.ssh/authorized_keys

DOCKER_HOST="127.0.0.1"

echo "Prepare the basic system"

sudo add-apt-repository -y ppa:duplicity-team/ppa
sudo apt-get -qq update
sudo apt-get -y upgrade
sudo apt-get -y install mc htop openssh-server git postgresql-client-common
sudo apt-get -y install postgresql-client python-software-properties
sudo apt-get -y install build-essential redis-tools apache2-utils
sudo apt-get -y install python-pip
sudo apt-get -y install duplicity duply
sudo apt-get -y install linux-headers-$(uname -r)
sudo pip install boto

#sudo apt-get -y install --no-install-recommends virtualbox-guest-utils
#sudo apt-get -y install virtualbox-guest-dkms

echo "Install Docker"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sudo sh -c "echo deb http://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
sudo apt-get -qq update
sudo apt-get -y install lxc-docker cgroup-lite
sudo gpasswd -a webprod docker
sudo service docker restart

echo "Install FTP"

sudo apt-get install vsftpd libpam-pwdfile
sudo mv /etc/vsftpd.conf /etc/vsftpd.conf.bak

sudo cat << EOT > /etc/vsftpd.conf
anonymous_enable=NO
local_enable=YES
chroot_local_user=YES
user_config_dir=/etc/vsftpd/vsftpd-virtual-user/
virtual_use_local_privs=YES
dual_log_enable=YES
connect_from_port_20=YES
listen=YES
pam_service_name=ftp
tcp_wrappers=YES
allow_writeable_chroot=YES
EOT

sudo restart vsftpd

sudo mkdir -p /etc/vsftpd/
sudo mkdir -p /etc/vsftpd/vsftpd-virtual-user/
sudo touch /etc/vsftpd/vsftpd-virtual-user/vsftpd_user

echo "Setup PAM"

sudo cp /etc/pam.d/vsftpd /etc/pam.d/vsftpd.bak
sudo cat << EOT > /etc/pam.d/vsftpd
session optional        pam_keyinit.so  force   revoke
auth   required        pam_listfile.so item=user sense=deny file=/etc/ftpusers onerr=succeed
auth   required        pam_shells.so
auth    include system-auth
account include system-auth
session include system-auth
session required pam_loginuid.so
EOT

echo "Prepare directories"

sudo mkdir -p /home/webprod/src
sudo mkdir -p /home/webprod/tmp
sudo mkdir -p /home/webprod/backups
sudo mkdir -p /home/webprod/cert
sudo chown webprod:webprod /home/webprod/src
sudo chown webprod:webprod /home/webprod/tmp
sudo chown webprod:webprod /home/webprod/backups
sudo chown webprod:webprod /home/webprod/cert

echo "Setup complete. Good luck!"
