BEGIN TRANSACTION;
CREATE TABLE "accounts_user" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "password" varchar(128) NOT NULL, "last_login" datetime NULL, "is_superuser" bool NOT NULL, "email" varchar(255) NOT NULL UNIQUE, "email_verification_code" varchar(255) NOT NULL, "full_name" varchar(100) NOT NULL, "is_staff" bool NOT NULL, "is_active" bool NOT NULL, "date_joined" datetime NOT NULL);
INSERT INTO accounts_user VALUES(1,'pbkdf2_sha256$20000$lcEKkB3Rxsm7$Vt5xwSF+cOZgELwdpysDN/jRy/3ELQuKSkZwldQvDI0=','2015-10-02 13:22:43',1,'demo@grape-project.com','VERIFIED','Demo Admin',1,1,'2015-08-12 09:19:47');
CREATE TABLE "accounts_user_groups" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "group_id" integer NOT NULL REFERENCES "auth_group" ("id"), UNIQUE ("user_id", "group_id"));
CREATE TABLE "accounts_user_user_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"), UNIQUE ("user_id", "permission_id"));
CREATE TABLE "applications_webapplication" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "short_description" varchar(300) NOT NULL, "name" varchar(100) NOT NULL UNIQUE, "added" datetime NOT NULL, "public_app_port" integer unsigned NULL, "public_ssh_port" integer unsigned NULL, "docker_container_id" integer NULL UNIQUE REFERENCES "containers_container" ("id"), "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"), "owner_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "active" bool NOT NULL, "docker_image_id" integer NULL REFERENCES "containers_dockerimage" ("id"));
CREATE TABLE "auth_group" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(80) NOT NULL UNIQUE);
CREATE TABLE "auth_group_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "group_id" integer NOT NULL REFERENCES "auth_group" ("id"), "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"), UNIQUE ("group_id", "permission_id"));
CREATE TABLE "auth_permission" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"), "codename" varchar(100) NOT NULL, "name" varchar(255) NOT NULL, UNIQUE ("content_type_id", "codename"));
INSERT INTO auth_permission VALUES(1,1,'add_logentry','Can add log entry');
INSERT INTO auth_permission VALUES(2,1,'change_logentry','Can change log entry');
INSERT INTO auth_permission VALUES(3,1,'delete_logentry','Can delete log entry');
INSERT INTO auth_permission VALUES(4,2,'add_permission','Can add permission');
INSERT INTO auth_permission VALUES(5,2,'change_permission','Can change permission');
INSERT INTO auth_permission VALUES(6,2,'delete_permission','Can delete permission');
INSERT INTO auth_permission VALUES(7,3,'add_group','Can add group');
INSERT INTO auth_permission VALUES(8,3,'change_group','Can change group');
INSERT INTO auth_permission VALUES(9,3,'delete_group','Can delete group');
INSERT INTO auth_permission VALUES(10,4,'add_contenttype','Can add content type');
INSERT INTO auth_permission VALUES(11,4,'change_contenttype','Can change content type');
INSERT INTO auth_permission VALUES(12,4,'delete_contenttype','Can delete content type');
INSERT INTO auth_permission VALUES(13,5,'add_session','Can add session');
INSERT INTO auth_permission VALUES(14,5,'change_session','Can change session');
INSERT INTO auth_permission VALUES(15,5,'delete_session','Can delete session');
INSERT INTO auth_permission VALUES(16,6,'add_related','Can add related');
INSERT INTO auth_permission VALUES(17,6,'change_related','Can change related');
INSERT INTO auth_permission VALUES(18,6,'delete_related','Can delete related');
INSERT INTO auth_permission VALUES(19,7,'add_testfieldsmodel','Can add test fields model');
INSERT INTO auth_permission VALUES(20,7,'change_testfieldsmodel','Can change test fields model');
INSERT INTO auth_permission VALUES(21,7,'delete_testfieldsmodel','Can delete test fields model');
INSERT INTO auth_permission VALUES(22,8,'add_emptymodel','Can add empty model');
INSERT INTO auth_permission VALUES(23,8,'change_emptymodel','Can change empty model');
INSERT INTO auth_permission VALUES(24,8,'delete_emptymodel','Can delete empty model');
INSERT INTO auth_permission VALUES(25,9,'add_user','Can add User');
INSERT INTO auth_permission VALUES(26,9,'change_user','Can change User');
INSERT INTO auth_permission VALUES(27,9,'delete_user','Can delete User');
INSERT INTO auth_permission VALUES(28,10,'add_servertype','Can add Server type');
INSERT INTO auth_permission VALUES(29,10,'change_servertype','Can change Server type');
INSERT INTO auth_permission VALUES(30,10,'delete_servertype','Can delete Server type');
INSERT INTO auth_permission VALUES(31,11,'add_host','Can add Host');
INSERT INTO auth_permission VALUES(32,11,'change_host','Can change Host');
INSERT INTO auth_permission VALUES(33,11,'delete_host','Can delete Host');
INSERT INTO auth_permission VALUES(34,12,'add_sshkey','Can add SSH Key');
INSERT INTO auth_permission VALUES(35,12,'change_sshkey','Can change SSH Key');
INSERT INTO auth_permission VALUES(36,12,'delete_sshkey','Can delete SSH Key');
INSERT INTO auth_permission VALUES(37,13,'add_sshkeyrecord','Can add SSH Key Record');
INSERT INTO auth_permission VALUES(38,13,'change_sshkeyrecord','Can change SSH Key Record');
INSERT INTO auth_permission VALUES(39,13,'delete_sshkeyrecord','Can delete SSH Key Record');
INSERT INTO auth_permission VALUES(40,14,'add_ftpuser','Can add FTP User');
INSERT INTO auth_permission VALUES(41,14,'change_ftpuser','Can change FTP User');
INSERT INTO auth_permission VALUES(42,14,'delete_ftpuser','Can delete FTP User');
INSERT INTO auth_permission VALUES(43,15,'add_dockerimage','Can add Docker image');
INSERT INTO auth_permission VALUES(44,15,'change_dockerimage','Can change Docker image');
INSERT INTO auth_permission VALUES(45,15,'delete_dockerimage','Can delete Docker image');
INSERT INTO auth_permission VALUES(46,16,'add_exposedport','Can add Exposed port');
INSERT INTO auth_permission VALUES(47,16,'change_exposedport','Can change Exposed port');
INSERT INTO auth_permission VALUES(48,16,'delete_exposedport','Can delete Exposed port');
INSERT INTO auth_permission VALUES(49,17,'add_envvariable','Can add Environment variable');
INSERT INTO auth_permission VALUES(50,17,'change_envvariable','Can change Environment variable');
INSERT INTO auth_permission VALUES(51,17,'delete_envvariable','Can delete Environment variable');
INSERT INTO auth_permission VALUES(52,18,'add_imagetag','Can add Image tag');
INSERT INTO auth_permission VALUES(53,18,'change_imagetag','Can change Image tag');
INSERT INTO auth_permission VALUES(54,18,'delete_imagetag','Can delete Image tag');
INSERT INTO auth_permission VALUES(55,19,'add_container','Can add Container');
INSERT INTO auth_permission VALUES(56,19,'change_container','Can change Container');
INSERT INTO auth_permission VALUES(57,19,'delete_container','Can delete Container');
INSERT INTO auth_permission VALUES(58,20,'add_haproxyservice','Can add HAProxy Service');
INSERT INTO auth_permission VALUES(59,20,'change_haproxyservice','Can change HAProxy Service');
INSERT INTO auth_permission VALUES(60,20,'delete_haproxyservice','Can delete HAProxy Service');
INSERT INTO auth_permission VALUES(61,21,'add_pluginstate','Can add Plugin State');
INSERT INTO auth_permission VALUES(62,21,'change_pluginstate','Can change Plugin State');
INSERT INTO auth_permission VALUES(63,21,'delete_pluginstate','Can delete Plugin State');
INSERT INTO auth_permission VALUES(64,22,'add_dockerimagepullinglog','Can add Docker image pulling log');
INSERT INTO auth_permission VALUES(65,22,'change_dockerimagepullinglog','Can change Docker image pulling log');
INSERT INTO auth_permission VALUES(66,22,'delete_dockerimagepullinglog','Can delete Docker image pulling log');
INSERT INTO auth_permission VALUES(67,23,'add_webapplication','Can add Web application');
INSERT INTO auth_permission VALUES(68,23,'change_webapplication','Can change Web application');
INSERT INTO auth_permission VALUES(69,23,'delete_webapplication','Can delete Web application');
INSERT INTO auth_permission VALUES(70,24,'add_domain','Can add Domain');
INSERT INTO auth_permission VALUES(71,24,'change_domain','Can change Domain');
INSERT INTO auth_permission VALUES(72,24,'delete_domain','Can delete Domain');
INSERT INTO auth_permission VALUES(73,25,'add_taskmeta','Can add task state');
INSERT INTO auth_permission VALUES(74,25,'change_taskmeta','Can change task state');
INSERT INTO auth_permission VALUES(75,25,'delete_taskmeta','Can delete task state');
INSERT INTO auth_permission VALUES(76,26,'add_tasksetmeta','Can add saved group result');
INSERT INTO auth_permission VALUES(77,26,'change_tasksetmeta','Can change saved group result');
INSERT INTO auth_permission VALUES(78,26,'delete_tasksetmeta','Can delete saved group result');
INSERT INTO auth_permission VALUES(79,27,'add_intervalschedule','Can add interval');
INSERT INTO auth_permission VALUES(80,27,'change_intervalschedule','Can change interval');
INSERT INTO auth_permission VALUES(81,27,'delete_intervalschedule','Can delete interval');
INSERT INTO auth_permission VALUES(82,28,'add_crontabschedule','Can add crontab');
INSERT INTO auth_permission VALUES(83,28,'change_crontabschedule','Can change crontab');
INSERT INTO auth_permission VALUES(84,28,'delete_crontabschedule','Can delete crontab');
INSERT INTO auth_permission VALUES(85,29,'add_periodictasks','Can add periodic tasks');
INSERT INTO auth_permission VALUES(86,29,'change_periodictasks','Can change periodic tasks');
INSERT INTO auth_permission VALUES(87,29,'delete_periodictasks','Can delete periodic tasks');
INSERT INTO auth_permission VALUES(88,30,'add_periodictask','Can add periodic task');
INSERT INTO auth_permission VALUES(89,30,'change_periodictask','Can change periodic task');
INSERT INTO auth_permission VALUES(90,30,'delete_periodictask','Can delete periodic task');
INSERT INTO auth_permission VALUES(91,31,'add_workerstate','Can add worker');
INSERT INTO auth_permission VALUES(92,31,'change_workerstate','Can change worker');
INSERT INTO auth_permission VALUES(93,31,'delete_workerstate','Can delete worker');
INSERT INTO auth_permission VALUES(94,32,'add_taskstate','Can add task');
INSERT INTO auth_permission VALUES(95,32,'change_taskstate','Can change task');
INSERT INTO auth_permission VALUES(96,32,'delete_taskstate','Can delete task');
INSERT INTO auth_permission VALUES(103,35,'add_registeredport','Can add Registered port');
INSERT INTO auth_permission VALUES(104,35,'change_registeredport','Can change Registered port');
INSERT INTO auth_permission VALUES(105,35,'delete_registeredport','Can delete Registered port');
INSERT INTO auth_permission VALUES(106,36,'add_postgresqlservice','Can add PostgreSQL Service');
INSERT INTO auth_permission VALUES(107,36,'change_postgresqlservice','Can change PostgreSQL Service');
INSERT INTO auth_permission VALUES(108,36,'delete_postgresqlservice','Can delete PostgreSQL Service');
INSERT INTO auth_permission VALUES(109,37,'add_postgresqluser','Can add PostgreSQL User');
INSERT INTO auth_permission VALUES(110,37,'change_postgresqluser','Can change PostgreSQL User');
INSERT INTO auth_permission VALUES(111,37,'delete_postgresqluser','Can delete PostgreSQL User');
INSERT INTO auth_permission VALUES(112,38,'add_postgresqldatabase','Can add PostgreSQL Database');
INSERT INTO auth_permission VALUES(113,38,'change_postgresqldatabase','Can change PostgreSQL Database');
INSERT INTO auth_permission VALUES(114,38,'delete_postgresqldatabase','Can delete PostgreSQL Database');
INSERT INTO auth_permission VALUES(115,39,'add_sslcertificate','Can add SSL Certificate');
INSERT INTO auth_permission VALUES(116,39,'change_sslcertificate','Can change SSL Certificate');
INSERT INTO auth_permission VALUES(117,39,'delete_sslcertificate','Can delete SSL Certificate');
CREATE TABLE "base_pluginstate" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "object_id" integer unsigned NOT NULL, "plugin_class" varchar(255) NOT NULL, "added" datetime NOT NULL, "added_by_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"));
CREATE TABLE "celery_taskmeta" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "task_id" varchar(255) NOT NULL UNIQUE, "status" varchar(50) NOT NULL, "result" text NULL, "date_done" datetime NOT NULL, "traceback" text NULL, "hidden" bool NOT NULL, "meta" text NULL);
CREATE TABLE "celery_tasksetmeta" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "taskset_id" varchar(255) NOT NULL UNIQUE, "result" text NOT NULL, "date_done" datetime NOT NULL, "hidden" bool NOT NULL);
CREATE TABLE "containers_container" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "container_id" varchar(96) NULL, "meta" text NULL, "is_running" bool NOT NULL, "added" datetime NOT NULL, "modified" datetime NOT NULL, "last_sync" datetime NULL, "docker_image_id" integer NULL REFERENCES "containers_dockerimage" ("id"), "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"));
CREATE TABLE "containers_dockerimage" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(255) NOT NULL UNIQUE, "active" bool NOT NULL, "repository" varchar(255) NOT NULL, "image_name" varchar(255) NOT NULL, "description" text NOT NULL, "main_user" varchar(50) NOT NULL, "entrypoint" varchar(300) NOT NULL, "ssh_support" bool NOT NULL, "added" datetime NOT NULL, "modified" datetime NOT NULL, "container_type" varchar(100) NOT NULL);
INSERT INTO containers_dockerimage VALUES(20,'HAProxy (legacy)',0,'korkholeh/haproxy:0.1','korkholeh/haproxy:0.1','Docker image for HAProxy routing and balancing','root','/sbin/my_init --',1,'2015-09-10 13:34:04.010684','2015-10-13 13:41:22.028434','service');
INSERT INTO containers_dockerimage VALUES(21,'Python App (legacy)',0,'korkholeh/webapp:0.1','korkholeh/webapp:0.1','Basic web application boilerplate as it works on the old server.','root','/sbin/my_init --',1,'2015-09-14 11:34:16.510232','2015-10-13 13:41:46.214769','app');
INSERT INTO containers_dockerimage VALUES(22,'PostgreSQL 9.3 (legacy)',0,'korkholeh/postgres93:0.2','korkholeh/postgres93:0.2','PostgreSQL 9.3 as a service','root','/sbin/my_init --',1,'2015-10-07 14:17:27.048441','2015-10-13 13:41:57.435449','service');
CREATE TABLE "containers_dockerimagepullinglog" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "added" datetime NOT NULL, "modified" datetime NOT NULL, "finished" bool NOT NULL, "docker_image_id" integer NOT NULL REFERENCES "containers_dockerimage" ("id"), "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"), "log" text NOT NULL);
CREATE TABLE "containers_envvariable" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "default_value" varchar(300) NOT NULL, "use_in_creation_form" bool NOT NULL, "docker_image_id" integer NOT NULL REFERENCES "containers_dockerimage" ("id"));
INSERT INTO containers_envvariable VALUES(1,'PG_PASSWORD','qazwsx',1,22);
CREATE TABLE "containers_exposedport" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "number" integer NOT NULL, "is_public" bool NOT NULL, "expose_to" varchar(100) NOT NULL, "docker_image_id" integer NOT NULL REFERENCES "containers_dockerimage" ("id"));
INSERT INTO containers_exposedport VALUES(36,80,1,80,20);
INSERT INTO containers_exposedport VALUES(37,443,1,443,20);
INSERT INTO containers_exposedport VALUES(38,22,1,'',20);
INSERT INTO containers_exposedport VALUES(39,22,1,'',21);
INSERT INTO containers_exposedport VALUES(40,80,1,'',21);
INSERT INTO containers_exposedport VALUES(41,22,1,'',22);
INSERT INTO containers_exposedport VALUES(42,5432,1,'',22);
CREATE TABLE "containers_imagetag" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "tag" varchar(100) NOT NULL, "docker_image_id" integer NOT NULL REFERENCES "containers_dockerimage" ("id"));
INSERT INTO containers_imagetag VALUES(17,'haproxy',20);
INSERT INTO containers_imagetag VALUES(18,'webapp',21);
INSERT INTO containers_imagetag VALUES(19,'postgresql',22);
CREATE TABLE "django_admin_log" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "action_time" datetime NOT NULL, "object_id" text NULL, "object_repr" varchar(200) NOT NULL, "action_flag" smallint unsigned NOT NULL, "change_message" text NOT NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id"), "user_id" integer NOT NULL REFERENCES "accounts_user" ("id"));
INSERT INTO django_admin_log VALUES(11,'2015-08-20 09:09:05.207693',6,'my-thinkpad',3,'',13,1);
INSERT INTO django_admin_log VALUES(12,'2015-08-20 09:22:16.420518',7,'my-thinkpad',3,'',13,1);
INSERT INTO django_admin_log VALUES(13,'2015-08-20 09:40:21.254099',1,'ftp-test@Demo server',1,'',14,1);
INSERT INTO django_admin_log VALUES(14,'2015-08-20 10:35:38.262428',2,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(15,'2015-08-20 12:04:16.288313',9,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(16,'2015-08-20 12:30:20.614550',10,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(17,'2015-08-20 12:31:30.701514',11,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(18,'2015-08-20 12:48:59.874903',14,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(19,'2015-08-20 14:10:36.708639',15,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(20,'2015-08-20 14:22:44.096316',19,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(21,'2015-08-20 14:27:11.763146',21,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(22,'2015-08-20 14:29:13.121720',22,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(23,'2015-08-20 14:30:53.414651',23,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(24,'2015-08-20 14:32:57.610301',24,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(25,'2015-08-21 08:18:34.460592',25,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(26,'2015-08-21 08:23:39.710104',26,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(27,'2015-08-21 08:33:06.777422',27,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(28,'2015-08-21 08:33:40.788255',28,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(29,'2015-08-21 08:34:36.827752',29,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(30,'2015-08-21 09:07:37.466630',30,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(31,'2015-08-21 09:10:17.022593',31,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(32,'2015-08-21 09:11:56.148300',32,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(33,'2015-08-21 09:20:22.274674',33,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(34,'2015-08-27 09:33:23.048933',1,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(35,'2015-08-27 09:34:15.308910',2,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(36,'2015-08-27 09:36:15.501655',3,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(37,'2015-08-27 09:37:53.599307',4,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(38,'2015-08-27 09:38:34.037479',5,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(39,'2015-08-27 11:21:06.485450',6,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(40,'2015-08-27 11:31:11.572865',1,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(41,'2015-08-27 11:33:51.491474',2,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(42,'2015-08-27 11:33:59.450183',7,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(43,'2015-08-27 11:35:24.086039',8,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(44,'2015-08-27 11:35:32.601374',3,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(45,'2015-08-27 11:38:32.356874',4,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(46,'2015-08-27 11:38:42.451940',9,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(47,'2015-08-31 10:13:00.913233',5,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(48,'2015-08-31 10:13:11.680594',10,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(49,'2015-08-31 10:16:02.652906',11,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(50,'2015-08-31 10:16:11.459903',6,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(51,'2015-08-31 10:41:48.156173',2,'HAProxy',3,'',22,1);
INSERT INTO django_admin_log VALUES(52,'2015-08-31 10:41:56.580174',7,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(53,'2015-08-31 10:42:05.959275',12,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(54,'2015-08-31 10:46:17.685649',3,'HAProxy',2,'Changed log and finished.',22,1);
INSERT INTO django_admin_log VALUES(55,'2015-08-31 13:01:23.445111',1,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(56,'2015-08-31 13:03:37.775918',2,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(57,'2015-08-31 13:04:22.483665',13,'HAProxy',2,'Changed expose_to for Exposed port "HAProxy:22".',15,1);
INSERT INTO django_admin_log VALUES(58,'2015-09-10 10:48:04.111764',4,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(59,'2015-09-10 10:48:04.265880',3,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(60,'2015-09-10 11:37:10.941025',13,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(61,'2015-09-10 11:37:39.270722',8,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(62,'2015-09-10 11:38:35.307842',9,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(63,'2015-09-10 11:38:48.338537',14,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(64,'2015-09-10 11:50:15.809727',15,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(65,'2015-09-10 11:50:27.931880',10,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(66,'2015-09-10 11:52:42.118485',16,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(67,'2015-09-10 11:52:52.923327',11,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(68,'2015-09-10 13:04:47.444773',12,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(69,'2015-09-10 13:04:55.406399',17,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(70,'2015-09-10 13:27:27.074644',18,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(71,'2015-09-10 13:27:34.342938',13,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(72,'2015-09-10 13:33:39.698696',19,'HAProxy',3,'',15,1);
INSERT INTO django_admin_log VALUES(73,'2015-09-10 13:33:48.101273',14,'Demo server',3,'',20,1);
INSERT INTO django_admin_log VALUES(74,'2015-09-11 11:17:13.372747',5,'haproxy@Demo server',1,'',19,1);
INSERT INTO django_admin_log VALUES(75,'2015-09-11 11:37:24.978527',20,'HAProxy',2,'Changed expose_to for Exposed port "HAProxy:80". Changed expose_to for Exposed port "HAProxy:443". Changed expose_to for Exposed port "HAProxy:22".',15,1);
INSERT INTO django_admin_log VALUES(76,'2015-09-11 11:38:52.483919',20,'HAProxy',2,'Changed expose_to for Exposed port "HAProxy:80". Changed expose_to for Exposed port "HAProxy:443".',15,1);
INSERT INTO django_admin_log VALUES(77,'2015-09-11 12:40:44.509984',5,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(78,'2015-09-11 12:48:53.721995',6,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(79,'2015-09-14 11:34:16.530529',21,'Old Grape app image',1,'',15,1);
INSERT INTO django_admin_log VALUES(80,'2015-09-14 11:34:38.351244',21,'Old Grape app image',2,'Changed active.',15,1);
INSERT INTO django_admin_log VALUES(81,'2015-09-14 12:41:27.951075',2,'Ddd',3,'',11,1);
INSERT INTO django_admin_log VALUES(82,'2015-09-14 12:58:50.575312',1,'webapp_01@Demo server',3,'',23,1);
INSERT INTO django_admin_log VALUES(83,'2015-09-14 14:03:23.151173',2,'webapp_01@Demo server',2,'Changed public_app_port and public_ssh_port.',23,1);
INSERT INTO django_admin_log VALUES(84,'2015-09-15 10:08:30.647205',3,'webapp_03@Demo server',2,'Changed active.',23,1);
INSERT INTO django_admin_log VALUES(85,'2015-09-15 10:08:46.816810',3,'webapp_03@Demo server',2,'Changed docker_container.',23,1);
INSERT INTO django_admin_log VALUES(86,'2015-09-15 10:14:05.456628',3,'webapp_03@Demo server',2,'Changed public_app_port and public_ssh_port.',23,1);
INSERT INTO django_admin_log VALUES(87,'2015-09-23 11:38:52.775899',4,'webapp_04@Demo server',2,'Changed public_app_port and public_ssh_port.',23,1);
INSERT INTO django_admin_log VALUES(88,'2015-09-23 11:39:11.750366',3,'webapp_03@Demo server',2,'Changed public_app_port and public_ssh_port.',23,1);
INSERT INTO django_admin_log VALUES(89,'2015-09-23 11:39:41.539793',2,'webapp_01@Demo server',2,'Changed public_app_port and public_ssh_port.',23,1);
INSERT INTO django_admin_log VALUES(90,'2015-10-05 11:04:07.223100',4,'mydemo4.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(93,'2015-10-07 10:35:25.102531',9,'webapp_03@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(94,'2015-10-07 10:35:54.388242',5,'mydemo3.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(95,'2015-10-07 10:39:28.648943',11,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(96,'2015-10-07 10:40:32.037902',10,'webapp_04@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(97,'2015-10-07 10:40:53.189642',9,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(98,'2015-10-07 11:19:48.823559',6,'mydemo2.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(99,'2015-10-07 11:26:53.485804',1,'mydemo1.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(100,'2015-10-07 11:27:29.091978',10,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(101,'2015-10-07 11:31:00.823208',10,'my-thinkpad',3,'',13,1);
INSERT INTO django_admin_log VALUES(102,'2015-10-07 11:36:16.735050',7,'mydemo1.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(103,'2015-10-07 11:38:47.053159',8,'mydemo1.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(104,'2015-10-07 11:38:55.754153',7,'webapp_01@Demo server',3,'',23,1);
INSERT INTO django_admin_log VALUES(105,'2015-10-07 11:39:17.584152',7,'webapp_01@Demo server',3,'',23,1);
INSERT INTO django_admin_log VALUES(106,'2015-10-07 11:39:28.716578',13,'webapp_01@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(107,'2015-10-07 11:39:48.735728',16,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(108,'2015-10-07 11:41:58.593871',9,'mydemo1.com',3,'',24,1);
INSERT INTO django_admin_log VALUES(109,'2015-10-07 13:19:13.204956',1,'Demo server',2,'Changed last_available_port.',11,1);
INSERT INTO django_admin_log VALUES(110,'2015-10-07 14:17:27.075484',22,'PostgreSQL 9.3 (Old)',1,'',15,1);
INSERT INTO django_admin_log VALUES(111,'2015-10-08 09:45:02.837939',1,'postgresql_01@Demo server',3,'',36,1);
INSERT INTO django_admin_log VALUES(112,'2015-10-08 09:45:46.597214',2,'postgresql_01@Demo server',3,'',36,1);
INSERT INTO django_admin_log VALUES(113,'2015-10-08 09:48:06.134972',3,'postgresql_01@Demo server',3,'',36,1);
INSERT INTO django_admin_log VALUES(114,'2015-10-08 09:48:19.656373',10,'PostgreSQL 9.3 (Old)',3,'',22,1);
INSERT INTO django_admin_log VALUES(115,'2015-10-08 09:48:57.249565',25,'postgresql_01@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(116,'2015-10-08 09:52:00.823409',4,'postgresql_01@Demo server',3,'',36,1);
INSERT INTO django_admin_log VALUES(117,'2015-10-08 09:52:16.583464',11,'PostgreSQL 9.3 (Old)',3,'',22,1);
INSERT INTO django_admin_log VALUES(118,'2015-10-08 10:00:16.638934',5,'postgresql_01@Demo server',2,'Changed docker_container and active.',36,1);
INSERT INTO django_admin_log VALUES(119,'2015-10-08 14:10:00.006583',26,'my-thinkpad',3,'',13,1);
INSERT INTO django_admin_log VALUES(120,'2015-10-09 08:11:19.908154',1,'demo (postgresql_01@Demo server)',3,'',37,1);
INSERT INTO django_admin_log VALUES(121,'2015-10-09 08:16:41.272705',2,'demo (postgresql_01@Demo server)',2,'No fields changed.',37,1);
INSERT INTO django_admin_log VALUES(122,'2015-10-09 12:21:40.289968',1,'demo',3,'',38,1);
INSERT INTO django_admin_log VALUES(123,'2015-10-12 12:38:11.261949',18,'mydemo4.com',2,'Changed ssl_certificate.',24,1);
INSERT INTO django_admin_log VALUES(124,'2015-10-12 12:39:17.247359',18,'mydemo4.com',2,'Changed ssl_certificate.',24,1);
INSERT INTO django_admin_log VALUES(125,'2015-10-12 12:40:59.864818',18,'mydemo4.com',2,'Changed ssl_certificate.',24,1);
INSERT INTO django_admin_log VALUES(126,'2015-10-13 13:39:08.842643',1,'Dedicated server',2,'Changed build_script.',10,1);
INSERT INTO django_admin_log VALUES(127,'2015-10-13 13:39:51.510619',1,'Demo Admin',2,'Changed email and full_name.',9,1);
INSERT INTO django_admin_log VALUES(128,'2015-10-13 13:40:07.937711',18,'webapp_18@Demo server',3,'',23,1);
INSERT INTO django_admin_log VALUES(129,'2015-10-13 13:40:08.134880',17,'webapp_01@Demo server',3,'',23,1);
INSERT INTO django_admin_log VALUES(130,'2015-10-13 13:40:31.431750',25,'plugins.postgresql.plugin.PostgreSQLPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(131,'2015-10-13 13:40:31.663286',24,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(132,'2015-10-13 13:40:31.828610',23,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(133,'2015-10-13 13:40:32.035270',12,'plugins.postgresql.plugin.PostgreSQLPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(134,'2015-10-13 13:40:32.563570',8,'plugins.haproxy.plugin.HAProxyPlugin',3,'',21,1);
INSERT INTO django_admin_log VALUES(135,'2015-10-13 13:40:42.895210',27,'postgresql_01@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(136,'2015-10-13 13:40:43.108664',24,'webapp_18@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(137,'2015-10-13 13:40:43.290183',23,'webapp_01@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(138,'2015-10-13 13:40:43.469205',7,'haproxy@Demo server',3,'',19,1);
INSERT INTO django_admin_log VALUES(139,'2015-10-13 13:40:53.475473',12,'PostgreSQL 9.3 (Old)',3,'',22,1);
INSERT INTO django_admin_log VALUES(140,'2015-10-13 13:40:53.727621',9,'Old Grape app image',3,'',22,1);
INSERT INTO django_admin_log VALUES(141,'2015-10-13 13:40:53.900471',8,'HAProxy',3,'',22,1);
INSERT INTO django_admin_log VALUES(142,'2015-10-13 13:41:22.035775',20,'HAProxy (legacy)',2,'Changed name.',15,1);
INSERT INTO django_admin_log VALUES(143,'2015-10-13 13:41:46.227539',21,'Python App (legacy)',2,'Changed name.',15,1);
INSERT INTO django_admin_log VALUES(144,'2015-10-13 13:41:57.443780',22,'PostgreSQL 9.3 (legacy)',2,'Changed name.',15,1);
INSERT INTO django_admin_log VALUES(145,'2015-10-13 13:42:26.527035',37,'ftp-blah@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(146,'2015-10-13 13:42:26.683112',36,'ftp-demo@Demo server',3,'',14,1);
INSERT INTO django_admin_log VALUES(147,'2015-10-13 13:43:23.506671',1,'Demo server',2,'Changed ip_address, host_domain, description and last_available_port.',11,1);
INSERT INTO django_admin_log VALUES(148,'2015-10-13 13:43:41.203962',10,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(149,'2015-10-13 13:43:41.438972',9,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(150,'2015-10-13 13:43:41.606603',8,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(151,'2015-10-13 13:43:41.775585',7,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(152,'2015-10-13 13:43:41.944614',6,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(153,'2015-10-13 13:43:42.096156',5,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(154,'2015-10-13 13:43:42.265674',4,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(155,'2015-10-13 13:43:42.459678',3,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(156,'2015-10-13 13:43:42.682342',2,'RegisteredPort object',3,'',35,1);
INSERT INTO django_admin_log VALUES(157,'2015-10-13 13:43:42.855671',1,'RegisteredPort object',3,'',35,1);
CREATE TABLE "django_content_type" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app_label" varchar(100) NOT NULL, "model" varchar(100) NOT NULL, UNIQUE ("app_label", "model"));
INSERT INTO django_content_type VALUES(1,'admin','logentry');
INSERT INTO django_content_type VALUES(2,'auth','permission');
INSERT INTO django_content_type VALUES(3,'auth','group');
INSERT INTO django_content_type VALUES(4,'contenttypes','contenttype');
INSERT INTO django_content_type VALUES(5,'sessions','session');
INSERT INTO django_content_type VALUES(6,'easy_select2','related');
INSERT INTO django_content_type VALUES(7,'easy_select2','testfieldsmodel');
INSERT INTO django_content_type VALUES(8,'easy_select2','emptymodel');
INSERT INTO django_content_type VALUES(9,'accounts','user');
INSERT INTO django_content_type VALUES(10,'hosts','servertype');
INSERT INTO django_content_type VALUES(11,'hosts','host');
INSERT INTO django_content_type VALUES(12,'sshkeys','sshkey');
INSERT INTO django_content_type VALUES(13,'sshkeys','sshkeyrecord');
INSERT INTO django_content_type VALUES(14,'hosts','ftpuser');
INSERT INTO django_content_type VALUES(15,'containers','dockerimage');
INSERT INTO django_content_type VALUES(16,'containers','exposedport');
INSERT INTO django_content_type VALUES(17,'containers','envvariable');
INSERT INTO django_content_type VALUES(18,'containers','imagetag');
INSERT INTO django_content_type VALUES(19,'containers','container');
INSERT INTO django_content_type VALUES(20,'haproxy','haproxyservice');
INSERT INTO django_content_type VALUES(21,'base','pluginstate');
INSERT INTO django_content_type VALUES(22,'containers','dockerimagepullinglog');
INSERT INTO django_content_type VALUES(23,'applications','webapplication');
INSERT INTO django_content_type VALUES(24,'haproxy','domain');
INSERT INTO django_content_type VALUES(25,'djcelery','taskmeta');
INSERT INTO django_content_type VALUES(26,'djcelery','tasksetmeta');
INSERT INTO django_content_type VALUES(27,'djcelery','intervalschedule');
INSERT INTO django_content_type VALUES(28,'djcelery','crontabschedule');
INSERT INTO django_content_type VALUES(29,'djcelery','periodictasks');
INSERT INTO django_content_type VALUES(30,'djcelery','periodictask');
INSERT INTO django_content_type VALUES(31,'djcelery','workerstate');
INSERT INTO django_content_type VALUES(32,'djcelery','taskstate');
INSERT INTO django_content_type VALUES(35,'hosts','registeredport');
INSERT INTO django_content_type VALUES(36,'postgresql','postgresqlservice');
INSERT INTO django_content_type VALUES(37,'postgresql','postgresqluser');
INSERT INTO django_content_type VALUES(38,'postgresql','postgresqldatabase');
INSERT INTO django_content_type VALUES(39,'haproxy','sslcertificate');
CREATE TABLE "django_migrations" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app" varchar(255) NOT NULL, "name" varchar(255) NOT NULL, "applied" datetime NOT NULL);
CREATE TABLE "django_session" ("session_key" varchar(40) NOT NULL PRIMARY KEY, "session_data" text NOT NULL, "expire_date" datetime NOT NULL);
CREATE TABLE "djcelery_crontabschedule" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "minute" varchar(64) NOT NULL, "hour" varchar(64) NOT NULL, "day_of_week" varchar(64) NOT NULL, "day_of_month" varchar(64) NOT NULL, "month_of_year" varchar(64) NOT NULL);
INSERT INTO djcelery_crontabschedule VALUES(1,0,4,'*','*','*');
INSERT INTO djcelery_crontabschedule VALUES(2,'*','*','*','*','*');
CREATE TABLE "djcelery_intervalschedule" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "every" integer NOT NULL, "period" varchar(24) NOT NULL);
CREATE TABLE "djcelery_periodictask" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(200) NOT NULL UNIQUE, "task" varchar(200) NOT NULL, "interval_id" integer NULL REFERENCES "djcelery_intervalschedule" ("id"), "crontab_id" integer NULL REFERENCES "djcelery_crontabschedule" ("id"), "args" text NOT NULL, "kwargs" text NOT NULL, "queue" varchar(200) NULL, "exchange" varchar(200) NULL, "routing_key" varchar(200) NULL, "expires" datetime NULL, "enabled" bool NOT NULL, "last_run_at" datetime NULL, "total_run_count" integer unsigned NOT NULL, "date_changed" datetime NOT NULL, "description" text NOT NULL);
INSERT INTO djcelery_periodictask VALUES(1,'celery.backend_cleanup','celery.backend_cleanup',NULL,1,'[]','{}',NULL,NULL,NULL,NULL,1,'2015-10-13 07:31:50.411892',8,'2015-10-13 08:16:35.541534','');
INSERT INTO djcelery_periodictask VALUES(2,'apps.containers.tasks.inspect_all_containers','apps.containers.tasks.inspect_all_containers',NULL,2,'[]','{}',NULL,NULL,NULL,NULL,1,'2015-10-13 13:36:00.002203',2348,'2015-10-13 13:36:00.006721','');
CREATE TABLE "djcelery_periodictasks" ("ident" smallint NOT NULL PRIMARY KEY, "last_update" datetime NOT NULL);
INSERT INTO djcelery_periodictasks VALUES(1,'2015-10-13 08:16:35.878964');
CREATE TABLE "djcelery_taskstate" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "state" varchar(64) NOT NULL, "task_id" varchar(36) NOT NULL UNIQUE, "name" varchar(200) NULL, "tstamp" datetime NOT NULL, "args" text NULL, "kwargs" text NULL, "eta" datetime NULL, "expires" datetime NULL, "result" text NULL, "traceback" text NULL, "runtime" real NULL, "retries" integer NOT NULL, "worker_id" integer NULL REFERENCES "djcelery_workerstate" ("id"), "hidden" bool NOT NULL);
CREATE TABLE "djcelery_workerstate" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "hostname" varchar(255) NOT NULL UNIQUE, "last_heartbeat" datetime NULL);
CREATE TABLE "haproxy_domain" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "domain" varchar(100) NOT NULL, "protocol" varchar(20) NOT NULL, "is_default" bool NOT NULL, "added" datetime NOT NULL, "added_by_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "haproxy_id" integer NULL REFERENCES "haproxy_haproxyservice" ("id"), "ssl_certificate_id" integer NULL REFERENCES "haproxy_sslcertificate" ("id"));
CREATE TABLE "haproxy_domain_webapps" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "domain_id" integer NOT NULL REFERENCES "haproxy_domain" ("id"), "webapplication_id" integer NOT NULL REFERENCES "applications_webapplication" ("id"), UNIQUE ("domain_id", "webapplication_id"));
CREATE TABLE "haproxy_haproxyservice" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "active" bool NOT NULL, "docker_container_id" integer NULL UNIQUE REFERENCES "containers_container" ("id"), "host_id" integer NOT NULL UNIQUE REFERENCES "hosts_host" ("id"));
CREATE TABLE "haproxy_sslcertificate" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(255) NOT NULL, "crt_file" text NOT NULL, "key_file" text NOT NULL, "added" datetime NOT NULL, "added_by_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "haproxy_id" integer NOT NULL REFERENCES "haproxy_haproxyservice" ("id"), UNIQUE ("name", "haproxy_id"));
CREATE TABLE "hosts_ftpuser" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "e_password" BLOB NOT NULL, "added" datetime NOT NULL, "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"), "owner_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "username" varchar(100) NOT NULL, UNIQUE ("username", "host_id"));
CREATE TABLE "hosts_host" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "e_password" BLOB NOT NULL, "name" varchar(100) NOT NULL, "ip_address" char(39) NOT NULL, "host_domain" varchar(200) NOT NULL, "description" text NOT NULL, "main_user" varchar(50) NOT NULL, "server_type_id" integer NOT NULL REFERENCES "hosts_servertype" ("id"), "is_manageable" bool NOT NULL, "docker_api_port" integer unsigned NOT NULL, "last_available_port" integer NOT NULL);
INSERT INTO hosts_host VALUES(1,'ש�fg@�$kmNZTk8h6DzMxu+EuG3W0aJ/+xHqdQdrmv+4Y4bM/bu6zqmqHHwvIc0jB0CRwv/h0KBpGaoDhByk3kaMDUdePEFro9Gw/0sbLYAvuQKNUQMecDjmtsB6+65mNOLkoaYEWfMnt6ETbt+mfA5AqaRZDvLqnu+VHRGCom8Ossg5k5lwwVZNf7eXX5+zHTYwSCM48zryMcZzBJArSPMGLWgH94DLeMGrcTFbtj5ipvSjeCXVEmxauCCXK+LZtRhQPiGsZC9OgYmzIQsWvtVsUlz7xfF48ohNBatSViqVckm8NTN51108dqV3TyrTp3ABwy4by5t+fezykT6MhzHFBVGh5LQAqDTmJlaNay6nP/jIDDuteILdZHhh8kkVfsbtBuh5hA1OTb1zW9/qIFWnKo2Yh17jLTEvfiOGdw2QIstk9zxrimukllh44R/XGxTwjt+qEEvCb7INCbrbYVC7qI5M39u434U4E27lu6zCOQknlOu0vZCKmqlq6W0YSIRZ+6f8mcfiWY7jGRYjpvWq/4U3P9nxE1LQZ5OH9YZIGPpwf/BcTldJudHoF5SjUPkw6qENQpb1ErQiIAZxyo0P9sMYzYoKfgdkQEkE1v9jHyCEQYyHu/d3kxk/AtU1m4fmGlNRbDuJTytoMDL9oQkwz0KmvGWi7H5JRyXBXI3CChMyLp1i7aUXdxH2TlFOHGIE8W/ugckR23h3z7QX6zWQ8G+iDyYcyi4XMVXJch3DRWWiSz7CSDYxSv4+D4Qlc+wPs/IuXBBr/W+duymjFcoYf4op52FZ1PDiqVS+4hoxeSal9i4KTbyUbc6pSdQUF1FMiFWW9Rtjv1mT+qDc8/rvhB9N4oS+Jl2HLwRNe0vILLgreiYTsNrY6EutjsPXEXGKHj3pE8YsvQHQEDrsi6AOdhGtUGK7boYCr2bdHiKn7P2+Ycqw','Demo server','10.10.10.10','demo.grape-project.com','Demo server description','webprod',1,1,2376,32900);
CREATE TABLE "hosts_registeredport" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "port_number" integer NOT NULL, "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"));
CREATE TABLE "hosts_servertype" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "build_script" text NOT NULL);
INSERT INTO hosts_servertype VALUES(1,'Dedicated server','#!/bin/bash
echo "Start to build infrastructure type: Chicago"

DOCKER_HOST="127.0.0.1"

echo "Prepare the basic system"

sudo apt-get -qq update
sudo apt-get -y upgrade
sudo apt-get -y install mc htop openssh-server git postgresql-client-common postgresql-client python-software-properties build-essential redis-tools apache2-utils linux-headers-$(uname -r)
sudo apt-get -y install --no-install-recommends virtualbox-guest-utils && sudo apt-get -y install virtualbox-guest-dkms

echo "Install Docker"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sudo sh -c "echo deb http://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
sudo apt-get -qq update
sudo apt-get -y install lxc-docker cgroup-lite
sudo gpasswd -a {{host.main_user}} docker
sudo service docker restart

echo "Install FTP"

sudo apt-get install vsftpd libpam-pwdfile
sudo mv /etc/vsftpd.conf /etc/vsftpd.conf.bak

sudo cat << EOT > /etc/vsftpd.conf
anonymous_enable=NO
local_enable=YES
chroot_local_user=YES
user_config_dir=/etc/vsftpd/vsftpd-virtual-user/
virtual_use_local_privs=YES
dual_log_enable=YES
connect_from_port_20=YES
listen=YES
pam_service_name=ftp
tcp_wrappers=YES
allow_writeable_chroot=YES
EOT

sudo restart vsftpd

sudo mkdir -p /etc/vsftpd/
sudo mkdir -p /etc/vsftpd/vsftpd-virtual-user/
sudo touch /etc/vsftpd/vsftpd-virtual-user/vsftpd_user

echo "Setup PAM"

sudo cp /etc/pam.d/vsftpd /etc/pam.d/vsftpd.bak
sudo cat << EOT > /etc/pam.d/vsftpd
session optional        pam_keyinit.so  force   revoke
auth   required        pam_listfile.so item=user sense=deny file=/etc/ftpusers onerr=succeed
auth   required        pam_shells.so
auth    include system-auth
account include system-auth
session include system-auth
session required pam_loginuid.so
EOT

echo "Prepare directories"

sudo mkdir -p /home/{{host.main_user}}/src
sudo mkdir -p /home/{{host.main_user}}/tmp
sudo mkdir -p /home/{{host.main_user}}/backups
sudo mkdir -p /home/{{host.main_user}}/cert
sudo chown {{host.main_user}}:{{host.main_user}} /home/{{host.main_user}}/src
sudo chown {{host.main_user}}:{{host.main_user}} /home/{{host.main_user}}/tmp
sudo chown {{host.main_user}}:{{host.main_user}} /home/{{host.main_user}}/backups
sudo chown {{host.main_user}}:{{host.main_user}} /home/{{host.main_user}}/cert

echo "Setup complete. Good luck!"');
CREATE TABLE "postgresql_postgresqldatabase" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "username" varchar(100) NOT NULL, "owner_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "pg_service_id" integer NOT NULL REFERENCES "postgresql_postgresqlservice" ("id"), "webapp_id" integer NOT NULL REFERENCES "applications_webapplication" ("id"), UNIQUE ("name", "pg_service_id"));
CREATE TABLE "postgresql_postgresqlservice" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(255) NOT NULL, "active" bool NOT NULL, "docker_container_id" integer NULL REFERENCES "containers_container" ("id"), "host_id" integer NOT NULL REFERENCES "hosts_host" ("id"), "e_password" BLOB NOT NULL, "added" datetime NOT NULL, "docker_image_id" integer NULL REFERENCES "containers_dockerimage" ("id"), UNIQUE ("name", "host_id"));
CREATE TABLE "postgresql_postgresqluser" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "e_password" BLOB NOT NULL, "username" varchar(100) NOT NULL, "owner_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "pg_service_id" integer NOT NULL REFERENCES "postgresql_postgresqlservice" ("id"), "webapp_id" integer NOT NULL REFERENCES "applications_webapplication" ("id"), UNIQUE ("username", "pg_service_id"));
CREATE TABLE sqlite_sequence(name,seq);
INSERT INTO sqlite_sequence VALUES('django_migrations',44);
INSERT INTO sqlite_sequence VALUES('django_content_type',39);
INSERT INTO sqlite_sequence VALUES('auth_permission',117);
INSERT INTO sqlite_sequence VALUES('accounts_user',1);
INSERT INTO sqlite_sequence VALUES('hosts_servertype',1);
INSERT INTO sqlite_sequence VALUES('django_admin_log',159);
INSERT INTO sqlite_sequence VALUES('sshkeys_sshkey',1);
INSERT INTO sqlite_sequence VALUES('sshkeys_sshkeyrecord',28);
INSERT INTO sqlite_sequence VALUES('hosts_ftpuser',37);
INSERT INTO sqlite_sequence VALUES('containers_dockerimage',22);
INSERT INTO sqlite_sequence VALUES('containers_imagetag',19);
INSERT INTO sqlite_sequence VALUES('containers_exposedport',42);
INSERT INTO sqlite_sequence VALUES('containers_envvariable',1);
INSERT INTO sqlite_sequence VALUES('containers_container',27);
INSERT INTO sqlite_sequence VALUES('base_pluginstate',25);
INSERT INTO sqlite_sequence VALUES('haproxy_haproxyservice',15);
INSERT INTO sqlite_sequence VALUES('containers_dockerimagepullinglog',12);
INSERT INTO sqlite_sequence VALUES('applications_webapplication',18);
INSERT INTO sqlite_sequence VALUES('haproxy_domain_webapps',30);
INSERT INTO sqlite_sequence VALUES('djcelery_crontabschedule',2);
INSERT INTO sqlite_sequence VALUES('djcelery_periodictask',2);
INSERT INTO sqlite_sequence VALUES('hosts_host',1);
INSERT INTO sqlite_sequence VALUES('hosts_registeredport',10);
INSERT INTO sqlite_sequence VALUES('postgresql_postgresqlservice',6);
INSERT INTO sqlite_sequence VALUES('postgresql_postgresqluser',5);
INSERT INTO sqlite_sequence VALUES('postgresql_postgresqldatabase',3);
INSERT INTO sqlite_sequence VALUES('haproxy_sslcertificate',5);
INSERT INTO sqlite_sequence VALUES('haproxy_domain',22);
CREATE TABLE "sshkeys_sshkey" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "key" text NOT NULL, "added" datetime NOT NULL, "owner_id" integer NOT NULL REFERENCES "accounts_user" ("id"), UNIQUE ("name", "owner_id", "key"));
CREATE TABLE "sshkeys_sshkeyrecord" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "object_id" integer unsigned NOT NULL, "added" datetime NOT NULL, "added_by_id" integer NOT NULL REFERENCES "accounts_user" ("id"), "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"), "key_id" integer NOT NULL REFERENCES "sshkeys_sshkey" ("id"));
CREATE INDEX "accounts_user_groups_0e939a4f" ON "accounts_user_groups" ("group_id");
CREATE INDEX "accounts_user_groups_e8701ad4" ON "accounts_user_groups" ("user_id");
CREATE INDEX "accounts_user_user_permissions_8373b171" ON "accounts_user_user_permissions" ("permission_id");
CREATE INDEX "accounts_user_user_permissions_e8701ad4" ON "accounts_user_user_permissions" ("user_id");
CREATE INDEX "applications_webapplication_5e7b1936" ON "applications_webapplication" ("owner_id");
CREATE INDEX "applications_webapplication_8396f175" ON "applications_webapplication" ("host_id");
CREATE INDEX "applications_webapplication_90d3cd44" ON "applications_webapplication" ("docker_image_id");
CREATE INDEX "auth_group_permissions_0e939a4f" ON "auth_group_permissions" ("group_id");
CREATE INDEX "auth_group_permissions_8373b171" ON "auth_group_permissions" ("permission_id");
CREATE INDEX "auth_permission_417f1b1c" ON "auth_permission" ("content_type_id");
CREATE INDEX "base_pluginstate_0c5d7d4e" ON "base_pluginstate" ("added_by_id");
CREATE INDEX "base_pluginstate_417f1b1c" ON "base_pluginstate" ("content_type_id");
CREATE INDEX "celery_taskmeta_662f707d" ON "celery_taskmeta" ("hidden");
CREATE INDEX "celery_tasksetmeta_662f707d" ON "celery_tasksetmeta" ("hidden");
CREATE INDEX "containers_container_8396f175" ON "containers_container" ("host_id");
CREATE INDEX "containers_container_90d3cd44" ON "containers_container" ("docker_image_id");
CREATE INDEX "containers_dockerimagepullinglog_8396f175" ON "containers_dockerimagepullinglog" ("host_id");
CREATE INDEX "containers_dockerimagepullinglog_90d3cd44" ON "containers_dockerimagepullinglog" ("docker_image_id");
CREATE INDEX "containers_envvariable_90d3cd44" ON "containers_envvariable" ("docker_image_id");
CREATE INDEX "containers_exposedport_90d3cd44" ON "containers_exposedport" ("docker_image_id");
CREATE INDEX "containers_imagetag_90d3cd44" ON "containers_imagetag" ("docker_image_id");
CREATE INDEX "django_admin_log_417f1b1c" ON "django_admin_log" ("content_type_id");
CREATE INDEX "django_admin_log_e8701ad4" ON "django_admin_log" ("user_id");
CREATE INDEX "django_session_de54fa62" ON "django_session" ("expire_date");
CREATE INDEX "djcelery_periodictask_1dcd7040" ON "djcelery_periodictask" ("interval_id");
CREATE INDEX "djcelery_periodictask_f3f0d72a" ON "djcelery_periodictask" ("crontab_id");
CREATE INDEX "djcelery_taskstate_662f707d" ON "djcelery_taskstate" ("hidden");
CREATE INDEX "djcelery_taskstate_863bb2ee" ON "djcelery_taskstate" ("tstamp");
CREATE INDEX "djcelery_taskstate_9ed39e2e" ON "djcelery_taskstate" ("state");
CREATE INDEX "djcelery_taskstate_b068931c" ON "djcelery_taskstate" ("name");
CREATE INDEX "djcelery_taskstate_ce77e6ef" ON "djcelery_taskstate" ("worker_id");
CREATE INDEX "djcelery_workerstate_f129901a" ON "djcelery_workerstate" ("last_heartbeat");
CREATE INDEX "haproxy_domain_0c5d7d4e" ON "haproxy_domain" ("added_by_id");
CREATE INDEX "haproxy_domain_2bdded38" ON "haproxy_domain" ("ssl_certificate_id");
CREATE INDEX "haproxy_domain_f191cee5" ON "haproxy_domain" ("haproxy_id");
CREATE INDEX "haproxy_domain_webapps_418294d4" ON "haproxy_domain_webapps" ("webapplication_id");
CREATE INDEX "haproxy_domain_webapps_662cbf12" ON "haproxy_domain_webapps" ("domain_id");
CREATE INDEX "haproxy_sslcertificate_0c5d7d4e" ON "haproxy_sslcertificate" ("added_by_id");
CREATE INDEX "haproxy_sslcertificate_f191cee5" ON "haproxy_sslcertificate" ("haproxy_id");
CREATE INDEX "hosts_ftpuser_5e7b1936" ON "hosts_ftpuser" ("owner_id");
CREATE INDEX "hosts_ftpuser_8396f175" ON "hosts_ftpuser" ("host_id");
CREATE INDEX "hosts_host_38e2bcbc" ON "hosts_host" ("server_type_id");
CREATE INDEX "hosts_registeredport_8396f175" ON "hosts_registeredport" ("host_id");
CREATE INDEX "postgresql_postgresqldatabase_5e7b1936" ON "postgresql_postgresqldatabase" ("owner_id");
CREATE INDEX "postgresql_postgresqldatabase_7e3f36bd" ON "postgresql_postgresqldatabase" ("pg_service_id");
CREATE INDEX "postgresql_postgresqldatabase_dae6fc8f" ON "postgresql_postgresqldatabase" ("webapp_id");
CREATE INDEX "postgresql_postgresqlservice_8396f175" ON "postgresql_postgresqlservice" ("host_id");
CREATE INDEX "postgresql_postgresqlservice_90d3cd44" ON "postgresql_postgresqlservice" ("docker_image_id");
CREATE INDEX "postgresql_postgresqlservice_b968e211" ON "postgresql_postgresqlservice" ("docker_container_id");
CREATE INDEX "postgresql_postgresqluser_5e7b1936" ON "postgresql_postgresqluser" ("owner_id");
CREATE INDEX "postgresql_postgresqluser_7e3f36bd" ON "postgresql_postgresqluser" ("pg_service_id");
CREATE INDEX "postgresql_postgresqluser_dae6fc8f" ON "postgresql_postgresqluser" ("webapp_id");
CREATE INDEX "sshkeys_sshkey_5e7b1936" ON "sshkeys_sshkey" ("owner_id");
CREATE INDEX "sshkeys_sshkeyrecord_0c5d7d4e" ON "sshkeys_sshkeyrecord" ("added_by_id");
CREATE INDEX "sshkeys_sshkeyrecord_30f69126" ON "sshkeys_sshkeyrecord" ("key_id");
CREATE INDEX "sshkeys_sshkeyrecord_417f1b1c" ON "sshkeys_sshkeyrecord" ("content_type_id");
COMMIT;
