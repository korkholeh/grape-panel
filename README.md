# Grape Panel

## Requirements

    pip install -r requirements.txt

How to install Redis

    sudo add-apt-repository ppa:chris-lea/redis-server
    sudo apt-get update
    sudo apt-get install redis-server

How to launch Celery worker for debug purposes:

    celery --app=grpanel.celery:app worker --beat --loglevel=INFO

## Docker setup requirements

Grape use Docker API with certificates support for security. Copy `ca.pem`, `server-cert.pem` and `server-key.pm` to the folder `/home/webprod/certs` to the server.

The next Docker options in file `/etc/default/docker` are required:

    DOCKER_OPTS="--tlsverify --tlscacert=/home/webprod/certs/ca.pem --tlscert=/home/webprod/certs/server-cert.pem --tlskey=/home/webprod/certs/server-key.pem -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock"

How to create certificates: https://docs.docker.com/articles/https/

