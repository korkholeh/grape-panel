from django.conf import settings


def send_mail(
        to=[],
        subject='',
        html_template='',
        txt_template='',
        context={}):
    """Send mail to the all contacts
    """
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import render_to_string
    _to = to

    context.update({
        'project_domain': settings.PROJECT_DOMAIN,
        'project_name': settings.PROJECT_NAME,
    })

    text = render_to_string(txt_template, context)
    html = render_to_string(html_template, context)

    mail = EmailMultiAlternatives(
        subject=subject,
        body=text,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=_to)
    mail.attach_alternative(html, "text/html")
    return mail.send(fail_silently=True)


def send_mail_to_staff(
        subject='',
        html_template='',
        txt_template='',
        context={}):
    from .models import User
    staff = User.objects.filter(active=True, is_staff=True)
    addresses = [x.email for x in staff]
    return send_mail(addresses, subject, html_template, txt_template, context)
