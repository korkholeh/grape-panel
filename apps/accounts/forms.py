from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import UserCreationForm as OldUserCreationForm
from django.contrib.auth.forms import UserChangeForm as OldUserChangeForm
from django.contrib.auth.forms import (
    ReadOnlyPasswordHashField,
    PasswordResetForm,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, ButtonHolder, Submit
from .models import User


class UserChangeForm(OldUserChangeForm):
    password = ReadOnlyPasswordHashField(
        label=_('Password'),
        help_text=_(
            u'Raw passwords are not stored, so there is no way to see '
            u'this user\'s password, but you can change the password '
            u'using <a href="password/">this form</a>.'))

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_password(self):
        return self.initial['password']


class UserCreationForm(OldUserCreationForm):
    """
    A form that creates a user, with no privileges,
    from the given email, full_name and password.
    """
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # del self.fields['username']

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'email', 'full_name',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'email',
            'full_name',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class UserRegistrationForm(UserCreationForm):
    accept_tos = forms.BooleanField(
        label=_(
            'I have read and accept the <a href="/terms/" '
            'rel="nofollow" target="_blank">Terms of Service</a>'),
        initial=False, required=False)

    class Meta:
        model = User
        fields = ('email', 'full_name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'email',
            'full_name',
            'password1',
            'password2',
            'accept_tos',
            ButtonHolder(
                Submit('submit', _('Create user'), css_class='btn btn-primary')
            )
        )

    def clean_accept_tos(self):
        t = self.cleaned_data['accept_tos']
        if not t:
            raise forms.ValidationError(
                _('You should accept Terms of Service for registration.'))


class UserPasswordResetForm(PasswordResetForm):
    def get_users(self, email):
        active_users = User.objects.filter(
            email__iexact=email, is_active=True)
        return (u for u in active_users)
