from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, render
from django.views.generic.detail import DetailView
from django.views.generic import UpdateView
from django.utils.decorators import method_decorator
from django.conf import settings
from django.http import Http404
from django.contrib.messages.views import SuccessMessageMixin
from .forms import (
    EditUserForm,
    UserRegistrationForm,
)
from .models import User


class MyAccountUpdateView(SuccessMessageMixin, UpdateView):
    model = User
    template_name = 'accounts/edit_my_account.html'
    form_class = EditUserForm
    success_message = _(u"Your account settings was changed.")

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        return reverse('my_account')


def my_account(request):
    user = request.user
    return render(
        request,
        'accounts/my_account.html',
        {
            'user': user,
        })


def verify_email(request, id, code):
    is_ok = False
    try:
        user = User.objects.get(
            id=id,
            email_verification_code__exact=code,
        )
        user.email_verification_code = 'VERIFIED'
        # All users with verified emails are also marked like verified
        # until new algorythm will not be implemented
        user.verified = True
        user.save()
        is_ok = True
    except User.DoesNotExist:
        is_ok = False

    if is_ok:
        return render(
            request,
            'accounts/email_verification_success.html',
            {
                'user': user,
            })
    else:
        return render(
            request,
            'accounts/email_verification_fail.html',
            {})


def logout_view(request):
    from django.contrib.auth import logout
    logout(request)
    response = redirect(settings.LOGIN_REDIRECT_URL)
    return response
