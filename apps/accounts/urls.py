from django.conf.urls import patterns, url
from django.contrib.auth.views import (
    login,
    password_change,
    password_change_done,
    password_reset,
    password_reset_done,
    password_reset_confirm,
    password_reset_complete,
)
# from django.views.generic import TemplateView
# from .decorators import ajax_required
from .forms import UserPasswordResetForm
from .views import (
    my_account,
    MyAccountUpdateView,
    # register_view,
    verify_email,
    # send_verification_letter,
    logout_view,
)


urlpatterns = patterns(
    '',
    url(
        r'^login/$', login,
        {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    # url(
    #     r'^register/$', register_view,
    #     name='register'),
    # url(r'^register/success/$',
    #     TemplateView.as_view(
    #         template_name='accounts/registration_success.html'),
    #     name='registration_success'),

    # url(
    #     r'^send-verify-email/(?P<id>\d+)/$', send_verification_letter,
    #     name='send_verify_email'),
    url(
        r'^verify-email/(?P<id>\d+)/(?P<code>\w+)/$', verify_email,
        name='verify_email'),

    url(
        r'^password-change/$',
        password_change,
        {'template_name': 'accounts/password_change_form.html'},
        name='password_change'),
    url(
        r'^password-change/done/$',
        password_change_done,
        {'template_name': 'accounts/password_change_done.html'},
        name='password_change_done'),
    url(
        r'^password-reset/$',
        password_reset,
        {
            'template_name': 'accounts/password_reset_form.html',
            'password_reset_form': UserPasswordResetForm},
        name='password_reset'),
    url(
        r'^password-reset/done/$',
        password_reset_done,
        {'template_name': 'accounts/password_reset_done.html'},
        name='password_reset_done'),
    url(
        r'^password-reset/(?P<uidb64>.+)/(?P<token>.+)/confirm/$',
        password_reset_confirm,
        {'template_name': 'accounts/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(
        r'^password-reset/complete/$',
        password_reset_complete,
        {'template_name': 'accounts/password_reset_complete.html'},
        name='password_reset_complete'),

    # url(r'^user-toolbar/$', ajax_required(
    #     TemplateView.as_view(template_name='accounts/user_toolbar.html'))),
    url(
        r'^my-account/edit/$',
        MyAccountUpdateView.as_view(),
        name='edit_my_account'),
    url(r'^my-account/$', my_account, name='my_account'),
)
