from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin

from .models import User
from .forms import UserChangeForm, UserCreationForm


class UserAdmin(UserAdmin):
    add_form_template = 'accounts/admin/auth/user/add_form.html'
    fieldsets = (
        (None, {
            'fields': (
                'email', 'full_name',
                'password',
            )}),
        (_(u'Verification'), {
            'fields': (
                'email_verification_code',
            ),
        }),
        (_(u'Permissions'), {
            'fields': (
                'is_active', 'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',
            )}),
        (_(u'Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'full_name', 'password1', 'password2')
        }),
    )

    list_display = (
        'email', 'full_name', 'is_staff', 'is_superuser', 'is_active',
        'date_joined', 'last_login',
        'has_usable_password')
    list_filter = (
        'is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('full_name', 'email')
    ordering = ('email',)
    date_hierarchy = 'date_joined'
    filter_horizontal = ('groups', 'user_permissions',)

    form = UserChangeForm
    add_form = UserCreationForm
    actions = [
        'request_email_verification',
        'activate',
        'disactivate',
        'set_unusable_password',
    ]

    def request_email_verification(self, request, queryset):
        for u in queryset:
            u.send_email_verification_letter()
    request_email_verification.short_description = \
        _('Request email verification')

    def activate(self, request, queryset):
        queryset.update(is_active=True)
    activate.short_description = \
        _('Activate')

    def disactivate(self, request, queryset):
        queryset.update(is_active=False)
    disactivate.short_description = \
        _('Disactivate')

    def set_unusable_password(self, request, queryset):
        for q in queryset:
            q.set_unusable_password()
            q.save()
    set_unusable_password.short_description = \
        _('Set unusable password')


admin.site.register(User, UserAdmin)
