from django import template

register = template.Library()


@register.inclusion_tag(
    'accounts/user_toolbar.html', takes_context=True)
def user_toolbar(context):
    request = context['request']
    return {
        'user': request.user,
    }
