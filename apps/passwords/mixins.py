# based on snippet: https://djangosnippets.org/snippets/1330/
from os import urandom
from base64 import b64encode, b64decode
from django.db import models
from django.conf import settings
from Crypto.Cipher import ARC4


ENCRYPT_SECRET_KEY = 'jsdhflkahxlkHOAUDOW'


def get_value(name):
    def f(self):
        return EncryptedMixin.decrypt(getattr(self, 'e_%s' % name))
    return f


def set_value(name):
    def f(self, value):
        setattr(self, 'e_%s' % name, EncryptedMixin.encrypt(value))
    return f


def encrypted_property(name):
    return property(get_value(name), set_value(name))


class EncryptedMixin:
    """Mixin for build the models with encrypted fields.

    How to use:
        from django.db import models
        from apps.passwords.mixins import EncryptedMixin, encrypted_property

        class MyFunnyPasswordMixin(EncryptedMixin, models.Model):
            e_password = models.TextField(blank=True)
            password = encrypted_property('password')

            class Meta:
                abstract = True
    """
    SALT_SIZE = 8

    @staticmethod
    def encrypt(plaintext):
        salt = urandom(EncryptedMixin.SALT_SIZE)
        arc4 = ARC4.new(salt + bytes(ENCRYPT_SECRET_KEY, 'utf-8'))
        plaintext = "%3d%s%s" % (
            len(plaintext),
            plaintext,
            urandom(256-len(plaintext)))
        plaintext = bytes(plaintext, 'utf-8')
        return salt + b'$' + b64encode(arc4.encrypt(plaintext))

    @staticmethod
    def decrypt(ciphertext):
        _t = ciphertext.split(b'$')
        salt = _t[0]
        ciphertext = b64decode(_t[1])
        arc4 = ARC4.new(salt + bytes(ENCRYPT_SECRET_KEY, 'utf-8'))
        plaintext = arc4.decrypt(ciphertext)
        return (plaintext[3:3+int(plaintext[:3].strip())]).decode('utf-8')


class ModelWithPassword(EncryptedMixin, models.Model):
    """Adds the encrypted password field to a model.
    """
    e_password = models.BinaryField(blank=True)
    password = encrypted_property('password')

    class Meta:
        abstract = True
