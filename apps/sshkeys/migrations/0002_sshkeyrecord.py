# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sshkeys', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SSHKeyRecord',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Added by')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('key', models.ForeignKey(to='sshkeys.SSHKey')),
            ],
            options={
                'verbose_name_plural': 'SSH Key Records',
                'verbose_name': 'SSH Key Record',
            },
        ),
    ]
