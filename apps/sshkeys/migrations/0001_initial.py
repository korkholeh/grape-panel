# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SSHKey',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('key', models.TextField(verbose_name='Key')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Owner')),
            ],
            options={
                'verbose_name_plural': 'SSH Keys',
                'ordering': ('name',),
                'verbose_name': 'SSH Key',
            },
        ),
        migrations.AlterUniqueTogether(
            name='sshkey',
            unique_together=set([('name', 'owner', 'key')]),
        ),
    ]
