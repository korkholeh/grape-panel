from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import SSHKey, SSHKeyRecord


class SSHKeyForm(forms.ModelForm):

    class Meta:
        model = SSHKey
        fields = ['name', 'key']

    def __init__(self, owner, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.owner = owner
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['key'].widget.attrs['readonly'] = True

    def clean_name(self):
        sshkeys = SSHKey.objects.filter(
            owner=self.owner, name=self.cleaned_data['name'])
        if sshkeys:
            raise forms.ValidationError(
                _('You already have SSH key with this name.'))
        else:
            return self.cleaned_data['name']

    def clean_key(self):
        sshkeys = SSHKey.objects.filter(
            owner=self.owner, key=self.cleaned_data['key'])
        if sshkeys and not self.fields['key'].widget.attrs['readonly']:
            raise forms.ValidationError(
                _('You already have this SSH key.'))
        else:
            return self.cleaned_data['key']


class AddSSHKeyRecordForm(forms.ModelForm):
    class Meta:
        model = SSHKeyRecord
        fields = ['key']

    def __init__(self, user, p_object, *args, **kwargs):
        from .models import SSHKey
        super().__init__(*args, **kwargs)
        self.p_object = p_object
        self.fields['key'].queryset = SSHKey.objects.filter(owner=user)

    def clean_key(self):
        from .utils import get_sshkey_records_for_object
        exist_ssh = get_sshkey_records_for_object(self.p_object)
        already_exist = exist_ssh.filter(
            key=self.cleaned_data['key']).exists()

        if not already_exist:
            return self.cleaned_data['key']
        raise forms.ValidationError(
            _(u'This key already added to %s.' % str(self.p_object)))
