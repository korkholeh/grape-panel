from django.contrib.contenttypes.models import ContentType


def get_sshkey_records_for_object(p_object):
    from .models import SSHKeyRecord
    p_type = ContentType.objects.get_for_model(p_object)
    return SSHKeyRecord.objects.filter(
        content_type=p_type,
        object_id=p_object.id,
    )
