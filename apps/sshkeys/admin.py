from django.contrib import admin
from .models import (
    SSHKey,
    SSHKeyRecord,
)


class SSHKeyAdmin(admin.ModelAdmin):
    list_display = ['name', 'owner', 'added']

admin.site.register(SSHKey, SSHKeyAdmin)


class SSHKeyRecordAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'key', 'added_by', 'added']

admin.site.register(SSHKeyRecord, SSHKeyRecordAdmin)
