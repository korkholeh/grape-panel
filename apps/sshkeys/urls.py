from django.conf.urls import patterns, url
from .views import (
    SSHKeyListView,
    SSHKeyCreateView,
    SSHKeyEditView,
    SSHKeyDeleteView,
)


urlpatterns = patterns(
    'sshkeys.views',
    url(r'^add/$', SSHKeyCreateView.as_view(), name='sshkeys_add_key'),
    url(
        r'^(?P<pk>\d+)/delete/$',
        SSHKeyDeleteView.as_view(), name='sshkeys_delete_key'),
    url(
        r'^(?P<pk>\d+)/edit/$',
        SSHKeyEditView.as_view(), name='sshkeys_edit_key'),
    url(r'^$', SSHKeyListView.as_view(), name='sshkeys_list'),
)
