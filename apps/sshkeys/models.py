from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class SSHKey(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Owner'))
    key = models.TextField(_('Key'))
    added = models.DateTimeField(_('Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('SSH Key')
        verbose_name_plural = _('SSH Keys')
        unique_together = ('name', 'owner', 'key')
        ordering = ('name',)

    def __str__(self):
        return self.name


class SSHKeyRecord(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    key = models.ForeignKey(SSHKey)
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('SSH Key Record')
        verbose_name_plural = _('SSH Key Records')

    def __str__(self):
        return str(self.key)
