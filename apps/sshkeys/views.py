from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404

from apps.core.mixins import CheckOwnerMixin
from .models import SSHKey, SSHKeyRecord
from .forms import SSHKeyForm, AddSSHKeyRecordForm


class SSHKeyListView(ListView):
    model = SSHKey
    context_object_name = 'keys'

    def get_queryset(self):
        user = self.request.user
        return SSHKey.objects.filter(owner=user)


class SSHKeyCreateView(CreateView):
    model = SSHKey
    form_class = SSHKeyForm
    success_url = reverse_lazy('sshkeys_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs

    def form_valid(self, form):
        key = form.save(commit=False)
        key.owner = self.request.user
        key.save()
        return super().form_valid(form)


class SSHKeyEditView(CheckOwnerMixin, UpdateView):
    model = SSHKey
    form_class = SSHKeyForm
    success_url = reverse_lazy('sshkeys_list')

    def get_form_kwargs(self):
        kwargs = super(SSHKeyEditView, self).get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs


class SSHKeyDeleteView(CheckOwnerMixin, DeleteView):
    model = SSHKey
    success_url = reverse_lazy('sshkeys_list')


class SSHKeyRecordListView(ListView):
    model = SSHKeyRecord
    context_object_name = 'keys_records'
    context_parent_object_name = 'p_object'
    add_key_url_name = ''
    remove_key_url_name = ''

    def get_parent_object(self):
        pass

    def get_parent_object_info(self):
        from django.contrib.contenttypes.models import ContentType
        p_object = self.get_parent_object()
        p_type = ContentType.objects.get_for_model(p_object)
        return p_type, p_object.id

    def get_queryset(self):
        (c_type, o_id) = self.get_parent_object_info()
        return SSHKeyRecord.objects.filter(
            content_type=c_type,
            object_id=o_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        p_object = self.get_parent_object()
        context.update({
            self.context_parent_object_name: p_object,
            'p_object': p_object,
            'add_key_url': self.get_add_key_url(),
            'remove_key_url_name': self.remove_key_url_name,
        })
        return context

    def get_add_key_url(self):
        p_object = self.get_parent_object()
        return reverse(
            self.add_key_url_name,
            args=(p_object.id,))


class SSHKeyRecordCreateView(CreateView):
    model = SSHKeyRecord
    form_class = AddSSHKeyRecordForm
    context_parent_object_name = 'p_object'
    success_url_name = ''

    def get_parent_object(self):
        pass

    def get_parent_object_info(self):
        from django.contrib.contenttypes.models import ContentType
        p_object = self.get_parent_object()
        p_type = ContentType.objects.get_for_model(p_object)
        return p_type, p_object.id

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['p_object'] = self.get_parent_object()
        return kwargs

    def form_valid(self, form):
        (c_type, o_id) = self.get_parent_object_info()
        key = form.save(commit=False)
        key.added_by = self.request.user
        key.content_type = c_type
        key.object_id = o_id
        key.save()
        p_object = self.get_parent_object()
        p_object.add_ssh_key(key)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        p_object = self.get_parent_object()
        context.update({
            self.context_parent_object_name: p_object,
            'p_object': p_object,
            'cancel_url': self.get_cancel_url(),
        })
        return context

    def get_success_url(self):
        p_object = self.get_parent_object()
        return reverse(
            self.success_url_name,
            args=(p_object.id,))

    def get_cancel_url(self):
        return self.get_success_url()


class SSHKeyRecordDeleteView(DeleteView):
    model = SSHKeyRecord
    context_parent_object_name = 'p_object'
    success_url_name = ''

    def get_parent_object(self):
        pass

    def get_parent_object_info(self):
        from django.contrib.contenttypes.models import ContentType
        p_object = self.get_parent_object()
        p_type = ContentType.objects.get_for_model(p_object)
        return p_type, p_object.id

    def get_object(self):
        (c_type, o_id) = self.get_parent_object_info()
        result = get_object_or_404(
            SSHKeyRecord,
            content_type=c_type,
            object_id=o_id,
            key__owner=self.request.user,
            pk=self.kwargs.get('sshkey_pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        p_object = self.get_parent_object()
        context.update({
            self.context_parent_object_name: p_object,
            'p_object': p_object,
            'success_url': self.get_success_url(),
            'cancel_url': self.get_cancel_url(),
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        p_object = self.get_parent_object()
        p_object.remove_ssh_key(self.object)
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        p_object = self.get_parent_object()
        return reverse(
            self.success_url_name, args=(p_object.id,))

    def get_cancel_url(self):
        return self.get_success_url()
