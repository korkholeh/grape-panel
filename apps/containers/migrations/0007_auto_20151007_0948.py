# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0006_auto_20151006_1510'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PortRegistrySettings',
        ),
        migrations.RemoveField(
            model_name='registeredport',
            name='docker_container',
        ),
        migrations.DeleteModel(
            name='RegisteredPort',
        ),
    ]
