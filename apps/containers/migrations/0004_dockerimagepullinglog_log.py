# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0003_dockerimagepullinglog'),
    ]

    operations = [
        migrations.AddField(
            model_name='dockerimagepullinglog',
            name='log',
            field=models.TextField(verbose_name='Log', blank=True),
        ),
    ]
