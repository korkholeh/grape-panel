# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('containers', '0004_dockerimagepullinglog_log'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortRegistrySettings',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('start_from', models.IntegerField(default=32771, verbose_name='Start from')),
            ],
            options={
                'verbose_name_plural': 'Port registry settings',
                'verbose_name': 'Port registry settings',
            },
        ),
        migrations.CreateModel(
            name='RegisteredPort',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('port_number', models.IntegerField(verbose_name='Port number')),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name_plural': 'Registered ports',
                'verbose_name': 'Registered port',
            },
        ),
    ]
