# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0005_auto_20150820_1230'),
        ('containers', '0002_auto_20150825_1327'),
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImagePullingLog',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('finished', models.BooleanField(verbose_name='Finished', default=False)),
                ('docker_image', models.ForeignKey(to='containers.DockerImage', verbose_name='Docker image', related_name='pulling_logs')),
                ('host', models.ForeignKey(to='hosts.Host', verbose_name='Host', related_name='image_pulling_logs')),
            ],
            options={
                'verbose_name': 'Docker image pulling log',
                'verbose_name_plural': 'Docker image pulling logs',
            },
        ),
    ]
