# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0005_portregistrysettings_registeredport'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='registeredport',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='registeredport',
            name='object_id',
        ),
        migrations.AddField(
            model_name='registeredport',
            name='docker_container',
            field=models.ForeignKey(default=1, related_name='registered_ports', to='containers.Container', verbose_name='Container'),
            preserve_default=False,
        ),
    ]
