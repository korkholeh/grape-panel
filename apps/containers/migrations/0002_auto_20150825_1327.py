# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0005_auto_20150820_1230'),
        ('containers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Container',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('container_id', models.CharField(max_length=96, verbose_name='Container ID', null=True, blank=True)),
                ('meta', models.TextField(verbose_name='Meta', default='{}', null=True, blank=True)),
                ('is_running', models.BooleanField(verbose_name='Is running', default=True)),
                ('added', models.DateTimeField(verbose_name='Added', auto_now_add=True)),
                ('modified', models.DateTimeField(verbose_name='Added', auto_now=True)),
                ('last_sync', models.DateTimeField(verbose_name='Last sync', null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Containers',
                'verbose_name': 'Container',
            },
        ),
        migrations.CreateModel(
            name='EnvVariable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('default_value', models.CharField(max_length=300, verbose_name='Default value', blank=True)),
                ('use_in_creation_form', models.BooleanField(verbose_name='Use in creation form', default=False)),
            ],
            options={
                'verbose_name_plural': 'Environment variables',
                'verbose_name': 'Environment variable',
            },
        ),
        migrations.CreateModel(
            name='ExposedPort',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('number', models.IntegerField(verbose_name='Number')),
                ('is_public', models.BooleanField(verbose_name='Is public', default=True)),
                ('expose_to', models.CharField(max_length=100, verbose_name='Expose to', default='', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Exposed ports',
                'verbose_name': 'Exposed port',
            },
        ),
        migrations.CreateModel(
            name='ImageTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('tag', models.CharField(max_length=100, verbose_name='Tag')),
            ],
            options={
                'verbose_name_plural': 'Image tags',
                'verbose_name': 'Image tag',
            },
        ),
        migrations.AlterField(
            model_name='dockerimage',
            name='container_type',
            field=models.CharField(max_length=100, choices=[('app', 'Application'), ('service', 'Service')], default='app', verbose_name='Type'),
        ),
        migrations.AddField(
            model_name='imagetag',
            name='docker_image',
            field=models.ForeignKey(related_name='tags', verbose_name='Docker image', to='containers.DockerImage'),
        ),
        migrations.AddField(
            model_name='exposedport',
            name='docker_image',
            field=models.ForeignKey(related_name='exposed_ports', verbose_name='Docker image', to='containers.DockerImage'),
        ),
        migrations.AddField(
            model_name='envvariable',
            name='docker_image',
            field=models.ForeignKey(related_name='env_variables', verbose_name='Docker image', to='containers.DockerImage'),
        ),
        migrations.AddField(
            model_name='container',
            name='docker_image',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Docker image', to='containers.DockerImage'),
        ),
        migrations.AddField(
            model_name='container',
            name='host',
            field=models.ForeignKey(related_name='containers', verbose_name='Host', to='hosts.Host'),
        ),
    ]
