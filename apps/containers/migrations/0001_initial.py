# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(unique=True, verbose_name='Name', max_length=255)),
                ('active', models.BooleanField(verbose_name='Active', default=False)),
                ('repository', models.CharField(max_length=255, blank=True, help_text='Public image repository at Docker Hub.', verbose_name='Repository')),
                ('image_name', models.CharField(max_length=255, verbose_name='Image name')),
                ('description', models.TextField(help_text='You can use Markdown here.', verbose_name='Description')),
                ('main_user', models.CharField(max_length=50, verbose_name='Main user', default='root')),
                ('entrypoint', models.CharField(max_length=300, verbose_name='Entrypoint', default='/sbin/my_init --')),
                ('ssh_support', models.BooleanField(verbose_name='SSH Support', default=True)),
                ('added', models.DateTimeField(verbose_name='Added', auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('container_type', models.CharField(max_length=100, verbose_name='Container Type', choices=[('app', 'Application'), ('service', 'Service')], default='app')),
            ],
            options={
                'verbose_name_plural': 'Docker images',
                'verbose_name': 'Docker image',
            },
        ),
    ]
