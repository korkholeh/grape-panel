from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    DockerImage,
    DockerImagePullingLog,
    ImageTag,
    EnvVariable,
    ExposedPort,
    Container,
)


class ExposedPortInline(admin.TabularInline):
    model = ExposedPort


class EnvVariableInline(admin.TabularInline):
    model = EnvVariable


class ImageTagInline(admin.TabularInline):
    model = ImageTag


class DockerImageAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'repository', 'image_name', 'added']
    inlines = [ExposedPortInline, EnvVariableInline, ImageTagInline]

admin.site.register(DockerImage, DockerImageAdmin)


class DockerImagePullingLogAdmin(admin.ModelAdmin):
    list_display = ['docker_image', 'host', 'added', 'modified', 'finished']

admin.site.register(DockerImagePullingLog, DockerImagePullingLogAdmin)

class ContainerAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'host', 'container_id',
        'docker_image', 'is_running',
        'last_sync']
    actions = [
        'sync_action', 'start_action',
        'stop_action', 'restart_action']

    def sync_action(self, request, queryset):
        for q in queryset:
            q.sync()
    sync_action.short_description = _('Sync containers')

    def start_action(self, request, queryset):
        for q in queryset:
            q.start()
    start_action.short_description = _('Start')

    def stop_action(self, request, queryset):
        for q in queryset:
            q.stop()
    stop_action.short_description = _('Stop')

    def restart_action(self, request, queryset):
        for q in queryset:
            q.restart()
    restart_action.short_description = _('Restart')

admin.site.register(Container, ContainerAdmin)
