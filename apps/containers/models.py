from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils import timezone


CONTAINER_TYPE_CHOICES = (
    ('app', _('Application')),
    ('service', _('Service')),
)


class DockerImage(models.Model):
    name = models.CharField(
        _('Name'), max_length=255, unique=True)
    active = models.BooleanField(_('Active'), default=False)
    repository = models.CharField(
        _('Repository'), max_length=255,
        blank=True,
        help_text=_('Public image repository at Docker Hub.'))
    image_name = models.CharField(
        _('Image name'), max_length=255)
    description = models.TextField(
        _('Description'),
        help_text=_('You can use Markdown here.'))
    main_user = models.CharField(
        _('Main user'), max_length=50, default='root')
    entrypoint = models.CharField(
        _('Entrypoint'), max_length=300,
        default='/sbin/my_init --')
    ssh_support = models.BooleanField(_('SSH Support'), default=True)

    added = models.DateTimeField(_('Added'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)

    container_type = models.CharField(
        _('Type'), max_length=100, default='app',
        choices=CONTAINER_TYPE_CHOICES)

    class Meta:
        verbose_name = _('Docker image')
        verbose_name_plural = _('Docker images')

    def __str__(self):
        return self.name

    def pull_to_host(self, host):
        from .tasks import pull_docker_image
        pull_docker_image.delay(host.pk, self.pk)


class DockerImagePullingLog(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_('Docker image'),
        related_name='pulling_logs')
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_('Host'),
        related_name='image_pulling_logs')
    log = models.TextField(_('Log'), blank=True)

    added = models.DateTimeField(_('Added'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)
    finished = models.BooleanField(_('Finished'), default=False)

    class Meta:
        verbose_name = _('Docker image pulling log')
        verbose_name_plural = _('Docker image pulling logs')

    def __str__(self):
        return str(self.docker_image)

    def get_temporary_log(self):
        import redis

        pool = redis.ConnectionPool(
            host=settings.GRAPE_REDIS_HOST,
            port=int(settings.GRAPE_REDIS_PORT),
            db=int(settings.GRAPE_REDIS_DB))
        r = redis.Redis(connection_pool=pool)

        HASH_NAME = 'pulling_log:%s:%s' % (
            self.host_id,
            self.docker_image_id)
        full_log = ''
        _keys_str = r.hget(HASH_NAME, 'keys')
        if _keys_str:
            _keys = str(_keys_str, 'utf-8').split(',')
        else:
            _keys = []
        for k in _keys:
            if k != 'status':
                full_log += str(r.hget(HASH_NAME, k), 'utf-8')
        return full_log


class ExposedPort(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_('Docker image'),
        related_name='exposed_ports')
    number = models.IntegerField(_('Number'))
    is_public = models.BooleanField(_(u'Is public'), default=True)
    expose_to = models.CharField(
        _('Expose to'), default='', blank=True, max_length=100)

    class Meta:
        verbose_name = _('Exposed port')
        verbose_name_plural = _('Exposed ports')

    def __str__(self):
        return '{image}:{port}'.format(
            image=str(self.docker_image),
            port=self.number)


class EnvVariable(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_('Docker image'),
        related_name='env_variables')
    name = models.CharField(_('Name'), max_length=100)
    default_value = models.CharField(
        _('Default value'), max_length=300, blank=True)
    use_in_creation_form = models.BooleanField(
        _('Use in creation form'), default=False)

    class Meta:
        verbose_name = _('Environment variable')
        verbose_name_plural = _('Environment variables')

    def __str__(self):
        return '{image}:{env}'.format(
            image=str(self.docker_image),
            env=self.name)


class ImageTag(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_('Docker image'),
        related_name='tags')
    tag = models.CharField(_('Tag'), max_length=100)

    class Meta:
        verbose_name = _('Image tag')
        verbose_name_plural = _('Image tags')

    def __str__(self):
        return '{image}:{env}'.format(
            image=str(self.docker_image),
            env=self.tag)


class Container(models.Model):
    name = models.CharField(
        _('Name'), max_length=100)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_('Host'),
        related_name='containers')
    docker_image = models.ForeignKey(
        DockerImage, verbose_name=_('Docker image'),
        null=True, blank=True)

    container_id = models.CharField(
        _('Container ID'),
        max_length=96, null=True, blank=True)
    meta = models.TextField(
        _('Meta'),
        blank=True, null=True, default='{}')
    is_running = models.BooleanField(
        _('Is running'), default=True)

    added = models.DateTimeField(_('Added'), auto_now_add=True)
    modified = models.DateTimeField(_('Added'), auto_now=True)
    last_sync = models.DateTimeField(
        _('Last sync'),
        null=True, blank=True)

    APP_PORT = 80
    SSH_PORT = 22

    class Meta:
        verbose_name = _('Container')
        verbose_name_plural = _('Containers')

    def __str__(self):
        return '{container}@{host}'.format(
            container=self.name,
            host=str(self.host))

    def sync(self):
        # Do nothing if Container instance is not initialized yet
        if not self.container_id:
            return None

        from .utils import inspect_docker_container
        # Update container
        inspect_data = inspect_docker_container(self.id)
        _meta = str(inspect_data)
        self.meta = _meta
        self.last_sync = timezone.now()
        self.is_running = inspect_data['State']['Running']
        self.save()

        # Exit if not running
        if not self.is_running:
            return None

        # Update webapps
        from apps.applications.models import WebApplication
        webapps = WebApplication.objects.filter(docker_container=self)
        for wa in webapps:
            ap = self.get_app_port()
            sp = self.get_ssh_port()
            if wa.public_app_port != ap or wa.public_ssh_port != sp:
                wa.public_app_port = self.get_app_port()
                wa.public_ssh_port = self.get_ssh_port()
                wa.save()

    def exec_cmd(self, cmd):
        from .utils import exec_in_docker_container
        return exec_in_docker_container(self.id, cmd)

    def operate_container(self, operation, countdown=0, return_result=False):
        if countdown == 0:
            from .utils import operate_docker_container
            result = operate_docker_container(self.id, operation)
            self.sync()
        else:
            from .tasks import (
                operate_docker_container_task,
                container_operation_handler,
            )
            result = operate_docker_container_task.apply_async(
                args=(self.id, operation),
                countdown=countdown,
                link=container_operation_handler.s(),
            )
        if return_result:
            return result

    def start(self, countdown=0, return_result=False):
        return self.operate_container('start', countdown, return_result)

    def stop(self, countdown=0, return_result=False):
        return self.operate_container('stop', countdown, return_result)

    def restart(self, countdown=0, return_result=False):
        return self.operate_container('restart', countdown, return_result)

    def top(self, countdown=0, return_result=False):
        return self.operate_container('top', countdown, return_result)

    def create_and_start(self, env_variables={}):
        from .utils import create_and_start_docker_container
        create_and_start_docker_container(self.id, env_variables)

    def get_host_port(self, port):
        _meta = eval(self.meta)
        ports_config = _meta['NetworkSettings']['Ports']
        for p in ports_config.keys():
            t = p.split('/')
            if t[0] == str(port):
                if ports_config[p] is not None:
                    return ports_config[p][0]['HostPort']
        return None

    def get_meta_config(self, path):
        _meta = eval(self.meta)
        result = _meta
        keys = path.split('.')
        for key in keys:
            result = result[key]
        return result

    def get_internal_ip(self):
        return self.get_meta_config('NetworkSettings.IPAddress')

    def get_gateway_ip(self):
        return self.get_meta_config('NetworkSettings.Gateway')

    def get_volume_dir(self, container_dir):
        _meta = eval(self.meta)
        volumes_config = _meta['Volumes']
        return volumes_config.get(container_dir)

    def get_ssh_volume(self):
        if self.docker_image.main_user == 'root':
            return self.get_volume_dir('/root/.ssh')
        else:
            return self.get_volume_dir(
                '/home/%s/.ssh' % self.docker_image.main_user)

    def get_app_ip_and_port(self):
        return '%s:%s' % (
            self.host.ip_address,
            self.get_app_port(),
        )

    def get_app_port(self):
        _result = self.get_host_port(self.APP_PORT)
        if _result:
            return int(_result)
        else:
            return None

    def get_ssh_port(self):
        _result = self.get_host_port(self.SSH_PORT)
        if _result:
            return int(_result)
        else:
            return None

    def add_ssh_key(self, key_object):
        self.save_ssh_keys(add_keys=[key_object.key.key])

    def remove_ssh_key(self, key_object):
        self.save_ssh_keys(remove_keys=[key_object.key.key])

    def save_ssh_keys(self, add_keys=[], remove_keys=[]):
        from .utils import save_ssh_keys_for_container
        save_ssh_keys_for_container(self.id, add_keys, remove_keys)

    def get_ssh_command(self):
        ssh_port = self.get_ssh_port()
        return 'ssh -p {port} {user}@{ip}'.format(
            port=ssh_port,
            user=self.docker_image.main_user,
            ip=self.host.ip_address,
        )
