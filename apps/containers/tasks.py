import paramiko
# import select
import redis
import json
from celery import shared_task
from celery.decorators import periodic_task
from celery.schedules import crontab
from django.conf import settings


@shared_task
def operate_docker_container_task(container_id, operation):
    from .utils import operate_docker_container
    operate_docker_container(container_id, operation)
    return container_id


@shared_task
def container_operation_handler(container_id):
    from .models import Container
    container = Container.objects.get(pk=container_id)
    container.sync()


@shared_task
def pull_docker_image(host_id, docker_image_id):
    from docker import Client, tls
    from apps.hosts.models import Host
    from .models import (
        DockerImage,
        DockerImagePullingLog,
    )
    host = Host.objects.get(pk=host_id)
    docker_image = DockerImage.objects.get(pk=docker_image_id)

    try:
        pulling_log = DockerImagePullingLog.objects.get(
            host=host,
            docker_image=docker_image,
        )
        return None
    except DockerImagePullingLog.DoesNotExist:
        pass

    repo_name = docker_image.repository

    pool = redis.ConnectionPool(
        host=settings.GRAPE_REDIS_HOST,
        port=int(settings.GRAPE_REDIS_PORT),
        db=int(settings.GRAPE_REDIS_DB))
    r = redis.Redis(connection_pool=pool)

    tls_config = tls.TLSConfig(
        client_cert=(settings.DOCKER_TLS_CERT, settings.DOCKER_TLS_KEY),
        verify=settings.DOCKER_TLS_CA)
    cli = Client(
        base_url='https://%s:%s' % (
            host.ip_address, host.docker_api_port),
        tls=tls_config)

    HASH_NAME = 'pulling_log:%s:%s' % (host_id, docker_image_id)
    i = 0

    # Clear temporary log in Redis if exists
    _keys = r.hkeys(HASH_NAME)
    if _keys:
        for k in _keys:
            r.hdel(HASH_NAME, k)

    # Init log
    r.hmset(HASH_NAME, {'status': 'pulling'})
    flat_keys = []

    pulling_log = DockerImagePullingLog.objects.create(
        docker_image_id=docker_image_id,
        host_id=host_id,
    )

    for line in cli.pull(repo_name, stream=True):
        log_line = json.loads(str(line, 'utf-8'))
        if 'id' in log_line:
            if 'progress' in log_line:
                log_str = '%s: %s %s\n' % (
                    log_line.get('id', ''),
                    log_line.get('status', ''),
                    log_line.get('progress', ''),
                )
            else:
                log_str = '%s: %s\n' % (
                    log_line.get('id', ''),
                    log_line.get('status', ''))
            r.hset(HASH_NAME, log_line['id'], log_str)
            if log_line['id'] not in flat_keys:
                flat_keys.append(log_line['id'])
        else:
            log_str = log_line.get('status') + '\n'
            _key = 'f:%s' % i
            r.hset(HASH_NAME, _key, log_str)
            flat_keys.append(_key)
            i += 1
        r.hset(HASH_NAME, 'keys', ','.join(flat_keys))
        # if log_str:
        #     pulling_log, created = \
        #         DockerImagePullingLog.objects.get_or_create(
        #             docker_image_id=docker_image_id,
        #             host_id=host_id,
        #         )
        #     if created:
        #         pulling_log.log = log_str
        #     else:
        #         pulling_log.log += log_str
        #     pulling_log.save()

    # client = paramiko.SSHClient()
    # client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # client.connect(
    #     hostname=host.ip_address,
    #     username=host.main_user,
    #     password=host.password)

    # cmd_str = 'docker pull %s' % repo_name
    # stdin, stdout, stderr = client.exec_command(cmd_str)

    # while not stdout.channel.exit_status_ready():
    #     # Only print data if there is data to read in the channel
    #     if stdout.channel.recv_ready():
    #         rl, wl, xl = select.select([stdout.channel], [], [], 0.0)
    #         if len(rl) > 0:
    #             # Print data from stdout
    #             pulling_log, created = \
    #                 DockerImagePullingLog.objects.get_or_create(
    #                     docker_image_id=docker_image_id,
    #                     host_id=host_id,
    #                 )
    #             log_str = str(stdout.channel.recv(1024), 'utf-8')
    #             if created:
    #                 pulling_log.log = log_str
    #             else:
    #                 pulling_log.log += log_str
    #             pulling_log.save()

    r.hset(HASH_NAME, 'status', 'finished')

    pulling_log.log = pulling_log.get_temporary_log()
    pulling_log.finished = True
    pulling_log.save()


# @shared_task
# def start_docker_container(container_id, env_variables={}):
#     from .models import Container
#     container = Container.objects.get(pk=container_id)

#     host = container.host
#     client = paramiko.SSHClient()
#     client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#     client.connect(
#         hostname=host.ip_address,
#         username=host.main_user,
#         password=host.password)

#     docker_image = container.docker_image

#     _cmd = ['docker run -d']
#     for p in docker_image.exposed_ports.filter(is_public=True):
#         if p.expose_to:
#             _cmd.append('-p {0}:{1}'.format(p.expose_to, p.number))
#         else:
#             _cmd.append('-p {0}'.format(p.number))
#     for e in docker_image.env_variables.all():
#         _v = env_variables.get(e.name, e.default_value)
#         if _v:
#             _cmd.append('-e {name}="{value}"'.format(
#                 name=e.name, value=_v))
#     _cmd.append('--name="{0}"'.format(container.name))
#     _cmd.append(docker_image.image_name)
#     _cmd.append(docker_image.entrypoint)
#     cmd = ' '.join(_cmd)
#     # print ('cmd=', cmd)
#     stdin, stdout, stderr = client.exec_command(cmd)
#     container_id = str(stdout.read(), 'utf-8').strip()

#     container.container_id = container_id
#     container.is_running = True
#     container.save()

@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
def inspect_all_containers():
    from .models import Container
    containers = Container.objects.all()
    ids = []
    for c in containers:
        c.sync()  # Sync container metadata
        ids.append(c.pk)
    return ids
