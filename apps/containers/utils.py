import docker
import paramiko
import os
from django.conf import settings
from django.utils import timezone


def create_and_start_docker_container(container_id, env_variables={}):
    from .models import Container
    from apps.hosts.utils import register_new_port
    container = Container.objects.get(pk=container_id)

    host = container.host
    docker_image = container.docker_image
    cli = host.get_docker_client()

    ports = []
    port_bindings = {}
    for p in docker_image.exposed_ports.filter(is_public=True):
        ports.append(p.number)
        if p.expose_to:
            port_bindings[p.number] = p.expose_to
        else:
            port_bindings[p.number] = register_new_port(host)

    envs = {}
    for e in docker_image.env_variables.all():
        _v = env_variables.get(e.name, e.default_value)
        if _v:
            envs[e.name] = _v

    _ct = cli.create_container(
        image=docker_image.image_name,
        command=docker_image.entrypoint,
        detach=True,
        name=container.name,
        ports=ports,
        host_config=docker.utils.create_host_config(
            port_bindings=port_bindings,
        ),
        environment=envs,
    )
    # print (type(_ct), _ct)
    try:
        response = cli.start(container=_ct.get('Id', ''))
        if not response:
            container.is_running = True
        # print ('start:', response)
    except docker.errors.APIError:
        container.is_running = False

    inspection_result = cli.inspect_container(container=_ct.get('Id', ''))

    container.container_id = _ct.get('Id', '')
    container.meta = str(inspection_result)
    container.last_sync = timezone.now()
    container.save()
    return container.container_id


def operate_docker_container(container_id, operation):
    """Supports the next operations:

        'start'
        'stop'
        'restart'
        'top'
    """
    from .models import Container
    container = Container.objects.get(pk=container_id)

    host = container.host
    cli = host.get_docker_client()
    result = None
    if operation == 'start':
        result = cli.start(container=container.container_id)
    elif operation == 'stop':
        result = cli.stop(container=container.container_id)
    elif operation == 'restart':
        result = cli.restart(container=container.container_id)
    elif operation == 'top':
        result = cli.top(container=container.container_id)
    return result


def exec_in_docker_container(container_id, cmd):
    """Execute given command inside a container
    """
    from .models import Container
    container = Container.objects.get(pk=container_id)

    host = container.host
    cli = host.get_docker_client()
    result = None
    exec_instance = cli.exec_create(
        container=container.container_id,
        cmd=cmd,
    )
    result = cli.exec_start(exec_id=exec_instance['Id'])
    return result


def remove_docker_container(
        container_id, remove_volumes=False, force_remove=False):
    from .models import Container
    container = Container.objects.get(pk=container_id)

    host = container.host
    cli = host.get_docker_client()
    result = cli.remove_container(
        container=container.container_id,
        v=remove_volumes,
        force=force_remove)
    return result


def inspect_docker_container(container_id):
    from .models import Container
    container = Container.objects.get(pk=container_id)

    host = container.host
    cli = host.get_docker_client()
    inspection_result = cli.inspect_container(
        container=container.container_id)
    return inspection_result


def save_ssh_keys_for_container(container_id, add_keys=[], remove_keys=[]):
    from django.contrib.contenttypes.models import ContentType
    from apps.core.utils import get_manageable_ssh_key
    from apps.sshkeys.models import SSHKeyRecord
    from .models import Container
    container = Container.objects.get(pk=container_id)

    host = container.host
    ssh_dir = container.get_ssh_volume()
    keys = [get_manageable_ssh_key().strip()]

    p_type = ContentType.objects.get_for_model(container)
    sshkey_records = SSHKeyRecord.objects.filter(
        content_type=p_type,
        object_id=container.pk)
    for record in sshkey_records:
        if record.key.key not in remove_keys:
            keys.append(record.key.key)
    if len(add_keys) > 0:
        for key in add_keys:
            keys.append(key)

    authorized_keys_filename = os.path.join(ssh_dir, 'authorized_keys')
    authorized_keys_content = '\n'.join(keys)
    # Run background task
    host.save_str_as_file(authorized_keys_content, authorized_keys_filename)
