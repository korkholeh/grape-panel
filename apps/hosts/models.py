from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from apps.passwords.mixins import ModelWithPassword


class ServerType(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    build_script = models.TextField(_('Build script'))

    class Meta:
        verbose_name = _('Server type')
        verbose_name_plural = _('Server types')

    def __str__(self):
        return self.name


class Host(ModelWithPassword):
    """This is a node (dedicated host or VM) in Grape infrastructure.
    """
    name = models.CharField(_('Name'), max_length=100)
    server_type = models.ForeignKey(
        ServerType, verbose_name=_('Server type'))
    ip_address = models.GenericIPAddressField(_('IP Address'))
    docker_api_port = models.PositiveIntegerField(
        _('Docker API port'), default=2376)
    host_domain = models.CharField(
        _('Host domain'), max_length=200, default='webXXX.grape-project.com')
    description = models.TextField(
        _('Description'), blank=True,
        help_text=_('You can use Markdown here.'))
    main_user = models.CharField(_('Main user'), max_length=50)

    is_manageable = models.BooleanField(
        _('Is manageable'), default=False)
    last_available_port = models.IntegerField(
        _('Last available port'), default=32771)

    tlscacert = models.TextField(
        _('TLS CA Certificate'),
        default='',
        blank=True)
    tlscert = models.TextField(
        _('TLS Certificate'),
        default='',
        blank=True)
    tlskey = models.TextField(
        _('TLS Key'),
        default='',
        blank=True)

    class Meta:
        verbose_name = _(u'Host')
        verbose_name_plural = _(u'Hosts')
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.tlscert or self.tlskey or self.tlscacert:
            self.save_tls_files()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'host_detail',
            args=[self.pk])

    def get_description_as_html(self):
        import markdown
        md = markdown.Markdown(
            safe_mode='replace',
            html_replacement_text='--RAW HTML NOT ALLOWED--')
        return md.convert(self.description)

    def get_host_domain(self):
        return self.host_domain

    def get_ssh_command(self):
        return 'ssh {user}@{ip}'.format(
            user=self.main_user,
            ip=self.ip_address,
        )

    def get_build_script(self):
        from django.template import Context, Template
        template = Template(self.server_type.build_script)
        context = Context({"host": self})
        return template.render(context)

    def add_ssh_key(self, key_object):
        from .tasks import add_ssh_key_to_host
        add_ssh_key_to_host.delay(self.pk, key_object.key.key)

    def remove_ssh_key(self, key_object):
        from .tasks import remove_ssh_key_from_host
        remove_ssh_key_from_host.delay(self.pk, key_object.key.key)

    def add_ftp_user(self, ftpuser_object):
        from .tasks import add_ftp_user
        add_ftp_user.delay(
            self.pk, ftpuser_object.username, ftpuser_object.password)

    def remove_ftp_user(self, ftpuser_object):
        from .tasks import remove_ftp_user
        remove_ftp_user.delay(self.pk, ftpuser_object.username)

    def change_ftp_user_password(self, ftpuser_object):
        from .tasks import change_ftp_user_password
        change_ftp_user_password.delay(
            self.pk,
            ftpuser_object.username,
            ftpuser_object.password)

    def save_tls_files(self):
        import os
        tlscacert = os.path.join(
            settings.DOCKER_TLS_DIR, 'ca-%s.pem' % self.id)
        tlscert = os.path.join(
            settings.DOCKER_TLS_DIR, 'cert-%s.pem' % self.id)
        tlskey = os.path.join(
            settings.DOCKER_TLS_DIR, 'key-%s.pem' % self.id)

        with open(tlscacert, 'w') as f:
            f.write(self.tlscacert)
        with open(tlscert, 'w') as f:
            f.write(self.tlscert)
        with open(tlskey, 'w') as f:
            f.write(self.tlskey)

    def get_docker_tls_config(self):
        import docker
        import os
        tls_config = docker.tls.TLSConfig(
            client_cert=(
                os.path.join(settings.DOCKER_TLS_DIR, 'cert-%s.pem' % self.id),
                os.path.join(settings.DOCKER_TLS_DIR, 'key-%s.pem' % self.id),
            ),
            verify=os.path.join(
                settings.DOCKER_TLS_DIR, 'ca-%s.pem' % self.id))
        return tls_config

    def get_docker_client(self):
        '''Common util for connection to Docker API
        '''
        import docker
        tls_config = self.get_docker_tls_config()
        cli = docker.Client(
            base_url='https://%s:%s' % (
                self.ip_address, self.docker_api_port),
            version=settings.DOCKER_API_VERSION,
            tls=tls_config)
        return cli

    def save_str_as_file(self, content, file_name):
        from .tasks import save_str_as_file
        save_str_as_file.delay(self.pk, content, file_name)

    def exec_cmd(self, cmd, sudo=False):
        from .utils import exec_cmd_on_host
        return exec_cmd_on_host(self.pk, cmd, sudo)


class FTPUser(ModelWithPassword):
    username = models.CharField(
        _('User name'), max_length=100,
        help_text=_('Should starts from "ftp-".'))
    host = models.ForeignKey(Host, verbose_name=_('Host'))
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Owner'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('FTP User')
        verbose_name_plural = _('FTP Users')
        unique_together = ['username', 'host']

    def __str__(self):
        return '{0}@{1}'.format(self.username, str(self.host))

    def can_remove(self):
        # TODO: have to implement
        return True

    def get_ftp_dir(self):
        return '/home/%s' % self.username


class RegisteredPort(models.Model):
    port_number = models.IntegerField(_('Port number'))
    host = models.ForeignKey(
        Host, verbose_name=_('Host'),
        related_name='registered_ports')

    class Meta:
        verbose_name = _('Registered port')
        verbose_name_plural = _('Registered ports')
