# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.passwords.mixins


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('e_password', models.TextField(blank=True)),
                ('name', models.CharField(verbose_name='Name', max_length=100)),
                ('ip_address', models.GenericIPAddressField(verbose_name='IP Address')),
                ('host_domain', models.CharField(max_length=200, verbose_name='Host domain', default='webXXX.grape-project.com')),
                ('description', models.TextField(help_text='You can use Markdown here.', verbose_name='Description', blank=True)),
                ('main_user', models.CharField(verbose_name='Main user', max_length=50)),
            ],
            options={
                'verbose_name': 'Host',
                'verbose_name_plural': 'Hosts',
                'ordering': ['name'],
            },
            bases=(apps.passwords.mixins.EncryptedMixin, models.Model),
        ),
        migrations.CreateModel(
            name='ServerType',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='Name', max_length=100)),
                ('build_script', models.TextField(verbose_name='Build script')),
            ],
            options={
                'verbose_name': 'Server type',
                'verbose_name_plural': 'Server types',
            },
        ),
        migrations.AddField(
            model_name='host',
            name='server_type',
            field=models.ForeignKey(to='hosts.ServerType', verbose_name='Server type'),
        ),
    ]
