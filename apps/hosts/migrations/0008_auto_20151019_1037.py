# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0007_auto_20151007_0955'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='tlscacert',
            field=models.TextField(blank=True, verbose_name='TLS CA Certificate', default=''),
        ),
        migrations.AddField(
            model_name='host',
            name='tlscert',
            field=models.TextField(blank=True, verbose_name='TLS Certificate', default=''),
        ),
        migrations.AddField(
            model_name='host',
            name='tlskey',
            field=models.TextField(blank=True, verbose_name='TLS Key', default=''),
        ),
    ]
