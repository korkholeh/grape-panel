# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import apps.passwords.mixins


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hosts', '0003_auto_20150812_1621'),
    ]

    operations = [
        migrations.CreateModel(
            name='FTPUser',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('e_password', models.BinaryField(blank=True)),
                ('username', models.CharField(max_length=100, verbose_name='User name')),
                ('added', models.DateTimeField(verbose_name='Added', auto_now_add=True)),
                ('host', models.ForeignKey(verbose_name='Host', to='hosts.Host')),
                ('owner', models.ForeignKey(verbose_name='Owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'FTP User',
                'verbose_name_plural': 'FTP Users',
            },
            bases=(apps.passwords.mixins.EncryptedMixin, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='ftpuser',
            unique_together=set([('username', 'host')]),
        ),
    ]
