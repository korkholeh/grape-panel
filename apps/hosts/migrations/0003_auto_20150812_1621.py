# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0002_host_is_manageable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='host',
            name='e_password',
            field=models.BinaryField(blank=True),
        ),
    ]
