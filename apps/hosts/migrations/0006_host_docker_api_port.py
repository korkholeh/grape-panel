# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0005_auto_20150820_1230'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='docker_api_port',
            field=models.PositiveIntegerField(verbose_name='Docker API port', default=2376),
        ),
    ]
