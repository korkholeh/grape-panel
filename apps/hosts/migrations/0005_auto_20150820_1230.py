# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0004_auto_20150820_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ftpuser',
            name='username',
            field=models.CharField(help_text='Should starts from "ftp-".', max_length=100, verbose_name='User name'),
        ),
    ]
