# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0006_host_docker_api_port'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegisteredPort',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('port_number', models.IntegerField(verbose_name='Port number')),
            ],
            options={
                'verbose_name': 'Registered port',
                'verbose_name_plural': 'Registered ports',
            },
        ),
        migrations.AddField(
            model_name='host',
            name='last_available_port',
            field=models.IntegerField(verbose_name='Last available port', default=32771),
        ),
        migrations.AddField(
            model_name='registeredport',
            name='host',
            field=models.ForeignKey(related_name='registered_ports', to='hosts.Host', verbose_name='Host'),
        ),
    ]
