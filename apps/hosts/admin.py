from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    ServerType,
    Host,
    FTPUser,
    RegisteredPort,
)
from .forms import (
    ServerTypeForm,
    EditHostAdminForm,
    EditFTPUserAdminForm,
)


class ServerTypeAdmin(admin.ModelAdmin):
    list_display = ['name']
    form = ServerTypeForm

admin.site.register(ServerType, ServerTypeAdmin)


class HostAdmin(admin.ModelAdmin):
    form = EditHostAdminForm
    list_display = ['name', 'ip_address', 'server_type', 'is_manageable']
    search_fields = ['name', 'ip_address']

admin.site.register(Host, HostAdmin)


class RegisteredPortAdmin(admin.ModelAdmin):
    list_display = ['port_number', 'host']
    list_filter = ['host']

admin.site.register(RegisteredPort, RegisteredPortAdmin)


class FTPUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'host', 'owner', 'added')
    raw_id_fields = ('owner',)
    form = EditFTPUserAdminForm

admin.site.register(FTPUser, FTPUserAdmin)
