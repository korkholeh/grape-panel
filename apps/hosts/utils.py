import paramiko
from .models import RegisteredPort


def register_new_port(host):
    result = host.last_available_port
    RegisteredPort.objects.create(
        port_number=host.last_available_port,
        host=host,
    )
    host.last_available_port += 1
    host.save()
    return result


def read_file(host_id, file_name):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command('sudo cat {file_name}'.format(file_name=file_name))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())
    _result = ''
    if _output:
        _result = _output[0].decode('utf-8')
        _result = '\n'.join(_result.splitlines()[2:])
    return _result


def exec_cmd_on_host(host_id, cmd, sudo=False):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    if sudo:
        session = client.get_transport().open_session()
        session.set_combine_stderr(True)
        session.get_pty()
        session.exec_command(cmd)
        stdin = session.makefile('wb', -1)
        stdout = session.makefile('rb', -1)
        stdin.write('%s\n' % host.password)
        stdin.flush()
        _output.append(stdout.read())
    else:
        stdin, stdout, stderr = client.exec_command(cmd)
        _output.append(stdout.read())
    return _output
