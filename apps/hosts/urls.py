from django.conf.urls import patterns, url
from .views import (
    HostListView,
    HostCreateView,
    HostDetailView,
    setup_script,
    mark_manageable,
    HostSSHKeysListView,
    HostSSHKeyCreateView,
    HostSSHKeyDeleteView,
    FTPUserListView,
    FTPUserCreateView,
    FTPUserDeleteView,
    ChangeFTPUserPasswordView,
    HostPluginStateListView,
)

urlpatterns = patterns(
    'hosts.views',
    url(
        r'^(?P<pk>\d+)/plugins/$',
        HostPluginStateListView.as_view(), name='host_pluginstates'),
    url(
        r'^(?P<pk>\d+)/sshkeys/$',
        HostSSHKeysListView.as_view(), name='host_sshkeys'),
    url(
        r'^(?P<pk>\d+)/sshkeys/add/$',
        HostSSHKeyCreateView.as_view(), name='host_add_sshkey'),
    url(
        r'^(?P<pk>\d+)/sshkeys/(?P<sshkey_pk>\d+)/remove/$',
        HostSSHKeyDeleteView.as_view(), name='host_remove_sshkey'),
    url(
        r'^(?P<pk>\d+)/ftpusers/$',
        FTPUserListView.as_view(), name='host_ftpusers'),
    url(
        r'^(?P<pk>\d+)/ftpusers/add/$',
        FTPUserCreateView.as_view(), name='host_add_ftpuser'),
    url(
        r'^(?P<pk>\d+)/ftpusers/(?P<ftpuser_pk>\d+)/remove/$',
        FTPUserDeleteView.as_view(), name='host_remove_ftpuser'),
    url(
        r'^(?P<pk>\d+)/ftpusers/(?P<ftpuser_pk>\d+)/change-password/$',
        ChangeFTPUserPasswordView.as_view(),
        name='host_change_ftpuser_password'),
    url(
        r'^(?P<pk>\d+)/host-setup.sh$',
        setup_script, name='host_setup_script'),
    url(
        r'^(?P<pk>\d+)/mark-manageable/$',
        mark_manageable, name='host_mark_manageable'),
    url(
        r'^(?P<pk>\d+)/$',
        HostDetailView.as_view(), name='host_detail'),
    url(r'^add/$', HostCreateView.as_view(), name='add_host'),
    url(r'^$', HostListView.as_view(), name='host_list'),
)
