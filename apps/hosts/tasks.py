import paramiko
import time
from celery import shared_task


@shared_task
def add_ssh_key_to_host(host_id, key_str):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    cmd_str = 'echo "%s" >> ~/.ssh/authorized_keys' % key_str
    stdin, stdout, stderr = client.exec_command(cmd_str)


@shared_task
def remove_ssh_key_from_host(host_id, key_str):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    cmd_str = 'sed -i \'0,/%s/{//d;}\' ~/.ssh/authorized_keys' % (
        key_str.replace('/', '\/'),
    )
    stdin, stdout, stderr = client.exec_command(cmd_str)


@shared_task
def add_ftp_user(host_id, ftp_username, ftp_password):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'sudo useradd --home /home/{user} --gid nogroup -m '
        '--shell /bin/false {user}'.format(
            user=ftp_username))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command("echo {user}:{password}|sudo chpasswd".format(
        user=ftp_username,
        password=ftp_password))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'echo "%s" | sudo tee -a '
        '/etc/vsftpd/vsftpd-virtual-user/vsftpd_user' % ftp_username)
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    user_vsftpd_file = '''
local_root=/home/{user}

cmds_allowed=USER,PASS,SYST,FEAT,OPTS,PWD,TYPE,PASV,LIST,STOR,CWD,MKD,SIZE,MDTM,CDUP,RETR,RNFR,RNTO

local_umask=022

write_enable=YES
    '''.format(user=ftp_username)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'echo "{content}" | sudo tee '
        '/etc/vsftpd/vsftpd-virtual-user/{user}'.format(
            content=user_vsftpd_file,
            user=ftp_username))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())


@shared_task
def remove_ftp_user(host_id, ftp_username):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname=host.ip_address, username=host.main_user)
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'sudo rm -f /etc/vsftpd/vsftpd-virtual-user/{user}'.format(
            user=ftp_username))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'sudo sed -i \'0,/%s/{//d;}\' '
        '/etc/vsftpd/vsftpd-virtual-user/vsftpd_user' % (
            ftp_username,
        ))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'sudo userdel {user}'.format(user=ftp_username))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())

    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command(
        'sudo rm -rf /home/{user}'.format(user=ftp_username))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())


@shared_task
def change_ftp_user_password(host_id, ftp_username, new_password):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    session.exec_command("echo {user}:{password}|sudo chpasswd".format(
        user=ftp_username,
        password=new_password))
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())


@shared_task
def save_str_as_file(host_id, content, file_name, sudo=True):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    if sudo:
        session.exec_command(
            'echo "{content}" | sudo tee '
            '{file_name}'.format(
                content=content,
                file_name=file_name))
        stdin = session.makefile('wb', -1)
        stdout = session.makefile('rb', -1)
        stdin.write('%s\n' % host.password)
        stdin.flush()
    else:
        session.exec_command(
            'echo "{content}" | tee '
            '{file_name}'.format(
                content=content,
                file_name=file_name))
        stdout = session.makefile('rb', -1)
    _output.append(stdout.read())
    print (_output)


@shared_task
def remove_directories_recursive(host_id, dir_list=[]):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    cmd = 'sudo rm -rf {dirs}'.format(dirs=' '.join(dir_list))
    session.exec_command(cmd)
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())
    return cmd


@shared_task
def remove_files(host_id, file_list=[]):
    from .models import Host
    host = Host.objects.get(pk=host_id)
    _output = []
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        hostname=host.ip_address,
        username=host.main_user,
        password=host.password)
    session = client.get_transport().open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    cmd = 'sudo rm -f {files}'.format(files=' '.join(file_list))
    session.exec_command(cmd)
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    stdin.write('%s\n' % host.password)
    stdin.flush()
    _output.append(stdout.read())
    return cmd
