from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import (
    HttpResponseForbidden,
    HttpResponseRedirect,
    HttpResponse,
)
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import RequestContext
from django.conf import settings

from .models import Host, FTPUser
from .forms import (
    CreateHostForm,
    AddFTPUserForm,
    ChangeFTPUserPasswordForm,
)
from apps.sshkeys.views import (
    SSHKeyRecordListView,
    SSHKeyRecordCreateView,
    SSHKeyRecordDeleteView,
)
from plugins.base.views import PluginStateListView


class HostListView(ListView):
    model = Host
    context_object_name = 'hosts'


class HostCreateView(CreateView):
    model = Host
    form_class = CreateHostForm

    def get_success_url(self):
        return reverse('host_list')


class HostDetailView(DetailView):
    model = Host
    context_object_name = 'host'

    def get_context_data(self, **kwargs):
        from apps.core.utils import get_manageable_ssh_key
        from apps.passwords.widgets import ShowHidePasswordWidget
        context = super().get_context_data(**kwargs)
        context.update({
            'hide_sidebar': not self.get_object().is_manageable,
            'manage_key': get_manageable_ssh_key(),
            'panel_domain': settings.HOST_PANEL_DOMAIN,
            'password_html': ShowHidePasswordWidget().render(
                'pwd', self.get_object().password, {'id': 'id_pwd'}),
        })
        return context


def setup_script(request, pk):
    host = get_object_or_404(Host, pk=pk)
    response = HttpResponse(
        host.get_build_script(),
        content_type='text/plain')
    return response


def mark_manageable(request, pk):
    host = get_object_or_404(Host, pk=pk)
    host.is_manageable = True
    host.save()
    return redirect(host.get_absolute_url())


class HostSSHKeysListView(SSHKeyRecordListView):
    template_name = 'hosts/sshkey_list.html'
    context_parent_object_name = 'host'
    add_key_url_name = 'host_add_sshkey'
    remove_key_url_name = 'host_remove_sshkey'

    def get_parent_object(self):
        return get_object_or_404(Host, pk=self.kwargs.get('pk'))


class HostSSHKeyCreateView(SSHKeyRecordCreateView):
    template_name = 'hosts/sshkey_form.html'
    context_parent_object_name = 'host'
    success_url_name = 'host_sshkeys'

    def get_parent_object(self):
        return get_object_or_404(Host, pk=self.kwargs.get('pk'))


class HostSSHKeyDeleteView(SSHKeyRecordDeleteView):
    context_parent_object_name = 'host'
    success_url_name = 'host_sshkeys'
    template_name = 'hosts/sshkey_confirm_delete.html'

    def get_parent_object(self):
        return get_object_or_404(Host, pk=self.kwargs.get('pk'))


class FTPUserListView(ListView):
    model = FTPUser
    context_object_name = 'ftpusers'

    def get_queryset(self):
        from apps.passwords.widgets import ShowHidePasswordWidget
        host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        ftpusers = FTPUser.objects.filter(host=host)
        for ftpuser in ftpusers:
            ftpuser.pwd_html = ShowHidePasswordWidget().render(
                'pwd_%s' % ftpuser.id,
                ftpuser.password, {'id': 'id_pwd_%s' % ftpuser.id})
        return ftpusers

    def get_context_data(self, **kwargs):
        context = super(
            FTPUserListView, self).get_context_data(**kwargs)
        context.update({
            'host': Host.objects.get(pk=self.kwargs.get('pk'))
        })
        return context


class FTPUserCreateView(CreateView):
    model = FTPUser
    form_class = AddFTPUserForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        ftpuser = form.save(commit=False)
        ftpuser.host = self.host
        ftpuser.owner = self.request.user
        ftpuser.password = form.cleaned_data['password']
        ftpuser.save()
        self.host.add_ftp_user(ftpuser)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('host_ftpusers', args=(self.host.pk,))


class FTPUserDeleteView(DeleteView):
    model = FTPUser

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        self.host = host
        result = get_object_or_404(
            FTPUser,
            host=host,
            pk=self.kwargs.get('ftpuser_pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.host.remove_ftp_user(self.object)
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('host_ftpusers', args=(self.host.pk,))


class ChangeFTPUserPasswordView(FormView):
    form_class = ChangeFTPUserPasswordForm
    template_name = 'hosts/ftpuser_change_password.html'

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        self.host = host
        result = get_object_or_404(
            FTPUser,
            host=host,
            pk=self.kwargs.get('ftpuser_pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        context.update({
            'host': host,
        })
        return context

    def form_valid(self, form):
        ftpuser = self.get_object()
        ftpuser.password = form.cleaned_data['password1']
        ftpuser.save()
        self.host.change_ftp_user_password(ftpuser)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('host_ftpusers', args=(self.host.pk,))


class HostPluginStateListView(PluginStateListView):
    template_name = 'hosts/pluginstates_list.html'
    context_parent_object_name = 'host'
    plugin_section = 'hosts'

    def get_parent_object(self):
        self.host = get_object_or_404(Host, pk=self.kwargs.get('pk'))
        return self.host

    def get_return_url(self):
        return reverse('host_pluginstates', args=(self.host.pk,))
