from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _
from codemirror import CodeMirrorTextarea

from .models import ServerType, Host, FTPUser
from apps.passwords.widgets import ShowHidePasswordWidget


class ServerTypeForm(forms.ModelForm):
    class Meta:
        model = ServerType
        fields = '__all__'
        widgets = {
            'build_script': CodeMirrorTextarea(
                mode="nginx", config={'fixedGutter': True}),
        }


class CreateHostForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = Host
        fields = [
            'name', 'server_type',
            'ip_address', 'host_domain',
            'description',
            'main_user',
            'tlscacert',
            'tlscert',
            'tlskey']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class EditHostAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = Host
        fields = [
            'name', 'server_type',
            'ip_address', 'host_domain',
            'description',
            'main_user',
            'is_manageable',
            'last_available_port',
            'tlscacert',
            'tlscert',
            'tlskey']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class AddFTPUserForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = FTPUser
        fields = ['username', 'password']

    def __init__(self, host, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host

    def clean_username(self):
        if not self.cleaned_data['username'].startswith('ftp-'):
            raise forms.ValidationError(
                _('Name of FTP user should starts with "ftp-".'))
        already_exist = FTPUser.objects.filter(
            host=self.host,
            username__exact=self.cleaned_data['username']).exists()

        if not already_exist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_('Such user already exists.'))


class EditFTPUserAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = FTPUser
        fields = [
            'username', 'password', 'host',
            'owner']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class ChangeFTPUserPasswordForm(forms.Form):
    password1 = forms.CharField(
        label='New password',
        widget=forms.PasswordInput())
    password2 = forms.CharField(
        label='Repeat password',
        widget=forms.PasswordInput())

    def clean_password2(self):
        if self.cleaned_data['password2'] != self.cleaned_data['password1']:
            raise forms.ValidationError(
                _('The two password fields didn\'t match.'))
