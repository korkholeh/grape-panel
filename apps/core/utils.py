from django.conf import settings


def get_manageable_ssh_key():
    if settings.MAINTAINABLE_SSH_KEY:
        return settings.MAINTAINABLE_SSH_KEY
    else:
        return open(settings.MAINTAINABLE_SSH_KEY_FILE_PATH, 'r').read()
