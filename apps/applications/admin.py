from django.contrib import admin
from .models import WebApplication


class WebApplicationAdmin(admin.ModelAdmin):
    list_display = [
        'short_description', 'name', 'host',
        'docker_image', 'owner', 'added']
    list_filter = ('host', 'owner')
    date_hierarchy = 'added'

admin.site.register(WebApplication, WebApplicationAdmin)
