from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic import CreateView, DeleteView

from .models import WebApplication
from .forms import AddWebApplicationForm
from apps.sshkeys.views import (
    SSHKeyRecordListView,
    SSHKeyRecordCreateView,
    SSHKeyRecordDeleteView,
)
from plugins.base.views import PluginStateListView


class WebApplicationListView(ListView):
    model = WebApplication
    context_object_name = 'applications'


class WebApplicationCreateView(CreateView):
    model = WebApplication
    form_class = AddWebApplicationForm
    template_name = 'applications/webapplication_add.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        from apps.containers.models import DockerImagePullingLog
        webapp = form.save(commit=False)
        docker_image = form.cleaned_data['docker_image']

        webapp.owner = self.request.user
        webapp.save()
        self.webapp = webapp

        try:
            _pulling_log = DockerImagePullingLog.objects.get(
                docker_image=docker_image,
                host=webapp.host)
            # print ('image already pulled')
            webapp_init(webapp)
            messages.success(
                self.request, _('Application is ready to use.'))
        except DockerImagePullingLog.DoesNotExist:
            # print ('image not pulled yet')
            docker_image.pull_to_host(webapp.host)
            messages.success(
                self.request,
                _('Application setup process started, please wait.'))

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('app_detail', args=(self.webapp.pk,))


def webapp_detail(request, pk):
    webapp = get_object_or_404(WebApplication, pk=pk)
    return render(request, 'applications/webapplication_detail.html', {
        'app': webapp,
        'hide_sidebar': not webapp.active,
    })


def webapp_init(webapp):
    from apps.containers.models import Container
    host = webapp.host
    docker_image = webapp.docker_image
    try:
        container = Container.objects.get(
            name__exact=webapp.name, host=host)
        if container.is_running:
            return 'ready'
        else:
            return 'processing'
    except Container.DoesNotExist:
        container = Container(
            name=webapp.name,
            host=host,
            docker_image=docker_image,
            is_running=False,
        )
        container.save()
        container.create_and_start()

        # Read Container instance from DB again because we need "meta"
        container = Container.objects.get(pk=container.pk)
        webapp.docker_container = container
        webapp.public_app_port = container.get_app_port()
        webapp.public_ssh_port = container.get_ssh_port()
        webapp.active = True
        webapp.save()

        return 'start'


def webapp_installation_log(request, pk):
    from apps.containers.models import DockerImagePullingLog
    webapp = get_object_or_404(WebApplication, pk=pk)
    host = webapp.host
    docker_image = webapp.docker_image
    pulling_log = DockerImagePullingLog.objects.get(
        host=host,
        docker_image=docker_image,
    )

    _init_result = 'none'
    _temporary_log = ''
    if pulling_log.finished:
        # Init container
        _init_result = webapp_init(webapp)
        # print (_init_result)
        if _init_result == 'ready':
            messages.success(
                request, _('Application is ready to use.'))
            return HttpResponse(
                '<script>window.location="%s";</script>' %
                reverse('app_detail', args=(webapp.pk,)))
    else:
        _temporary_log = pulling_log.get_temporary_log()

    context = {
        'temporary_log': _temporary_log,
        'image_pulling_log': pulling_log,
        'container_init': _init_result,
    }
    return render(request, 'applications/_installation_log.html', context)


def webapp_restart(request, pk):
    webapp = get_object_or_404(WebApplication, pk=pk)
    webapp.docker_container.restart()
    messages.success(request, _('Application has been restarted.'))
    return HttpResponseRedirect(reverse('app_detail', args=(webapp.pk,)))


def webapp_stop(request, pk):
    webapp = get_object_or_404(WebApplication, pk=pk)
    webapp.docker_container.stop()
    messages.success(request, _('Application has been stopped.'))
    return HttpResponseRedirect(reverse('app_detail', args=(webapp.pk,)))


def webapp_start(request, pk):
    webapp = get_object_or_404(WebApplication, pk=pk)
    webapp.docker_container.start()
    messages.success(request, _('Application has been started.'))
    return HttpResponseRedirect(reverse('app_detail', args=(webapp.pk,)))


class WebApplicationDeleteView(DeleteView):
    model = WebApplication

    def get_object(self):
        result = get_object_or_404(
            WebApplication,
            pk=self.kwargs.get('pk'))
        self.host = result.host
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.get_object(),
            'volumes':
            self.get_object().docker_container
            .get_meta_config('Volumes').items(),
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        _container = self.object.docker_container
        # print (_container.id)
        do_remove_volumes = bool(request.POST.get('remove-volumes'))
        # print (do_remove_volumes)

        # Remove SSH key records
        from django.contrib.contenttypes.models import ContentType
        from apps.sshkeys.models import SSHKeyRecord

        c_type = ContentType.objects.get_for_model(self.object)
        # print (c_type)
        sshkey_records = SSHKeyRecord.objects.filter(
            content_type=c_type,
            object_id=self.object.id,
        )
        # print (sshkey_records)
        sshkey_records.delete()

        # Remove plugin states
        from plugins.base.models import PluginState
        p_states = PluginState.objects.filter(
            content_type=c_type,
            object_id=self.object.id,
        )
        # print (p_states)
        p_states.delete()

        # Prepare volumes directories
        import os
        dir_list = list(_container.get_meta_config('Volumes').values())
        _parent_dirs = []
        for d in dir_list:
            _t = d.split('/')
            if len(_t) > 1 and _t[-1] == '_data':
                _parent_dirs.append('/'.join(_t[:-1]))
        dir_list += _parent_dirs

        host_id = _container.host_id

        # Remove container
        from apps.containers.models import Container
        from apps.containers.utils import remove_docker_container
        remove_docker_container(
            container_id=_container.id,
            force_remove=True,
        )
        _container.delete()
        # Application is removing automatically as linked object
        # self.object.delete()

        if do_remove_volumes:
            # Remove linked volumes
            from apps.hosts.tasks import remove_directories_recursive
            remove_directories_recursive.delay(
                host_id=host_id,
                dir_list=dir_list,
            )

        # Domain router service will be updated automatically
        # it is listening pre_delete signal for application
        messages.success(request, _('Application had been deleted.'))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('app_list')


class AppSSHKeysListView(SSHKeyRecordListView):
    template_name = 'applications/sshkey_list.html'
    context_parent_object_name = 'app'
    add_key_url_name = 'app_add_sshkey'
    remove_key_url_name = 'app_remove_sshkey'

    def get_parent_object(self):
        return get_object_or_404(WebApplication, pk=self.kwargs.get('pk'))


class AppSSHKeyCreateView(SSHKeyRecordCreateView):
    template_name = 'applications/sshkey_form.html'
    context_parent_object_name = 'app'
    success_url_name = 'app_sshkeys'

    def get_parent_object(self):
        return get_object_or_404(WebApplication, pk=self.kwargs.get('pk'))


class AppSSHKeyDeleteView(SSHKeyRecordDeleteView):
    context_parent_object_name = 'app'
    success_url_name = 'app_sshkeys'
    template_name = 'applications/sshkey_confirm_delete.html'

    def get_parent_object(self):
        return get_object_or_404(WebApplication, pk=self.kwargs.get('pk'))


class AppPluginStateListView(PluginStateListView):
    template_name = 'applications/pluginstates_list.html'
    context_parent_object_name = 'app'
    plugin_section = 'apps'

    def get_parent_object(self):
        self.app = get_object_or_404(WebApplication, pk=self.kwargs.get('pk'))
        return self.app

    def get_return_url(self):
        return reverse('app_pluginstates', args=(self.app.pk,))
