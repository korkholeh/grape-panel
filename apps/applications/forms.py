from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from django.conf import settings
from apps.containers.models import DockerImage
from .models import WebApplication


class AddWebApplicationForm(forms.ModelForm):
    class Meta:
        model = WebApplication
        fields = ['short_description', 'name', 'host', 'docker_image']

    def __init__(self, user, *args, **kwargs):
        from apps.hosts.models import Host
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields['host'].queryset = Host.objects.filter(is_manageable=True)
        self.fields['docker_image'].queryset = \
            DockerImage.objects.filter(container_type='app')

        try:
            max_app_id = \
                WebApplication.objects.order_by('-id').first().id + 1
        except AttributeError:
            max_app_id = 1
        self.fields['name'].initial = 'webapp_{0:02d}'.format(max_app_id)
