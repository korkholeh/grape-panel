from django.conf.urls import patterns, url
from .views import (
    WebApplicationListView,
    WebApplicationCreateView,
    WebApplicationDeleteView,
    webapp_detail,
    webapp_installation_log,
    AppSSHKeyCreateView,
    AppSSHKeysListView,
    AppSSHKeyDeleteView,
    AppPluginStateListView,
    webapp_start,
    webapp_stop,
    webapp_restart,
)


urlpatterns = patterns(
    'applications.views',
    url(
        r'^(?P<pk>\d+)/plugins/$',
        AppPluginStateListView.as_view(), name='app_pluginstates'),
    url(
        r'^(?P<pk>\d+)/sshkeys/$',
        AppSSHKeysListView.as_view(), name='app_sshkeys'),
    url(
        r'^(?P<pk>\d+)/sshkeys/add/$',
        AppSSHKeyCreateView.as_view(), name='app_add_sshkey'),
    url(
        r'^(?P<pk>\d+)/sshkeys/(?P<sshkey_pk>\d+)/remove/$',
        AppSSHKeyDeleteView.as_view(), name='app_remove_sshkey'),
    url(
        r'^(?P<pk>\d+)/installation-log/$',
        webapp_installation_log, name='app_installation_log'),
    url(
        r'^(?P<pk>\d+)/$',
        webapp_detail, name='app_detail'),
    url(r'^(?P<pk>\d+)/restart/$', webapp_restart, name='app_restart'),
    url(r'^(?P<pk>\d+)/start/$', webapp_start, name='app_start'),
    url(r'^(?P<pk>\d+)/stop/$', webapp_stop, name='app_stop'),
    url(
        r'^(?P<pk>\d+)/remove/$',
        WebApplicationDeleteView.as_view(), name='app_remove'),
    url(r'^add/$', WebApplicationCreateView.as_view(), name='add_webapp'),
    url(r'^$', WebApplicationListView.as_view(), name='app_list'),
)
