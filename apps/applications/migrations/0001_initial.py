# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0004_dockerimagepullinglog_log'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hosts', '0006_host_docker_api_port'),
    ]

    operations = [
        migrations.CreateModel(
            name='WebApplication',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('short_description', models.CharField(max_length=300, verbose_name='Short description')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='Name')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('public_app_port', models.PositiveIntegerField(verbose_name='Public app port')),
                ('public_ssh_port', models.PositiveIntegerField(null=True, blank=True, verbose_name='Public SSH port')),
                ('docker_container', models.OneToOneField(verbose_name='Container', related_name='webapps', blank=True, to='containers.Container', null=True)),
                ('host', models.ForeignKey(verbose_name='Host', related_name='webapps', to='hosts.Host')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Owner')),
            ],
            options={
                'ordering': ['short_description'],
                'verbose_name_plural': 'Web applications',
                'verbose_name': 'Web application',
            },
        ),
    ]
