# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webapplication',
            name='public_app_port',
            field=models.PositiveIntegerField(null=True, verbose_name='Public app port', blank=True),
        ),
    ]
