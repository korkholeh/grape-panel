# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0004_dockerimagepullinglog_log'),
        ('applications', '0003_webapplication_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='webapplication',
            name='docker_image',
            field=models.ForeignKey(blank=True, null=True, to='containers.DockerImage', verbose_name='Docker image'),
        ),
    ]
