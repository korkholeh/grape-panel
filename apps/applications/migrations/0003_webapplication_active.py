# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0002_auto_20150914_1410'),
    ]

    operations = [
        migrations.AddField(
            model_name='webapplication',
            name='active',
            field=models.BooleanField(verbose_name='Active', default=False),
        ),
    ]
