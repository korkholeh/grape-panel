from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
# import json


class WebApplication(models.Model):
    short_description = models.CharField(
        _('Short description'), max_length=300)
    name = models.CharField(_('Name'), max_length=100, unique=True)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_('Host'),
        related_name='webapps')
    docker_container = models.OneToOneField(
        'containers.Container', verbose_name=_('Container'),
        related_name='webapps',
        null=True, blank=True)
    docker_image = models.ForeignKey(
        'containers.DockerImage', verbose_name=_('Docker image'),
        null=True, blank=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Owner'))
    active = models.BooleanField(_('Active'), default=False)
    added = models.DateTimeField(_('Added'), auto_now_add=True)
    public_app_port = models.PositiveIntegerField(
        _('Public app port'), null=True, blank=True)
    public_ssh_port = models.PositiveIntegerField(
        _('Public SSH port'), null=True, blank=True)

    class Meta:
        verbose_name = _('Web application')
        verbose_name_plural = _('Web applications')
        ordering = ['short_description']

    def __str__(self):
        return u'{0}@{1}'.format(self.name, str(self.host))

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'app_detail',
            args=[self.pk])

    def add_ssh_key(self, key_object):
        self.docker_container.add_ssh_key(key_object)

    def remove_ssh_key(self, key_object):
        self.docker_container.remove_ssh_key(key_object)

    def get_main_domain(self):
        domains = getattr(self, 'domains', None)
        if domains:
            domains = domains.all()
            if domains:
                domain = domains.first()
                return domain.get_link()
        return ''

    def get_domains(self):
        domains = getattr(self, 'domains', None)
        if domains:
            domains = domains.all()
            return domains
        return []
