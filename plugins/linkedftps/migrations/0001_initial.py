# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0004_webapplication_docker_image'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hosts', '0008_auto_20151019_1037'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkedFTP',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('directory', models.CharField(max_length=400, verbose_name='Directory', default='webapp/ftp')),
                ('ftp_directory', models.CharField(max_length=400, verbose_name='FTP directory', default='files')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Added by')),
                ('ftpuser', models.ForeignKey(to='hosts.FTPUser', verbose_name='FTP User')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', verbose_name='Web application')),
            ],
            options={
                'verbose_name_plural': 'Linked FTPs',
                'verbose_name': 'Linked FTP',
            },
        ),
        migrations.AlterUniqueTogether(
            name='linkedftp',
            unique_together=set([('webapp', 'directory')]),
        ),
    ]
