from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class LinkedFTPsPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Linked FTPs')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 60,
                    'label': _('Linked FTPs'),
                    'url': reverse(
                        'linkedftps_index', args=(app.pk,)),
                    'itemname': 'linkedftps',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        from .views import (
            LinkedFTPCreateView,
            LinkedFTPDeleteView,
        )
        return patterns(
            'plugins.linkedftps.views',
            url(
                r'^applications/(?P<app_pk>\d+)/linkedftps/$', 'dashboard',
                name='linkedftps_index'),
            url(
                r'^applications/(?P<app_pk>\d+)/linkedftps/add/$',
                LinkedFTPCreateView.as_view(),
                name='linkedftps_add'),
            url(
                r'^applications/(?P<app_pk>\d+)/linkedftps/(?P<pk>\d+)/delete/$',
                LinkedFTPDeleteView.as_view(),
                name='linkedftps_delete'),
        )

    def get_supported_sections(self):
        return ['apps']
