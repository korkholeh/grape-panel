import os
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import pre_delete
from apps.passwords.mixins import ModelWithPassword
from apps.applications.models import WebApplication


class LinkedFTP(models.Model):
    webapp = models.ForeignKey(
        WebApplication, verbose_name=_('Web application'))
    ftpuser = models.ForeignKey(
        'hosts.FTPUser', verbose_name=_('FTP User'))
    directory = models.CharField(
        _('Directory'), max_length=400, default='webapp/ftp',
        help_text=_('Subdirectory in app container started from /home/app'))
    ftp_directory = models.CharField(
        _('FTP directory'), max_length=400, default='files',
        help_text=_('Users will see this directory when logged via FTP'))
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Added by'))
    added = models.DateTimeField(_('Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('Linked FTP')
        verbose_name_plural = _('Linked FTPs')
        unique_together = ['webapp', 'directory']

    def __str__(self):
        return '{0}::{1}//{2}'.format(
            str(self.ftpuser),
            str(self.webapp),
            self.directory,
        )

    def add_to_server(self):
        from apps.hosts.utils import exec_cmd_on_host
        app_dir = self.webapp.docker_container.get_volume_dir('/home/app')
        ftp_dir = os.path.join(
            self.ftpuser.get_ftp_dir(), self.ftp_directory)
        # Create ftp directory on host
        exec_cmd_on_host(
            self.ftpuser.host_id,
            'sudo mkdir -p %s' % ftp_dir,
            sudo=True
        )
        # Moun directory to ftp
        exec_cmd_on_host(
            self.ftpuser.host_id,
            'sudo mount --bind {linkeddir} {ftpdir}'.format(
                linkeddir=os.path.join(app_dir, self.directory),
                ftpdir=ftp_dir),
            sudo=True
        )

    def remove_from_server(self):
        from apps.hosts.utils import exec_cmd_on_host
        ftp_dir = os.path.join(
            self.ftpuser.get_ftp_dir(), self.ftp_directory)
        # Unmount directory
        result = exec_cmd_on_host(
            self.ftpuser.host_id,
            'sudo umount %s' % ftp_dir,
            sudo=True
        )
        _t = str(result[0])
        if 'device is busy' in _t:
            return False
        else:
            # Delete directory
            exec_cmd_on_host(
                self.ftpuser.host_id,
                'sudo rm -rf %s' % ftp_dir,
                sudo=True
            )
            return True
