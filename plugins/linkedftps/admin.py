from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from .models import (
    LinkedFTP,
)


class LinkedFTPAdmin(admin.ModelAdmin):
    list_display = ['directory', 'ftp_directory', 'ftpuser', 'webapp']

admin.site.register(LinkedFTP, LinkedFTPAdmin)
