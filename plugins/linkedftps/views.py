from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.views.generic import CreateView, UpdateView, DeleteView
from .models import LinkedFTP
from .forms import AddLinkedFTPForm


def dashboard(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    linked_ftps = LinkedFTP.objects.filter(webapp=app)

    context = {
        'app': app,
        'linked_ftps': linked_ftps,
    }
    return render(request, 'linkedftps/dashboard.html', context)


class LinkedFTPCreateView(CreateView):
    model = LinkedFTP
    form_class = AddLinkedFTPForm

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def form_valid(self, form):
        linkedftp = form.save(commit=False)
        linkedftp.added_by = self.request.user
        linkedftp.webapp = self.app
        linkedftp.save()
        linkedftp.add_to_server()
        messages.success(self.request, _('Linked FTP was created.'))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def get_success_url(self):
        return reverse('linkedftps_index', args=(self.app.pk,))


class LinkedFTPDeleteView(DeleteView):
    model = LinkedFTP

    def get_object(self):
        from apps.applications.models import WebApplication
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            LinkedFTP,
            webapp=self.app,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        # Unmount ftp from host
        if self.object.remove_from_server():
            messages.success(self.request, _('Linked FTP was removed.'))
        else:
            messages.warning(self.request, _('Error. FTP is busy. Please login to server via SSH and remove folder manually.'))
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('linkedftps_index', args=(self.app.pk,))
