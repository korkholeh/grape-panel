from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _

from apps.passwords.widgets import ShowHidePasswordWidget
from .models import (
    LinkedFTP,
)


class AddLinkedFTPForm(forms.ModelForm):
    class Meta:
        model = LinkedFTP
        fields = ['ftpuser', 'directory', 'ftp_directory']

    def __init__(self, app, *args, **kwargs):
        from apps.hosts.models import FTPUser
        super().__init__(*args, **kwargs)
        self.app = app
        self.fields['ftpuser'].queryset = \
            FTPUser.objects.filter(host=app.host)

    def clean_directory(self):
        _dir = self.cleaned_data['directory']
        try:
            LinkedFTP.objects.get(
                directory=_dir,
                webapp=self.app,
            )
        except LinkedFTP.DoesNotExist:
            return _dir
        raise forms.ValidationError(
            _('Another linked FTP for current directory already exists.'))

    def clean_ftp_directory(self):
        _dir = self.cleaned_data['ftp_directory']
        try:
            _ftpuser = self.cleaned_data['ftpuser']
        except KeyError:
            return _dir

        try:
            LinkedFTP.objects.get(
                ftp_directory=_dir,
                ftpuser=_ftpuser,
            )
        except LinkedFTP.DoesNotExist:
            return _dir
        raise forms.ValidationError(
            _('Another linked FTP for current user and FTP directory already exists.'))
