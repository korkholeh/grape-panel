from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages


def dashboard(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    _top = app.docker_container.top(return_result=True)

    context = {
        'app': app,
        'top_output': _top,
        'titles': _top['Titles'],
        'processes': _top['Processes'],
    }
    return render(request, 'cmdtop/dashboard.html', context)
