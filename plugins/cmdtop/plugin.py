from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class CmdTopPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Processes')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 50,
                    'label': _('Processes'),
                    'url': reverse(
                        'cmdtop_index', args=(app.pk,)),
                    'itemname': 'cmdtop',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        return patterns(
            'plugins.cmdtop.views',
            url(
                r'^applications/(?P<app_pk>\d+)/cmdtop/$', 'dashboard',
                name='cmdtop_index'),
        )

    def get_supported_sections(self):
        return ['apps']
