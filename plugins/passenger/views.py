from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages


def dashboard(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    # Get Passenger status
    passenger_status = app.docker_container.exec_cmd('passenger-status')

    context = {
        'app': app,
        'passenger_status': passenger_status,
    }
    return render(request, 'passenger/dashboard.html', context)


def restart_app(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    # Restart passenger app
    result = app.docker_container.exec_cmd(
        'passenger-config restart-app --name /home/app/webapp/public')

    messages.success(request, _('Application was restarted'))
    return HttpResponseRedirect(reverse('passenger_index', args=(app.pk,)))
