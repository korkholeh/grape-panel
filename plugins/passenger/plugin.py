from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class PassengerPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Passenger')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 40,
                    'label': _('Passenger'),
                    'url': reverse(
                        'passenger_index', args=(app.pk,)),
                    'itemname': 'passenger',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        return patterns(
            'plugins.passenger.views',
            url(
                r'^applications/(?P<app_pk>\d+)/passenger/$', 'dashboard',
                name='passenger_index'),
            url(
                r'^applications/(?P<app_pk>\d+)/passenger/restart-app/$',
                'restart_app',
                name='passenger_restart_app'),
        )

    def get_supported_sections(self):
        return ['apps']
