from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.views.generic.list import ListView
from django.contrib import messages
from .models import PluginState
from .utils import get_plugins, load_object


class PluginStateListView(ListView):
    model = PluginState
    context_object_name = 'plugin_states'
    context_parent_object_name = 'p_object'
    plugin_section = ''

    def get_parent_object(self):
        pass

    def get_parent_object_info(self):
        from django.contrib.contenttypes.models import ContentType
        p_object = self.get_parent_object()
        p_type = ContentType.objects.get_for_model(p_object)
        return p_type, p_object.id

    def get_queryset(self):
        (c_type, o_id) = self.get_parent_object_info()
        return PluginState.objects.filter(
            content_type=c_type,
            object_id=o_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        p_object = self.get_parent_object()
        p_object_info = self.get_parent_object_info()
        plugin_states = PluginState.objects.filter(
            content_type=p_object_info[0],
            object_id=p_object.pk,
        )
        enabled_plugins_classes = plugin_states.values_list(
            'plugin_class', flat=True)

        all_plugins = get_plugins()
        plugins = []
        for p in all_plugins:
            _t = p(request=self.request, context=context)
            if self.plugin_section in _t.get_supported_sections():
                _t.class_name = _t.__module__ + '.' + _t.__class__.__name__
                if _t.class_name not in enabled_plugins_classes:
                    plugins.append(_t)

        context.update({
            self.context_parent_object_name: p_object,
            'p_object': p_object,
            'p_object_info': p_object_info,
            'plugins': plugins,
            'return_url': self.get_return_url(),
        })
        return context

    def get_return_url(self):
        pass


def enable_plugin_for_object(request, app_label, obj_name, obj_id):
    from django.contrib.contenttypes.models import ContentType
    plugin_class_name = request.GET.get('plugin_class')
    return_url = request.GET.get('return_url')
    if plugin_class_name in settings.INSTALLED_PLUGINS:
        p_object_type = ContentType.objects.get(
            app_label=app_label, model=obj_name.lower())
        p_object_model = p_object_type.model_class()
        p_object = get_object_or_404(p_object_model, pk=obj_id)

        try:
            plugin_state = PluginState.objects.get(
                content_type=p_object_type,
                object_id=p_object.pk,
                plugin_class=plugin_class_name,
            )
            messages.error(request, _('Plugin is already enabled.'))
            return HttpResponseRedirect(return_url)
        except PluginState.DoesNotExist:
            plugin_state = PluginState(
                content_object=p_object,
                plugin_class=plugin_class_name,
                added_by=request.user,
            )
            plugin_state.save()
            messages.success(request, _('Plugin is enabled successfully.'))
    else:
        messages.error(request, _('Incorrect plugin class name.'))

    return HttpResponseRedirect(return_url)


def disable_plugin_for_object(request, app_label, obj_name, obj_id):
    from django.contrib.contenttypes.models import ContentType
    plugin_class_name = request.GET.get('plugin_class')
    return_url = request.GET.get('return_url')
    if plugin_class_name in settings.INSTALLED_PLUGINS:
        p_object_type = ContentType.objects.get(
            app_label=app_label, model=obj_name.lower())
        p_object_model = p_object_type.model_class()
        p_object = get_object_or_404(p_object_model, pk=obj_id)

        try:
            plugin_state = PluginState.objects.get(
                content_type=p_object_type,
                object_id=p_object.pk,
                plugin_class=plugin_class_name,
            )
            plugin_state.delete()
            messages.success(request, _('Plugin is disabled.'))
            return HttpResponseRedirect(return_url)
        except PluginState.DoesNotExist:
            messages.error(request, _('Cannot disable the plugin.'))
    else:
        messages.error(request, _('Incorrect plugin class name.'))

    return HttpResponseRedirect(return_url)
