from django.conf.urls import patterns, url
from .views import (
    enable_plugin_for_object,
    disable_plugin_for_object,
)


urlpatterns = patterns(
    'base.views',
    url(
        r'^(?P<app_label>[\w\d]+)/(?P<obj_name>[\w\d]+)/'
        r'(?P<obj_id>\d+)/enable/$',
        enable_plugin_for_object, name='plugin_enable'),
    url(
        r'^(?P<app_label>[\w\d]+)/(?P<obj_name>[\w\d]+)/'
        r'(?P<obj_id>\d+)/disable/$',
        disable_plugin_for_object, name='plugin_disable'),
)
