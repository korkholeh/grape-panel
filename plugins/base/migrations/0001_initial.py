# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PluginState',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('object_id', models.PositiveIntegerField()),
                ('plugin_class', models.CharField(verbose_name='Plugin class', max_length=255)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Added by')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name_plural': 'Plugin State',
                'verbose_name': 'Plugin State',
            },
        ),
    ]
