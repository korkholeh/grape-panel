from django.conf import settings


def load_object(name):
    import importlib
    _t = name.split('.')
    _module = '.'.join(_t[:-1])
    _object = _t[-1]
    mod = importlib.import_module(_module)
    return getattr(mod, _object)


def init_plugins():
    for p_class in settings.INSTALLED_PLUGINS:
        p = load_object(p_class)
        p()


def update_plugins_urls(patterns):
    _patterns = patterns
    for p_class in settings.INSTALLED_PLUGINS:
        _p = load_object(p_class)
        p = _p()
        _p_patterns = p.get_urls()
        if _p_patterns:
            _patterns += _p_patterns
    return _patterns


def get_plugins():
    result = []
    for p_class in settings.INSTALLED_PLUGINS:
        p = load_object(p_class)
        result.append(p)
    return result


def get_enabled_plugins_for(p_object):
    from django.contrib.contenttypes.models import ContentType
    from .models import PluginState
    p_type = ContentType.objects.get_for_model(p_object)
    plugin_states = PluginState.objects.filter(
        content_type=p_type,
        object_id=p_object.pk,
    )
    enabled_plugins_classes = plugin_states.values_list(
        'plugin_class', flat=True)

    result = []
    for p_class in settings.INSTALLED_PLUGINS:
        if p_class in enabled_plugins_classes:
            p = load_object(p_class)
            result.append(p)
    return result
