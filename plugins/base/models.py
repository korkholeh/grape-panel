from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from .utils import load_object


class PluginState(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    plugin_class = models.CharField(
        _('Plugin class'), max_length=255)
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('Plugin State')
        verbose_name_plural = _('Plugin State')

    def __str__(self):
        return str(self.plugin_class)

    def get_plugin_name(self):
        _result = 'None'
        if self.plugin_class in settings.INSTALLED_PLUGINS:
            p = load_object(self.plugin_class)
            _result = p().get_name()
        return _result
