from django import template
from ..utils import get_plugins, get_enabled_plugins_for

register = template.Library()


@register.inclusion_tag(
    'plugins/init_menu.html', takes_context=True)
def init_menu(context, menu, p_object=None):
    request = context['request']
    items = []
    if p_object:
        plugins = get_enabled_plugins_for(p_object)
    else:
        plugins = get_plugins()
    for p in plugins:
        p_obj = p(request=request, context=context)
        _menu_info = p_obj.get_menu_info()
        if _menu_info:
            new_items = list(_menu_info)
            for i in new_items:
                if i['menu'] == menu:
                    items.append(i)
    return {
        'items': items,
    }
