from apps.containers.models import DockerImage, ImageTag


class BasicPlugin:
    def __init__(self, **kwargs):
        pass

    def get_name(self):
        return 'Not specified'

    def get_menu_info(self):
        pass

    def search_docker_images(self, tags):
        image_tags = ImageTag.objects.filter(tag__in=tags)
        images = DockerImage.objects.filter(tags=image_tags)
        return images

    def install(self, host):
        pass

    def get_urls(self):
        pass

    def get_supported_sections(self):
        pass
