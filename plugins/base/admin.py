from django.contrib import admin
from .models import PluginState


class PluginStateAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'plugin_class', 'added_by', 'added']

admin.site.register(PluginState, PluginStateAdmin)
