from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import pre_delete
from apps.passwords.mixins import ModelWithPassword
from apps.applications.models import WebApplication


class HAProxyService(models.Model):
    host = models.OneToOneField(
        'hosts.Host', verbose_name=_('Host'),
        related_name='haproxy')
    docker_container = models.OneToOneField(
        'containers.Container', verbose_name=_('Container'),
        related_name='haproxy',
        null=True, blank=True)
    active = models.BooleanField(_('Active'), default=False)

    class Meta:
        verbose_name = _('HAProxy Service')
        verbose_name_plural = _('HAProxy Services')

    def __str__(self):
        return str(self.host)

    def get_config_auth_rule(self, domain):
        _result = ''
        if domain.auth_user_list and not domain.auth_ip_excludes:
            _result += \
                '    acl AuthOkay_{userlist} http_auth(UsersFor_{userlist})\n'\
                '    http-request auth realm {userlist} if !AuthOkay_{userlist}\n'\
                .format(userlist=domain.auth_user_list.name)
        elif domain.auth_user_list and domain.auth_ip_excludes:
            _result += \
                '    acl AuthOkay_{userlist} http_auth(UsersFor_{userlist})\n'\
                '    acl IPList_{iplist} src {ipexcludes}\n'\
                '    http-request auth realm {userlist} if !IPList_{iplist} !AuthOkay_{userlist}\n'\
                .format(
                    userlist=domain.auth_user_list.name,
                    iplist=domain.auth_ip_excludes.name,
                    ipexcludes=domain.auth_ip_excludes.get_ips_as_str(delimiter=' '),
                )
        elif domain.auth_ip_excludes and not domain.auth_user_list:
            _result += \
                '    acl IPList_{iplist} src {ipexcludes}\n'\
                '    http-request deny if !IPList_{iplist}\n'\
                .format(
                    iplist=domain.auth_ip_excludes.name,
                    ipexcludes=domain.auth_ip_excludes.get_ips_as_str(delimiter=' '),
                )
        return _result

    def get_config(self):
        # Basic config
        _config = '''
global
  log 127.0.0.1 local0
  log 127.0.0.1 local1 notice
  chroot /var/lib/haproxy
  user haproxy
  group haproxy
  # daemon

defaults
  log global
  mode http
  option httplog
  option dontlognull
  timeout connect 5000ms
  timeout client 50000ms
  timeout server 50000ms
  errorfile 400 /etc/haproxy/errors/400.http
  errorfile 403 /etc/haproxy/errors/403.http
  errorfile 408 /etc/haproxy/errors/408.http
  errorfile 500 /etc/haproxy/errors/500.http
  errorfile 502 /etc/haproxy/errors/502.http
  errorfile 503 /etc/haproxy/errors/503.http
  errorfile 504 /etc/haproxy/errors/504.http
'''

        # User lists and users
        users_def = ''
        for user_list in self.get_user_lists():
            users = user_list.get_users()
            userlist_def = ''
            if users.count() > 0:
                userlist_def += '''
userlist UsersFor_{userlist}
'''.format(userlist=user_list.name)
                for u in users:
                    userlist_def += '    user {user} '\
                        'insecure-password {password}\n'.format(
                            user=u.username, password=u.password)
                users_def += userlist_def
        _config += users_def

        # HTTP domains
        http_domains = self.domains.filter(protocol__exact='http')
        _http_frontend = '''
frontend http-in
    bind *:80
    mode http

    # Define hosts
'''
        for i in http_domains:
            if i.redirect_to_https:
                _http_frontend += \
                    '    redirect scheme https if {{ hdr(Host) -i {domain} }}'\
                    ' !{{ ssl_fc }}\n'\
                    .format(domain=i.domain)
            elif i.redirect_to:
                _http_frontend += \
                    '    redirect prefix {redirect} code {code} if '\
                    '{{ hdr(host) -i {domain} }}'\
                    .format(
                        redirect=i.redirect_to.get_link(),
                        code=i.redirect_code,
                        domain=i.domain,
                    )
            else:
                _http_frontend += '    acl {name} hdr(host) -i {domain}\n'\
                    .format(
                        name='host_%s' % i.pk,
                        domain=i.domain,
                    )

        _http_frontend += '\n    ## figure out which one to use\n'
        for i in http_domains:
            if not i.redirect_to_https and i.redirect_to is None:
                _http_frontend += '    use_backend {cluster} if {name}\n'\
                    .format(
                        cluster='cluster_%s' % i.pk,
                        name='host_%s' % i.pk,
                    )

        _config += _http_frontend

        # HTTPS domains
        https_domains = self.domains.filter(
            protocol__exact='https').exclude(ssl_certificate=None)
        # Certs list
        certs = ''
        for i in https_domains:
            # Save files to the /root/cert folder
            certs += ' crt {pemfile}'\
                .format(
                    pemfile=i.ssl_certificate.get_file_name(full=True),
                )

        _https_frontend = '''
frontend https-in
    bind *:443 ssl{certs}
    mode http

    # Define hosts
'''.format(certs=certs)

        if https_domains:
            for i in https_domains:
                _https_frontend += '    use_backend {cluster} if '\
                    '{{ ssl_fc_sni {domain} }}\n'\
                    .format(
                        cluster='https_cluster_%s' % i.pk,
                        domain=i.domain,
                    )
            _config += _https_frontend

        # HTTP backends
        _http_backends = ''
        for i in http_domains:
            if i.redirect_to_https or i.redirect_to is not None:
                continue

            _http_backend = '''
backend {cluster}
    balance leastconn
    option httpclose
    option forwardfor
    cookie JSESSIONID prefix
'''.format(cluster='cluster_%s' % i.pk)
            # Check for HTTP auth
            _http_backend += self.get_config_auth_rule(i)

            for webapp in i.webapps.all():
                _http_backend += '    server {node} {addr} cookie A check\n'\
                    .format(
                        node='web%s' % webapp.pk,
                        addr=webapp.docker_container.get_app_ip_and_port(),
                    )
            _http_backends += _http_backend
        _config += _http_backends

        # HTTPS backends
        _https_backends = ''
        for i in https_domains:
            _https_backend = '''
backend {cluster}
    balance leastconn
    option httpclose
    option forwardfor
    cookie JSESSIONID prefix
'''.format(cluster='https_cluster_%s' % i.pk)
            for webapp in i.webapps.all():
                # Check for HTTP auth
                _https_backend += self.get_config_auth_rule(i)

                _https_backend += '    server {node} {addr} cookie A check\n'\
                    .format(
                        node='web%s' % webapp.pk,
                        addr=webapp.docker_container.get_app_ip_and_port(),
                    )
            _https_backends += _https_backend
        _config += _https_backends


        _t = '''
listen stats :80
  stats enable
  stats uri /
'''
        return _config

    def restart(self):
        """Restart a HAProxy docker container with 7 seconds delay.
        """
        self.docker_container.restart(countdown=7)

    def update_config(self):
        from apps.hosts.tasks import save_str_as_file
        import os
        _config = self.get_config()
        _cfg_dir = self.docker_container.get_volume_dir('/etc/haproxy')
        # Run task synchronously
        save_str_as_file.apply(args=[
            self.host_id,
            _config,
            os.path.join(_cfg_dir, 'haproxy.cfg')])
        self.restart()

    def get_user_lists(self):
        return self.user_lists.all().order_by('name')

    def get_ip_lists(self):
        return self.ip_lists.all().order_by('name')


class SSLCertificate(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    haproxy = models.ForeignKey(
        HAProxyService,
        verbose_name=_('HAProxy Service'),
        related_name='certificates')
    crt_file = models.TextField(_('CRT file'))
    key_file = models.TextField(_('KEY file'))

    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('SSL Certificate')
        verbose_name_plural = _('SSL Certificates')
        unique_together = ('name', 'haproxy')

    def __str__(self):
        return self.name

    def get_pem_file(self):
        return '%s\n%s' % (self.crt_file, self.key_file)

    def get_file_name(self, full=False):
        import os
        fn = 'server_%s.pem' % self.id
        if full:
            _cfg_dir = '/etc/haproxy'
            return os.path.join(_cfg_dir, fn)
        else:
            return fn

    def push_to_server(self):
        from apps.hosts.tasks import save_str_as_file
        import os
        _content = self.get_pem_file()
        _cfg_dir = self.haproxy.docker_container.get_volume_dir(
            '/etc/haproxy')
        # Run task synchronously
        save_str_as_file.apply(args=[
            self.haproxy.host_id,
            _content,
            os.path.join(_cfg_dir, self.get_file_name())])

    def remove_from_server(self):
        from apps.hosts.tasks import remove_files
        import os
        _cfg_dir = self.haproxy.docker_container.get_volume_dir(
            '/etc/haproxy')
        # Run task synchronously
        remove_files.apply(args=[
            self.haproxy.host_id,
            [os.path.join(_cfg_dir, self.get_file_name())]])


class Domain(models.Model):
    PROTOCOLS = (
        ('http', 'HTTP'),
        ('https', 'HTTPS'),
    )
    REDIRECT_CODES = (
        (301, _('301 - Moved permanently')),
        (302, _('302 - Found')),
        (303, _('303 - See Other (since HTTP/1.1)')),
        (307, _('307 - Temporary Redirect (since HTTP/1.1)')),
        (308, _('308 - Permanent Redirect (RFC 7538)')),
    )

    domain = models.CharField(_('Domain'), max_length=100)
    protocol = models.CharField(
        _('Protocol'),
        max_length=20, choices=PROTOCOLS, default='http')
    webapps = models.ManyToManyField(
        WebApplication,
        verbose_name=_('Applications'),
        related_name='domains')
    haproxy = models.ForeignKey(
        HAProxyService,
        null=True, blank=True,
        verbose_name=_('HAProxy Service'),
        related_name='domains')
    is_default = models.BooleanField(
        _('Is default'), default=False,
        help_text=_('Only one domain for the app can be default.'))
    ssl_certificate = models.ForeignKey(
        SSLCertificate,
        verbose_name=_('SSL Certificate'),
        null=True, blank=True,
        related_name='domains',
    )

    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    # Redirection
    redirect_to = models.ForeignKey(
        'self', verbose_name=_('Redirect to'),
        related_name='redirect_from',
        blank=True, null=True,
    )
    redirect_code = models.IntegerField(
        _('Redirect code'),
        default=302, choices=REDIRECT_CODES,
    )
    redirect_to_https = models.BooleanField(
        _('Redirect to HTTPS'), blank=True, default=False,
    )

    # HTTP auth
    auth_user_list = models.ForeignKey(
        'HAProxyAuthUserList',
        verbose_name=_('Auth user list'),
        related_name='domains',
        blank=True, null=True,
    )
    auth_ip_excludes = models.ForeignKey(
        'HAProxyIPList',
        verbose_name=_('Auth IP excludes'),
        related_name='domains',
        blank=True, null=True,
    )

    class Meta:
        verbose_name = _('Domain')
        verbose_name_plural = _('Domains')

    def __str__(self):
        return self.get_link()

    def use_balancer(self):
        return self.webapps.all().count() > 1

    def get_link(self):
        return '%s://%s' % (self.protocol, self.domain)


class HAProxyAuthUserList(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    haproxy = models.ForeignKey(
        HAProxyService,
        verbose_name=_('HAProxy Service'),
        related_name='user_lists')

    class Meta:
        verbose_name = _('HAProxy auth user list')
        verbose_name_plural = _('HAProxy auth user lists')

    def __str__(self):
        return '%s:%s' % (self.name, str(self.haproxy))

    def get_users(self):
        from apps.passwords.widgets import ShowHidePasswordWidget
        result = self.users.all().order_by('username')
        for u in result:
            u.pwd_html = ShowHidePasswordWidget().render(
                'pwd_%s' % u.id,
                u.password, {'id': 'id_pwd_%s' % u.id})
        return result


class HAProxyAuthUser(ModelWithPassword):
    user_list = models.ForeignKey(
        HAProxyAuthUserList,
        verbose_name=_('User list'),
        related_name='users')
    username = models.CharField(_('Username'), max_length=100)

    class Meta:
        verbose_name = _('HAProxy auth user')
        verbose_name_plural = _('HAProxy auth users')

    def __str__(self):
        return self.username


class HAProxyIPList(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    haproxy = models.ForeignKey(
        HAProxyService,
        verbose_name=_('HAProxy Service'),
        related_name='ip_lists')
    ip_list = models.TextField(
        _('IP list'),
        help_text=_('Use separate line for different IPs'))

    class Meta:
        verbose_name = _('HAProxy IP list')
        verbose_name_plural = _('HAProxy IP lists')

    def __str__(self):
        return '%s:%s' % (self.name, str(self.haproxy))

    def get_ips_as_str(self, delimiter=', '):
        ips = self.ip_list.splitlines()
        return delimiter.join(ips)


# Signals listeners

@receiver(pre_delete, sender=WebApplication)
def update_domains_after_app_deleting(sender, **kwargs):
    # print ('SIGNAL')
    domains = kwargs['instance'].domains.all()
    haproxy_ids = []
    for d in domains:
        # print (d)
        haproxy_ids.append(d.haproxy_id)
        webapps = d.webapps.all()
        # print (webapps)
        # Delete domain record if sender is an only app which is linked with it
        if webapps.count() == 1 and webapps.first() == kwargs['instance']:
            d.delete()
    # print (haproxy_ids)
    # Update HAProxy configs
    haproxy_ids = list(set(haproxy_ids))
    for h_id in haproxy_ids:
        haproxy = HAProxyService.objects.get(id=h_id)
        haproxy.update_config()
