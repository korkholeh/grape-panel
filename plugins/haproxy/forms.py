from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _

from apps.passwords.widgets import ShowHidePasswordWidget
from .models import (
    Domain,
    SSLCertificate,
    HAProxyAuthUserList,
    HAProxyAuthUser,
    HAProxyIPList,
)


class AddDomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = ['haproxy', 'domain', 'protocol']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app


class ChooseSSLSertificateForDomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = ['ssl_certificate']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        self.fields['ssl_certificate'].queryset = \
            SSLCertificate.objects.filter(
                haproxy=kwargs['instance'].haproxy,
            )


class RedirectForDomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = ['redirect_to', 'redirect_code', 'redirect_to_https']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        instance = kwargs['instance']
        try:
            _https_variant = app.domains.get(
                protocol='https',
                domain=instance.domain,
            )
            del self.fields['redirect_to']
            del self.fields['redirect_code']
        except Domain.DoesNotExist:
            del self.fields['redirect_to_https']
            self.fields['redirect_to'].queryset = \
                app.domains.exclude(pk=instance.pk)


class AuthUserListForDomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = ['auth_user_list', 'auth_ip_excludes']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        instance = kwargs['instance']
        self.fields['auth_user_list'].queryset = \
            instance.haproxy.get_user_lists()
        self.fields['auth_ip_excludes'].queryset = \
            instance.haproxy.get_ip_lists()


class AddSSLCertificateForm(forms.ModelForm):
    class Meta:
        model = SSLCertificate
        fields = ['name', 'crt_file', 'key_file']

    def __init__(self, host, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host


class HAProxyAuthUserListForm(forms.ModelForm):
    class Meta:
        model = HAProxyAuthUserList
        fields = ['name']

    def __init__(self, host, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host


class AddHAProxyAuthUserForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = HAProxyAuthUser
        fields = ['username', 'password']

    def __init__(self, host, userlist, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host
        self.userlist = userlist


class HAProxyAuthUserAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = HAProxyAuthUser
        fields = ['username', 'user_list']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class HAProxyIPListForm(forms.ModelForm):
    class Meta:
        model = HAProxyIPList
        fields = ['name', 'ip_list']

    def __init__(self, host, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host
