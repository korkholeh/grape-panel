from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from .models import (
    HAProxyService, Domain, SSLCertificate,
    HAProxyAuthUserList,
    HAProxyAuthUser,
    HAProxyIPList,
)
from .forms import HAProxyAuthUserAdminForm


class HAProxyServiceAdmin(admin.ModelAdmin):
    list_display = ['host', 'docker_container', 'active']
    actions = ['update_config']

    def update_config_action(self, request, queryset):
        for q in queryset:
            q.update_config()
    update_config_action.short_description = _('Update config')


class DomainAdmin(admin.ModelAdmin):
    list_display = [
        'domain', 'is_default', 'protocol',
        'ssl_certificate',
        'haproxy', 'added_by']
    list_filter = ('haproxy', 'protocol')


class SSLCertificateAdmin(admin.ModelAdmin):
    list_display = ['name', 'haproxy', 'added_by']


class HAProxyAuthUserListAdmin(admin.ModelAdmin):
    list_display = ['name', 'haproxy']


class HAProxyAuthUserAdmin(admin.ModelAdmin):
    form = HAProxyAuthUserAdminForm
    list_display = ['username', 'user_list']


class HAProxyIPListAdmin(admin.ModelAdmin):
    list_display = ['name', 'haproxy']


admin.site.register(HAProxyService, HAProxyServiceAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(SSLCertificate, SSLCertificateAdmin)
admin.site.register(HAProxyAuthUserList, HAProxyAuthUserListAdmin)
admin.site.register(HAProxyAuthUser, HAProxyAuthUserAdmin)
admin.site.register(HAProxyIPList, HAProxyIPListAdmin)
