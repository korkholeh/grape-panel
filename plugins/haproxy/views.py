from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView, DeleteView

from apps.hosts.models import Host
from .models import (
    HAProxyService, Domain, SSLCertificate,
    HAProxyAuthUserList,
    HAProxyAuthUser,
    HAProxyIPList,
)
from .forms import (
    AddDomainForm,
    AddSSLCertificateForm,
    ChooseSSLSertificateForDomainForm,
    RedirectForDomainForm,
    HAProxyAuthUserListForm,
    AddHAProxyAuthUserForm,
    AuthUserListForDomainForm,
    HAProxyIPListForm,
)


DOCKER_IMAGE_NAME = 'korkholeh/haproxy:0.1'
CONTAINER_NAME = 'haproxy'


def plugin_not_installed(request, host_pk, haproxy_service=None):
    host = get_object_or_404(Host, pk=host_pk)
    return render(request, 'haproxy/not_installed.html', {
        'host': host,
        'haproxy_service': haproxy_service,
    })


def plugin_dashboard(request, host_pk, haproxy_service):
    host = get_object_or_404(Host, pk=host_pk)

    ssl_certs = SSLCertificate.objects.filter(haproxy=haproxy_service)
    return render(request, 'haproxy/dashboard.html', {
        'host': host,
        'haproxy': haproxy_service,
        'ssl_certs': ssl_certs,
    })


def plugin_index(request, host_pk):
    try:
        haproxy_service = HAProxyService.objects.get(host_id=host_pk)
        if haproxy_service.active:
            return plugin_dashboard(request, host_pk, haproxy_service)
        else:
            return plugin_not_installed(request, host_pk, haproxy_service)
    except HAProxyService.DoesNotExist:
        return plugin_not_installed(request, host_pk)


def start_install(request, host_pk):
    from apps.containers.models import ImageTag, DockerImage, ExposedPort
    from apps.hosts.models import Host

    host = get_object_or_404(Host, pk=host_pk)
    try:
        haproxy_service = HAProxyService.objects.get(host_id=host_pk)
        messages.error(
            request, _('Cannot start installation process.'))
    except HAProxyService.DoesNotExist:
        try:
            docker_image = DockerImage.objects.get(
                image_name__exact=DOCKER_IMAGE_NAME)
        except DockerImage.DoesNotExist:
            docker_image = DockerImage(
                name='HAProxy',
                image_name=DOCKER_IMAGE_NAME,
                repository=DOCKER_IMAGE_NAME,
                description='Docker image for HAProxy routing and balancing',
                container_type='service',
            )
            docker_image.save()
            tag = ImageTag(docker_image=docker_image, tag='haproxy')
            tag.save()
            port80 = ExposedPort(
                docker_image=docker_image,
                number=80,
                expose_to='80')
            port80.save()
            port443 = ExposedPort(
                docker_image=docker_image,
                number=443,
                expose_to='443')
            port443.save()
            port22 = ExposedPort(
                docker_image=docker_image,
                number=22)
            port22.save()

        # Create HAProxy item to DB
        haproxy_service = HAProxyService(host=host)
        haproxy_service.save()

        # Start the installation process
        # Pull the Docker image from Docker Hub
        docker_image.pull_to_host(host)

        messages.success(
            request,
            _('Plugin installation process started, please wait.'))
    return HttpResponseRedirect(reverse('haproxy_index', args=(host_pk,)))


def init_container(host_pk):
    from apps.containers.models import DockerImage, Container
    from apps.hosts.models import Host
    host = get_object_or_404(Host, pk=host_pk)
    docker_image = DockerImage.objects.get(
        image_name__exact=DOCKER_IMAGE_NAME)
    try:
        container = Container.objects.get(
            name__exact=CONTAINER_NAME, host=host)
        if container.is_running:
            return 'ready'
        else:
            return 'processing'
    except Container.DoesNotExist:
        container = Container(
            name=CONTAINER_NAME,
            host=host,
            docker_image=docker_image,
            is_running=False,
        )
        container.save()
        container.create_and_start()

        try:
            haproxy_service = HAProxyService.objects.get(host_id=host_pk)
            haproxy_service.docker_container = container
            haproxy_service.active = True
            haproxy_service.save()
        except HAProxyService.DoesNotExist:
            return 'error'

        return 'start'


def installation_log(request, host_pk):
    from apps.containers.models import DockerImage, DockerImagePullingLog
    from apps.hosts.models import Host
    host = get_object_or_404(Host, pk=host_pk)
    docker_image = DockerImage.objects.get(image_name__exact=DOCKER_IMAGE_NAME)
    pulling_log = DockerImagePullingLog.objects.get(
        host=host,
        docker_image=docker_image,
    )

    _init_result = 'none'
    _temporary_log = ''
    if pulling_log.finished:
        # Init container
        _init_result = init_container(host_pk)
        # print (_init_result)
        if _init_result == 'ready':
            messages.success(
                request, _('HAProxy service is ready to use.'))
            return HttpResponse(
                '<script>window.location="%s";</script>' %
                reverse('haproxy_index', args=(host_pk,)))
    else:
        _temporary_log = pulling_log.get_temporary_log()

    context = {
        'temporary_log': _temporary_log,
        'image_pulling_log': pulling_log,
        'container_init': _init_result,
    }
    return render(request, 'haproxy/_installation_log.html', context)


def save_haproxy_config(request, host_pk):
    host = get_object_or_404(Host, pk=host_pk)
    haproxy_service = HAProxyService.objects.get(host_id=host_pk)
    haproxy_service.update_config()
    return HttpResponseRedirect(reverse('haproxy_index', args=(host.pk,)))


# Applications section

def domains_list(request, app_pk):
    from apps.applications.models import WebApplication
    app = get_object_or_404(WebApplication, pk=app_pk)
    domains = app.domains.all()
    show_balancer = False
    for d in domains:
        if d.use_balancer():
            show_balancer = True
        if d.protocol == 'http':
            try:
                _https_domain = app.domains.get(
                    protocol='https', domain=d.domain)
                d.has_https_variant = True
            except Domain.DoesNotExist:
                pass

    context = {
        'app': app,
        'domains': domains,
        'show_balancer': show_balancer,
    }
    return render(request, 'haproxy/domains_list.html', context)


class DomainCreateView(CreateView):
    model = Domain
    form_class = AddDomainForm

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def form_valid(self, form):
        try:
            domain = Domain.objects.get(
                haproxy_id=form.cleaned_data['haproxy'],
                domain=form.cleaned_data['domain'],
                protocol=form.cleaned_data['protocol'])
            # Already exists. Add webapp to existing domain record.
            domain.webapps.add(self.app)

            # Update HAProxy config
            haproxy = domain.haproxy
            haproxy.update_config()

            return HttpResponseRedirect(self.get_success_url())
        except Domain.DoesNotExist:
            # Not exist yet. Create new domain record.
            domain = form.save(commit=False)
            domain.added_by = self.request.user
            domain.save()
            domain.webapps.add(self.app)

            # Update HAProxy config
            haproxy = domain.haproxy
            haproxy.update_config()

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_domains_list', args=(self.app.pk,))


class DomainDeleteView(DeleteView):
    model = Domain

    def get_object(self):
        from apps.applications.models import WebApplication
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            Domain,
            webapps=self.app,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        haproxy = self.object.haproxy
        if self.app in self.object.webapps.all():
            self.object.webapps.remove(self.app)

        if not self.object.webapps.all():
            self.object.delete()
        haproxy.update_config()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('haproxy_domains_list', args=(self.app.pk,))


class DomainSSLCertificateUpdateView(UpdateView):
    model = Domain
    form_class = ChooseSSLSertificateForDomainForm

    def get_object(self):
        from apps.applications.models import WebApplication
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            Domain,
            webapps=self.app,
            pk=self.kwargs.get('pk'))
        return result

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def form_valid(self, form):
        domain = form.save()
        domain.haproxy.update_config()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('haproxy_domains_list', args=(self.app.pk,))


class DomainRedirectUpdateView(UpdateView):
    model = Domain
    form_class = RedirectForDomainForm

    def get_object(self):
        from apps.applications.models import WebApplication
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            Domain,
            webapps=self.app,
            pk=self.kwargs.get('pk'))
        return result

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def form_valid(self, form):
        domain = form.save()
        domain.haproxy.update_config()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('haproxy_domains_list', args=(self.app.pk,))


class DomainAuthUserListUpdateView(UpdateView):
    model = Domain
    form_class = AuthUserListForDomainForm

    def get_object(self):
        from apps.applications.models import WebApplication
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            Domain,
            webapps=self.app,
            pk=self.kwargs.get('pk'))
        return result

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def form_valid(self, form):
        domain = form.save()
        domain.haproxy.update_config()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('haproxy_domains_list', args=(self.app.pk,))


class SSLCertificateCreateView(CreateView):
    model = SSLCertificate
    form_class = AddSSLCertificateForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        cert = form.save(commit=False)
        cert.haproxy = self.haproxy_service
        cert.added_by = self.request.user
        cert.save()
        cert.push_to_server()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class SSLCertificateDeleteView(DeleteView):
    model = SSLCertificate

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            SSLCertificate,
            haproxy__host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        haproxy = self.object.haproxy
        # Delete SSL certificate .pem file from server
        self.object.remove_from_server()
        self.object.delete()
        haproxy.update_config()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


def push_ssl_certificate(request, host_pk, pk):
    host = get_object_or_404(Host, pk=host_pk)
    cert = get_object_or_404(
        SSLCertificate,
        haproxy__host=host,
        pk=pk)
    cert.push_to_server()
    return HttpResponseRedirect(
        reverse('haproxy_index', args=(host.pk,)))


# User lists

class HAProxyAuthUserListCreateView(CreateView):
    model = HAProxyAuthUserList
    form_class = HAProxyAuthUserListForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        userlist = form.save(commit=False)
        userlist.haproxy = self.haproxy_service
        userlist.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyAuthUserListUpdateView(UpdateView):
    model = HAProxyAuthUserList
    form_class = HAProxyAuthUserListForm

    def get_object(self):
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        result = get_object_or_404(
            HAProxyAuthUserList,
            haproxy=self.haproxy_service,
            pk=self.kwargs.get('pk'))
        return result

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        userlist = form.save(commit=False)
        userlist.haproxy = self.haproxy_service
        userlist.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyAuthUserListDeleteView(DeleteView):
    model = HAProxyAuthUserList

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            HAProxyAuthUserList,
            haproxy__host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyAuthUserCreateView(CreateView):
    model = HAProxyAuthUser
    form_class = AddHAProxyAuthUserForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        self.userlist = HAProxyAuthUserList(
            haproxy=self.haproxy_service,
            pk=self.kwargs.get('pk'),
        )
        kwargs['userlist'] = self.userlist
        return kwargs

    def form_valid(self, form):
        user = form.save(commit=False)
        user.user_list = self.userlist
        user.password = form.cleaned_data['password']
        user.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyAuthUserDeleteView(DeleteView):
    model = HAProxyAuthUser

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            HAProxyAuthUser,
            user_list__haproxy__host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


# IP lists

class HAProxyIPListCreateView(CreateView):
    model = HAProxyIPList
    form_class = HAProxyIPListForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        iplist = form.save(commit=False)
        iplist.haproxy = self.haproxy_service
        iplist.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyIPListUpdateView(UpdateView):
    model = HAProxyIPList
    form_class = HAProxyIPListForm

    def get_object(self):
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        result = get_object_or_404(
            HAProxyIPList,
            haproxy=self.haproxy_service,
            pk=self.kwargs.get('pk'))
        return result

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(
            Host, pk=self.kwargs.get('host_pk'))
        self.haproxy_service = HAProxyService.objects.get(host=self.host)
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        iplist = form.save(commit=False)
        iplist.haproxy = self.haproxy_service
        iplist.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))


class HAProxyIPListDeleteView(DeleteView):
    model = HAProxyIPList

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            HAProxyIPList,
            haproxy__host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('haproxy_index', args=(self.host.pk,))
