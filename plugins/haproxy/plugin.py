from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class HAProxyPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('HAProxy')

    def get_menu_info(self):
        host = self.context.get('host')
        app = self.context.get('app')
        if host:
            return (
                {
                    'menu': 'hosts', 'position': 10,
                    'label': _('HAProxy'),
                    'url': reverse('haproxy_index', args=(host.pk,)),
                    'itemname': 'haproxies',
                },
            )
        elif app:
            return (
                {
                    'menu': 'apps', 'position': 10,
                    'label': _('Domains'),
                    'url': reverse('haproxy_domains_list', args=(app.pk,)),
                    'itemname': 'domains',
                },
            )
        else:
            return tuple()

    def install(self, host):
        images = self.search_docker_images('haproxy')
        # print (images)

    def get_urls(self):
        from .views import (
            DomainCreateView,
            DomainDeleteView,
            DomainSSLCertificateUpdateView,
            DomainRedirectUpdateView,
            DomainAuthUserListUpdateView,
            SSLCertificateCreateView,
            SSLCertificateDeleteView,
            HAProxyAuthUserListCreateView,
            HAProxyAuthUserListUpdateView,
            HAProxyAuthUserListDeleteView,
            HAProxyAuthUserCreateView,
            HAProxyAuthUserDeleteView,
            HAProxyIPListCreateView,
            HAProxyIPListUpdateView,
            HAProxyIPListDeleteView,
        )
        return patterns(
            'plugins.haproxy.views',
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/$', 'plugin_index',
                name='haproxy_index'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/iplists/add/$',
                HAProxyIPListCreateView.as_view(),
                name='haproxy_add_ip_list'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/iplists/(?P<pk>\d+)/edit/$',
                HAProxyIPListUpdateView.as_view(),
                name='haproxy_edit_ip_list'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/iplists/(?P<pk>\d+)/remove/$',
                HAProxyIPListDeleteView.as_view(),
                name='haproxy_ip_list_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/userlists/add/$',
                HAProxyAuthUserListCreateView.as_view(),
                name='haproxy_add_user_list'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/userlists/(?P<pk>\d+)/edit/$',
                HAProxyAuthUserListUpdateView.as_view(),
                name='haproxy_edit_user_list'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/userlists/(?P<pk>\d+)/remove/$',
                HAProxyAuthUserListDeleteView.as_view(),
                name='haproxy_user_list_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/userlists/(?P<pk>\d+)/add-user/$',
                HAProxyAuthUserCreateView.as_view(),
                name='haproxy_add_user'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/users/(?P<pk>\d+)/remove/$',
                HAProxyAuthUserDeleteView.as_view(),
                name='haproxy_user_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/ssl-certs/add/$',
                SSLCertificateCreateView.as_view(),
                name='haproxy_add_ssl_certificate'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/ssl-certs/(?P<pk>\d+)/remove/$',
                SSLCertificateDeleteView.as_view(),
                name='haproxy_ssl_certificate_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/ssl-certs/(?P<pk>\d+)/push/$',
                'push_ssl_certificate',
                name='haproxy_ssl_certificate_push'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/save-config/$',
                'save_haproxy_config',
                name='haproxy_save_config'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/install/$', 'start_install',
                name='haproxy_install'),
            url(
                r'^hosts/(?P<host_pk>\d+)/haproxy/installation-log/$',
                'installation_log',
                name='haproxy_installation_log'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/$', 'domains_list',
                name='haproxy_domains_list'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/(?P<pk>\d+)/delete/$',
                DomainDeleteView.as_view(),
                name='haproxy_domain_delete'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/(?P<pk>\d+)/ssl-cert/$',
                DomainSSLCertificateUpdateView.as_view(),
                name='haproxy_domain_ssl'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/(?P<pk>\d+)/redirect/$',
                DomainRedirectUpdateView.as_view(),
                name='haproxy_domain_redirect'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/(?P<pk>\d+)/user-list/$',
                DomainAuthUserListUpdateView.as_view(),
                name='haproxy_domain_user_list'),
            url(
                r'^applications/(?P<app_pk>\d+)/domains/add/$',
                DomainCreateView.as_view(),
                name='haproxy_domain_add'),
        )

    def get_supported_sections(self):
        return ['hosts', 'apps']
