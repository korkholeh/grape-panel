# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0010_auto_20151105_1511'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='auth_user_list',
            field=models.ForeignKey(verbose_name='Auth user list', blank=True, to='haproxy.HAProxyAuthUserList', related_name='domains', null=True),
        ),
    ]
