# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0004_webapplication_docker_image'),
        ('haproxy', '0002_domain'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='domain',
            name='webapp',
        ),
        migrations.AddField(
            model_name='domain',
            name='webapps',
            field=models.ManyToManyField(verbose_name='Applications', to='applications.WebApplication', related_name='domains'),
        ),
    ]
