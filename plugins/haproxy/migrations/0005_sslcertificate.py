# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('haproxy', '0004_auto_20151005_1340'),
    ]

    operations = [
        migrations.CreateModel(
            name='SSLCertificate',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(verbose_name='Name', max_length=255)),
                ('crt_file', models.TextField(verbose_name='CRT file')),
                ('key_file', models.TextField(verbose_name='KEY file')),
                ('added', models.DateTimeField(verbose_name='Added', auto_now_add=True)),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Added by')),
            ],
            options={
                'verbose_name_plural': 'SSL Certificates',
                'verbose_name': 'SSL Certificate',
            },
        ),
    ]
