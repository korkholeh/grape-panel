# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0011_domain_auth_user_list'),
    ]

    operations = [
        migrations.CreateModel(
            name='HAProxyIPList',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('ip_list', models.TextField(verbose_name='IP list', help_text='Use separate line for different IPs')),
                ('haproxy', models.ForeignKey(to='haproxy.HAProxyService', related_name='ip_lists', verbose_name='HAProxy Service')),
            ],
            options={
                'verbose_name_plural': 'HAProxy IP lists',
                'verbose_name': 'HAProxy IP list',
            },
        ),
    ]
