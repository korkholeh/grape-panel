# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.passwords.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0009_auto_20151103_1311'),
    ]

    operations = [
        migrations.CreateModel(
            name='HAProxyAuthUser',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('e_password', models.BinaryField(blank=True)),
                ('username', models.CharField(verbose_name='Username', max_length=100)),
            ],
            options={
                'verbose_name_plural': 'HAProxy auth users',
                'verbose_name': 'HAProxy auth user',
            },
            bases=(apps.passwords.mixins.EncryptedMixin, models.Model),
        ),
        migrations.CreateModel(
            name='HAProxyAuthUserList',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(verbose_name='Name', max_length=100)),
                ('haproxy', models.ForeignKey(verbose_name='HAProxy Service', to='haproxy.HAProxyService', related_name='user_lists')),
            ],
            options={
                'verbose_name_plural': 'HAProxy auth user lists',
                'verbose_name': 'HAProxy auth user list',
            },
        ),
        migrations.AlterField(
            model_name='domain',
            name='redirect_code',
            field=models.IntegerField(default=302, choices=[(301, '301 - Moved permanently'), (302, '302 - Found'), (303, '303 - See Other (since HTTP/1.1)'), (307, '307 - Temporary Redirect (since HTTP/1.1)'), (308, '308 - Permanent Redirect (RFC 7538)')], verbose_name='Redirect code'),
        ),
        migrations.AddField(
            model_name='haproxyauthuser',
            name='user_list',
            field=models.ForeignKey(verbose_name='User list', to='haproxy.HAProxyAuthUserList', related_name='users'),
        ),
    ]
