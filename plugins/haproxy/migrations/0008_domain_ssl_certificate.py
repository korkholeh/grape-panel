# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0007_auto_20151012_1219'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='ssl_certificate',
            field=models.ForeignKey(related_name='domains', to='haproxy.SSLCertificate', null=True, verbose_name='SSL Certificate', blank=True),
        ),
    ]
