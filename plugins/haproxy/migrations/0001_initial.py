# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0005_auto_20150820_1230'),
        ('containers', '0002_auto_20150825_1327'),
    ]

    operations = [
        migrations.CreateModel(
            name='HAProxyService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('active', models.BooleanField(verbose_name='Active', default=False)),
                ('docker_container', models.OneToOneField(related_name='haproxy', to='containers.Container', null=True, blank=True, verbose_name='Container')),
                ('host', models.OneToOneField(related_name='haproxy', to='hosts.Host', verbose_name='Host')),
            ],
            options={
                'verbose_name': 'HAProxy Service',
                'verbose_name_plural': 'HAProxy Services',
            },
        ),
    ]
