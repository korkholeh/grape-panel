# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0005_sslcertificate'),
    ]

    operations = [
        migrations.AddField(
            model_name='sslcertificate',
            name='haproxy',
            field=models.ForeignKey(related_name='certificates', null=True, verbose_name='HAProxy Service', blank=True, to='haproxy.HAProxyService'),
        ),
        migrations.AlterUniqueTogether(
            name='sslcertificate',
            unique_together=set([('name', 'haproxy')]),
        ),
    ]
