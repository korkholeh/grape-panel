# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0012_haproxyiplist'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='auth_ip_excludes',
            field=models.ForeignKey(to='haproxy.HAProxyIPList', blank=True, related_name='domains', null=True, verbose_name='Auth IP excludes'),
        ),
    ]
