# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0004_webapplication_docker_image'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('haproxy', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('domain', models.CharField(unique=True, max_length=100, verbose_name='Domain')),
                ('protocol', models.CharField(choices=[('http', 'HTTP'), ('https', 'HTTPS')], max_length=20, default='http', verbose_name='Protocol')),
                ('is_default', models.BooleanField(help_text='Only one domain for the pp can be default.', default=False, verbose_name='Is default')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Added by')),
                ('haproxy', models.ForeignKey(to='haproxy.HAProxyService', blank=True, related_name='domains', null=True)),
                ('webapp', models.ForeignKey(to='applications.WebApplication', verbose_name='Applications', related_name='domains')),
            ],
            options={
                'verbose_name_plural': 'Domains',
                'verbose_name': 'Domain',
            },
        ),
    ]
