# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0006_auto_20151012_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sslcertificate',
            name='haproxy',
            field=models.ForeignKey(default=1, to='haproxy.HAProxyService', verbose_name='HAProxy Service', related_name='certificates'),
            preserve_default=False,
        ),
    ]
