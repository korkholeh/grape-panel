# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0003_auto_20150921_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='domain',
            field=models.CharField(verbose_name='Domain', max_length=100),
        ),
        migrations.AlterField(
            model_name='domain',
            name='haproxy',
            field=models.ForeignKey(verbose_name='HAProxy Service', to='haproxy.HAProxyService', null=True, blank=True, related_name='domains'),
        ),
        migrations.AlterField(
            model_name='domain',
            name='is_default',
            field=models.BooleanField(help_text='Only one domain for the app can be default.', verbose_name='Is default', default=False),
        ),
    ]
