# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('haproxy', '0008_domain_ssl_certificate'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='redirect_code',
            field=models.IntegerField(default=302, verbose_name='Redirect code', choices=[(301, '301 - Moved permanently'), (302, '302 - Found'), (303, '303 See Other (since HTTP/1.1)'), (307, '307 - Temporary Redirect (since HTTP/1.1)'), (308, '308 - Permanent Redirect (RFC 7538)')]),
        ),
        migrations.AddField(
            model_name='domain',
            name='redirect_to',
            field=models.ForeignKey(null=True, blank=True, to='haproxy.Domain', related_name='redirect_from', verbose_name='Redirect to'),
        ),
        migrations.AddField(
            model_name='domain',
            name='redirect_to_https',
            field=models.BooleanField(default=False, verbose_name='Redirect to HTTPS'),
        ),
    ]
