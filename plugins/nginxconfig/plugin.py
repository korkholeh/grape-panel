from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class NginxConfigPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Nginx config')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 60,
                    'label': _('Nginx config'),
                    'url': reverse(
                        'nginxconfig_config', args=(app.pk,)),
                    'itemname': 'nginxconfig',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        return patterns(
            'plugins.nginxconfig.views',
            url(
                r'^applications/(?P<app_pk>\d+)/nginxconfig/$',
                'nginxconfig_config',
                name='nginxconfig_config'),
            url(
                r'^applications/(?P<app_pk>\d+)/nginxconfig/restart-nginx/$',
                'restart_nginx',
                name='nginxconfig_restart_nginx'),
        )

    def get_supported_sections(self):
        return ['apps']
