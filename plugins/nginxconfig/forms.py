from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _
from codemirror import CodeMirrorTextarea


class NginxConfigForm(forms.Form):
    nginx_config = forms.CharField(
        label='webapp.conf',
        required=True,
        widget=CodeMirrorTextarea(
            mode="shell", config={'fixedGutter': True}),
    )
