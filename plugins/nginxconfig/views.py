from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from .forms import NginxConfigForm


def nginxconfig_config(request, app_pk):
    import os
    import re
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    _dir = app.docker_container.get_volume_dir('/etc/nginx')
    filename = os.path.join(_dir, 'sites-enabled', 'webapp.conf')

    if request.method == 'POST':
        form = NginxConfigForm(request.POST)
        if form.is_valid():
            _configfile = form.cleaned_data['nginx_config']
            _configfile = '\n'.join(_configfile.splitlines())
            save_str_as_file.apply(args=[
                app.host_id, _configfile, filename])
            messages.success(request, _('Successfully saved. Press restart.'))
            return HttpResponseRedirect(
                reverse('nginxconfig_config', args=(app.pk,)))

    _configfile = read_file(app.host_id, filename)
    form = NginxConfigForm(initial={
        'nginx_config': _configfile,
    })

    context = {
        'app': app,
        'form': form,
    }
    return render(request, 'nginxconfig/edit_config.html', context)


def restart_nginx(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    # Restart Nginx
    result = app.docker_container.exec_cmd('sv restart nginx')

    messages.success(request, _('Nginx was restarted'))
    return HttpResponseRedirect(reverse('nginxconfig_config', args=(app.pk,)))
