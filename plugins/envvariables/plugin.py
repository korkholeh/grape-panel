from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class EnvVariablesPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Config variables')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 30,
                    'label': _('Config variables'),
                    'url': reverse(
                        'envvariables_list', args=(app.pk,)),
                    'itemname': 'envvariables',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        return patterns(
            'plugins.envvariables.views',
            url(
                r'^applications/(?P<app_pk>\d+)/variables/$', 'variables_list',
                name='envvariables_list'),
        )

    def get_supported_sections(self):
        return ['apps']
