from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages


def variables_list(request, app_pk):
    import os
    import re
    from apps.applications.models import WebApplication
    from apps.hosts.utils import read_file
    from apps.hosts.tasks import save_str_as_file
    app = get_object_or_404(WebApplication, pk=app_pk)

    _dir = app.docker_container.get_volume_dir('/etc/profile.d')
    variables = []

    if request.method == 'POST' and _dir:
        _envfile = ''
        for k in request.POST.keys():
            if k != 'csrfmiddlewaretoken':
                _envfile += 'export {varname}={value}\n'\
                    .format(
                        varname=k,
                        value=request.POST[k],
                    )
        save_str_as_file.apply(args=[
            app.host_id, _envfile, os.path.join(_dir, 'webapp.sh')])
        messages.success(request, _('Successfully saved'))
        return HttpResponseRedirect(
            reverse('envvariables_list', args=(app.pk,)))

    if _dir:
        PATTERN = r'export (?P<varname>\w+)=(?P<value>.*)'
        regex = re.compile(PATTERN)
        _envfile = read_file(app.host_id, os.path.join(_dir, 'webapp.sh'))
        _lines = _envfile.splitlines()
        for i in _lines:
            matches = regex.match(i)
            params = matches.groupdict()
            variables.append(params)

    context = {
        'has_support': _dir is not None,
        'app': app,
        'variables': variables,
    }
    return render(request, 'envvariables/variables_list.html', context)
