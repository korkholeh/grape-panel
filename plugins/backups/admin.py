from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import BackupSettings
from .forms import BackupSettingsAdminForm


class BackupSettingsAdmin(admin.ModelAdmin):
    form = BackupSettingsAdminForm
    list_display = ['webapp', 'schedule', 'last_update']
    actions = [
        'init_duply_action',
        'save_duply_action',
        'update_duply_status_action',
    ]

    def init_duply_action(self, request, queryset):
        for q in queryset:
            q.init_duply_config()
    init_duply_action.short_description = _('Init Duply config')

    def save_duply_action(self, request, queryset):
        for q in queryset:
            q.save_config()
    save_duply_action.short_description = _('Save Duply config')

    def update_duply_status_action(self, request, queryset):
        for q in queryset:
            q.update_status()
    update_duply_status_action.short_description = _('Update Duply status')

admin.site.register(BackupSettings, BackupSettingsAdmin)
