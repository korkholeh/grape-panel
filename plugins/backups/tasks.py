from celery import shared_task
from celery.decorators import periodic_task
from celery.schedules import crontab


@shared_task
def start_webapp_backup(webapp_id):
    from .models import BackupSettings
    b_settings = BackupSettings.objects.get(webapp_id=webapp_id)
    result = b_settings.start_backup_cmd()
    return result


@periodic_task(
    run_every=(crontab(hour="0,4,8,12,16,20", minute="30", day_of_week="*")))
def update_backup_statuses():
    """Update backup statuses. Each 4 hours.
    """
    from .models import BackupSettings
    b_settings = BackupSettings.objects.exclude(
        schedule__exact='never',
    )
    _result = []
    for b in b_settings:
        _result.append(b.update_status())
    return _result


@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="Sat")))
def weekly_backups():
    """Weekly backups. Every Saturday.
    """
    from .models import BackupSettings
    b_settings = BackupSettings.objects.filter(
        schedule__exact='weekly',
    )
    _result = []
    for b in b_settings:
        _result.append(b.start_backup_cmd())
    return _result


@periodic_task(run_every=(crontab(hour="0", minute="10", day_of_week="*")))
def daily_backups():
    """Daily backups. Every night at 00:10.
    """
    from .models import BackupSettings
    b_settings = BackupSettings.objects.filter(
        schedule__exact='daily',
    )
    _result = []
    for b in b_settings:
        _result.append(b.start_backup_cmd())
    return _result
