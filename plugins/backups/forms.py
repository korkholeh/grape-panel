from django import forms
from django.utils.translation import ugettext_lazy as _
from apps.passwords.widgets import ShowHidePasswordWidget
from .models import BackupSettings


class BackupSettingsAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = BackupSettings
        fields = [
            'webapp', 'passphrase',
            'target',
            'username',
            'password',
            'max_age',
            'volsize',
            'max_full_backups',
            'max_full_backup_age',
            's3_european_bucket',
            's3_use_new_style',
            'duply_status',
            'schedule',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class EditBackupSettingsForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = BackupSettings
        fields = [
            'target',
            'username',
            'password',
            'max_age',
            'volsize',
            'max_full_backups',
            'max_full_backup_age',
            's3_european_bucket',
            's3_use_new_style',
            'schedule',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password
                self.fields['password'].widget = ShowHidePasswordWidget()
