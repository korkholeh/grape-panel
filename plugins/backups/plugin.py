from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class BackupPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('Backups')

    def get_menu_info(self):
        app = self.context.get('app')
        if app:
            return (
                {
                    'menu': 'apps', 'position': 60,
                    'label': _('Backups'),
                    'url': reverse(
                        'backups_index', args=(app.pk,)),
                    'itemname': 'backups',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        return patterns(
            'plugins.backups.views',
            url(
                r'^applications/(?P<app_pk>\d+)/backups/$', 'dashboard',
                name='backups_index'),
            url(
                r'^applications/(?P<app_pk>\d+)/backups/configure/$',
                'configure_backups',
                name='backups_configure'),
            url(
                r'^applications/(?P<app_pk>\d+)/backups/start-backup/$',
                'start_duply_backup',
                name='backups_start'),
            url(
                r'^applications/(?P<app_pk>\d+)/backups/duply-config/$',
                'download_duply_config',
                name='backups_duply_config'),
            url(
                r'^applications/(?P<app_pk>\d+)/backups/rewrite-config/$',
                'rewrite_duply_config',
                name='backups_rewrite_config'),
        )

    def get_supported_sections(self):
        return ['apps']
