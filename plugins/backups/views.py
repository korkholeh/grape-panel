from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from .forms import EditBackupSettingsForm

BACKUPS_ROOT = '/home/webprod/backups'


def dashboard(request, app_pk):
    from apps.applications.models import WebApplication
    app = get_object_or_404(WebApplication, pk=app_pk)

    if hasattr(app, 'backup_settings'):
        backup_settings = app.backup_settings
    else:
        backup_settings = None

    context = {
        'app': app,
        'backup_settings': backup_settings,
    }
    return render(request, 'backups/dashboard.html', context)


def configure_backups(request, app_pk):
    import os
    from apps.applications.models import WebApplication
    app = get_object_or_404(WebApplication, pk=app_pk)

    if hasattr(app, 'backup_settings'):
        form = EditBackupSettingsForm(instance=app.backup_settings)
    else:
        form = EditBackupSettingsForm()

    if request.method == 'POST':
        if hasattr(app, 'backup_settings'):
            form = EditBackupSettingsForm(
                data=request.POST,
                instance=app.backup_settings)
        else:
            form = EditBackupSettingsForm(request.POST)

        if form.is_valid():
            _bs = form.save(commit=False)
            _bs.webapp = app
            _bs.password = form.cleaned_data['password']
            _bs.save()
            return redirect(reverse('backups_index', args=(app.pk,)))

    return render(
        request, 'backups/configure.html',
        {
            'app': app,
            'form': form,
        })


def download_duply_config(request, app_pk):
    import os
    from apps.applications.models import WebApplication
    app = get_object_or_404(WebApplication, pk=app_pk)

    if hasattr(app, 'backup_settings'):
        config = app.backup_settings.get_duply_config()
        return HttpResponse(config, content_type='text/plain')
    else:
        return redirect(reverse('backups_index', args=(app.pk,)))


def rewrite_duply_config(request, app_pk):
    import os
    from apps.applications.models import WebApplication
    app = get_object_or_404(WebApplication, pk=app_pk)

    if hasattr(app, 'backup_settings'):
        config = app.backup_settings.save_config()
        return redirect(reverse('backups_index', args=(app.pk,)))
    else:
        return redirect(reverse('backups_index', args=(app.pk,)))


def start_duply_backup(request, app_pk):
    import os
    from apps.applications.models import WebApplication
    from .tasks import start_webapp_backup
    app = get_object_or_404(WebApplication, pk=app_pk)
    start_webapp_backup.delay(app.pk)
    messages.success(
        request,
        _('Task for start application backup has been started.')
    )
    return redirect(reverse('backups_index', args=(app.pk,)))
