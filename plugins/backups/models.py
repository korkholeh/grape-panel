import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils.crypto import get_random_string
from apps.passwords.mixins import ModelWithPassword
from apps.applications.models import WebApplication


def get_backup_passphrase():
    return get_random_string(
        50, "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)")


class BackupSettings(ModelWithPassword):
    BACKUP_SCHEDULE_CHOICES = (
        ('never', _('Never')),
        ('daily', _('Daily')),
        ('weekly', _('Weekly')),
    )
    MAX_AGE_CHOICES = (
        ('1W', _('1 Week')),
        ('2W', _('2 Weeks')),
        ('3W', _('3 Weeks')),
        ('1M', _('1 Month')),
        ('2M', _('2 Months')),
        ('3M', _('3 Months')),
        ('6M', _('6 Months')),
        ('1Y', _('1 Year')),
        ('2Y', _('2 Years')),
    )

    webapp = models.OneToOneField(
        WebApplication,
        verbose_name=_('Web application'),
        related_name='backup_settings')
    passphrase = models.CharField(
        _('Encription passphrase'),
        max_length=100,
        default=get_backup_passphrase,
        help_text=_(
            'All your backups will be encripted with this passphrase.'))
    target = models.CharField(
        _('Target'),
        max_length=255,
        default=
        's3://s3-<region endpoint name>.amazonaws.com/'
        '<bucket name>/<folder name>',
        help_text=_('Specify where you want to store your backups.'))
    username = models.CharField(_('Target user'), max_length=100)

    max_age = models.CharField(
        _('Max age'), max_length=20, default='2M',
        choices=MAX_AGE_CHOICES,
        help_text=_('Time frame for old backups to keep.'))
    volsize = models.IntegerField(
        _('Volume size'), default=25,
        help_text=_('Volume size in MB'))

    max_full_backups = models.IntegerField(
        _('Max full backups'), default=4,
        help_text=_('Number of full backups to keep.'))
    max_full_backup_age = models.CharField(
        _('Max full backup age'), max_length=20, default='1M',
        choices=MAX_AGE_CHOICES,
        help_text=_(
            'Forces a full backup if last full backup '
            'reaches a specified age.'))
    s3_european_bucket = models.BooleanField(
        _('S3 european bucket'), default=True, blank=True,
    )
    s3_use_new_style = models.BooleanField(
        _('S3 new style auth'), default=True, blank=True,
    )

    duply_status = models.TextField(
        _('Duply status'), blank=True, default='')
    last_update = models.DateTimeField(_('Last update'), auto_now=True)

    schedule = models.CharField(
        _('Schedule'), default='never', max_length=50,
        choices=BACKUP_SCHEDULE_CHOICES)

    class Meta:
        verbose_name = _('Backup settings')
        verbose_name_plural = _('Backup settings')

    def __str__(self):
        return str(self.webapp)

    def get_duply_config_file_name(self, filename='conf'):
        import os
        return os.path.join(
            '/home',
            self.webapp.host.main_user,
            '.duply',
            self.webapp.name,
            filename)

    def get_extra_backup_directory(self):
        import os
        return os.path.join(
            '/home',
            self.webapp.host.main_user,
            'backups',
            self.webapp.name)

    def get_creation_cmd(self):
        return 'sudo duply %s create' % self.webapp.name

    def get_status_cmd(self):
        return 'sudo duply %s status' % self.webapp.name

    def get_backup_cmd(self):
        return 'sudo duply %s backup' % self.webapp.name

    def get_restore_to_dir_cmd(self):
        import os
        return 'sudo duply %s restore %s' % (
            self.webapp.name,
            os.path.join(
                '/home',
                self.webapp.host.main_user,
                'tmp',
                self.webapp.name),
        )

    def update_status(self):
        self.duply_status = self.backup_status_cmd()
        self.save()

    def backup_status_cmd(self):
        from apps.hosts.utils import exec_cmd_on_host
        result = exec_cmd_on_host(
            self.webapp.host_id,
            self.get_status_cmd(),
            sudo=True)
        result = '\n'.join(str(result[0], 'utf-8').splitlines()[2:])
        return result

    def start_backup_cmd(self):
        from apps.hosts.utils import exec_cmd_on_host
        result = exec_cmd_on_host(
            self.webapp.host_id,
            self.get_backup_cmd(),
            sudo=True)
        result = '\n'.join(str(result[0], 'utf-8').splitlines()[2:])
        return result

    def get_backup_status_as_obj(self):
        import re
        _result = {}

        # Get last status check time
        w = re.search(
            r'--- Start running command STATUS at (?P<time>.+) ---',
            self.duply_status,
        )
        _result['last_status_check'] = w.groups() if w else ''

        # Metadata sync status
        w = re.search(
            r'Local and Remote metadata are synchronized, no sync needed.',
            self.duply_status,
        )
        _result['matadata_are_synchronized'] = bool(w)

        # Last full backup date
        w = re.search(
            r'Last full backup date: (?P<date>.+)'+'\n',
            self.duply_status,
        )
        _result['last_full_backup'] = w.groups() if w else ''

        # Backup sets
        w = re.search(
            r'Number of contained backup sets: (?P<num>\d+)'+'\n',
            self.duply_status,
        )
        _result['backup_sets_count'] = int(w.groups()[0]) if w else 0
        _result['backup_sets'] = []
        _lines = self.duply_status.splitlines()
        _table_start = -1
        for (i, s) in enumerate(_lines):
            if s.startswith(' Type of backup set:'):
                _table_start = i
                break
        for i in range(_table_start+1, len(_lines)):
            if _lines[i].startswith('-------------------------'):
                break
            else:
                _decoded_bset = list(
                    map(
                        lambda x: x.strip(),
                        filter(bool, _lines[i].split('  '))
                    )
                )
                _result['backup_sets'].append(_decoded_bset)

        return _result

    def get_duply_config(self):
        from apps.hosts.utils import read_file
        filename = self.get_duply_config_file_name()
        config = read_file(self.webapp.host_id, filename)
        return config

    def save_config(self):
        from apps.hosts.tasks import save_str_as_file
        # Save main config
        save_str_as_file.apply(args=[
            self.webapp.host_id,
            self.compile_config(),
            self.get_duply_config_file_name(),
            False,
        ])
        # Save excludes
        save_str_as_file.apply(args=[
            self.webapp.host_id,
            self.compile_exclude(),
            self.get_duply_config_file_name(filename='exclude'),
            False,
        ])
        # Save pre script
        save_str_as_file.apply(args=[
            self.webapp.host_id,
            self.compile_pre_script(),
            self.get_duply_config_file_name(filename='pre'),
            False,
        ])
        # Save post script
        save_str_as_file.apply(args=[
            self.webapp.host_id,
            self.compile_post_script(),
            self.get_duply_config_file_name(filename='post'),
            False,
        ])

    def init_duply_config(self):
        result = self.webapp.host.exec_cmd(
            self.get_creation_cmd())
        self.save_config()
        return result

    def compile_pre_script(self):
        import os
        from plugins.postgresql.models import (
            PostgreSQLDatabase,
            PostgreSQLUser,
        )

        config = ''
        backup_root = self.get_extra_backup_directory()

        pg_dbs = PostgreSQLDatabase.objects.filter(
            webapp=self.webapp,
        )
        for pg_db in pg_dbs:
            config += "/bin/mkdir -p {broot}/{dbname}".format(
                broot=backup_root,
                dbname=pg_db.name,
            )
            pg_user = PostgreSQLUser.objects.get(
                username=pg_db.username, pg_service=pg_db.pg_service)

            config += '''
/usr/bin/pg_dump \\"dbname={dbname} host={host} user={user} password={pwd} port={port}\\" > {dir}/db-dump.sql
'''.format(
                dbname=pg_db.name,
                host=pg_db.pg_service.host.ip_address,
                user=pg_db.username,
                pwd=pg_user.password,
                port=pg_db.pg_service.get_public_pg_port(),
                dir=os.path.join(backup_root, pg_db.name)
            )
        return config

    def compile_post_script(self):
        import os
        from plugins.postgresql.models import PostgreSQLDatabase

        config = ''
        backup_root = self.get_extra_backup_directory()

        pg_dbs = PostgreSQLDatabase.objects.filter(
            webapp=self.webapp,
        )
        for pg_db in pg_dbs:
            config += '/bin/rm -rf {dir}\n'.format(
                dir=os.path.join(backup_root, pg_db.name),
            )
        return config

    def compile_config(self):
        config = ''
        # Passphrase
        config += "GPG_PW='%s'\n" % self.passphrase

        # Target
        config += """
TARGET='{target}'
TARGET_USER='{user}'
TARGET_PASS='{password}'
""".format(target=self.target, user=self.username, password=self.password)

        # Source
        # NOTE: the real sources are described in excludes
        config += "SOURCE='/'\n"

        # Max age
        config += "MAX_AGE={0}\n".format(self.max_age)
        config += """
MAX_FULL_BACKUPS={max_full_backups}
MAX_FULLBKP_AGE={max_full_backup_age}
DUPL_PARAMS='$DUPL_PARAMS --full-if-older-than $MAX_FULLBKP_AGE '
""".format(
            max_full_backups=self.max_full_backups,
            max_full_backup_age=self.max_full_backup_age)
        # Volume size
        config += """
VOLSIZE={0}
DUPL_PARAMS='$DUPL_PARAMS --volsize $VOLSIZE '
""".format(self.volsize)

        # Extra arguments
        config += \
            "DUPL_PARAMS='$DUPL_PARAMS "\
            "{s3_european_bucket}"\
            "{s3_use_new_style}'\n".format(
                s3_european_bucket=
                '--s3-european-buckets ' if self.s3_european_bucket else '',
                s3_use_new_style=
                '--s3-use-new-style ' if self.s3_use_new_style else '',
            )
        return config

    def compile_exclude(self):
        volumes = self.webapp.docker_container.get_meta_config('Volumes')
        config = ''
        # Include extra backup directory
        config += '+ {0}\n'.format(self.get_extra_backup_directory())
        # Include volumes
        for k in volumes.keys():
            # Exclude folder with logs from backups
            if k != '/var/log':
                config += '+ {volume}\n'.format(volume=volumes[k])
        # Exclude everything else
        config += '- **\n'
        return config
