# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backups', '0003_auto_20151112_1037'),
    ]

    operations = [
        migrations.AddField(
            model_name='backupsettings',
            name='max_full_backup_age',
            field=models.CharField(help_text='forces a full backup if last full backup reaches a specified age', verbose_name='Max full backup age', choices=[('1W', '1 Week'), ('2W', '2 Weeks'), ('3W', '3 Weeks'), ('1M', '1 Month'), ('2M', '2 Months'), ('3M', '3 Months'), ('6M', '6 Months'), ('1Y', '1 Year'), ('2Y', '2 Years')], default='1M', max_length=20),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='max_full_backups',
            field=models.IntegerField(help_text='Number of full backups to keep.', verbose_name='Max full backups', default=4),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='s3_european_bucket',
            field=models.BooleanField(verbose_name='S3 european bucket', default=True),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='s3_use_new_style',
            field=models.BooleanField(verbose_name='S3 new style auth', default=True),
        ),
        migrations.AlterField(
            model_name='backupsettings',
            name='max_age',
            field=models.CharField(help_text='Time frame for old backups to keep.', verbose_name='Max age', choices=[('1W', '1 Week'), ('2W', '2 Weeks'), ('3W', '3 Weeks'), ('1M', '1 Month'), ('2M', '2 Months'), ('3M', '3 Months'), ('6M', '6 Months'), ('1Y', '1 Year'), ('2Y', '2 Years')], default='2M', max_length=20),
        ),
    ]
