# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('backups', '0004_auto_20151112_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='backupsettings',
            name='duply_status',
            field=models.TextField(default='', verbose_name='Duply status', blank=True),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='last_update',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 17, 16, 13, 41, 466467, tzinfo=utc), verbose_name='Last update', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='backupsettings',
            name='max_full_backup_age',
            field=models.CharField(default='1M', choices=[('1W', '1 Week'), ('2W', '2 Weeks'), ('3W', '3 Weeks'), ('1M', '1 Month'), ('2M', '2 Months'), ('3M', '3 Months'), ('6M', '6 Months'), ('1Y', '1 Year'), ('2Y', '2 Years')], max_length=20, help_text='Forces a full backup if last full backup reaches a specified age.', verbose_name='Max full backup age'),
        ),
    ]
