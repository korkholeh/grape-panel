# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import plugins.backups.models


class Migration(migrations.Migration):

    dependencies = [
        ('backups', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='backupsettings',
            name='backup_db',
        ),
        migrations.RemoveField(
            model_name='backupsettings',
            name='backup_volumes',
        ),
        migrations.RemoveField(
            model_name='backupsettings',
            name='directory',
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='e_password',
            field=models.BinaryField(blank=True),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='passphrase',
            field=models.CharField(max_length=100, verbose_name='Encription passphrase', help_text='All your backups will be encripted with this passphrase.', default=plugins.backups.models.get_backup_passphrase),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='target',
            field=models.CharField(max_length=255, verbose_name='Target', help_text='Specify where you want to store your backups.', default='s3://s3-<region endpoint name>.amazonaws.com/<bucket name>/<folder name>'),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='username',
            field=models.CharField(max_length=100, verbose_name='Target user', default='test'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='backupsettings',
            name='schedule',
            field=models.CharField(max_length=50, verbose_name='Frequency', choices=[('never', 'Never'), ('daily', 'Daily'), ('weekly', 'Weekly')], default='never'),
        ),
    ]
