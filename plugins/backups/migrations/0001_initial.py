# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0004_webapplication_docker_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackupSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('directory', models.CharField(max_length=400, default='/home/webprod/backups', verbose_name='Directory')),
                ('backup_db', models.BooleanField(default=True, verbose_name='Backup DB')),
                ('backup_volumes', models.BooleanField(default=True, verbose_name='Backup volumes')),
                ('schedule', models.CharField(max_length=50, default='never', verbose_name='Frequency')),
                ('webapp', models.OneToOneField(to='applications.WebApplication', related_name='backup_settings', verbose_name='Web application')),
            ],
            options={
                'verbose_name_plural': 'Backup settings',
                'verbose_name': 'Backup settings',
            },
        ),
    ]
