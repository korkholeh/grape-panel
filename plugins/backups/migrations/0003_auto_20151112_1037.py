# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backups', '0002_auto_20151111_1842'),
    ]

    operations = [
        migrations.AddField(
            model_name='backupsettings',
            name='max_age',
            field=models.CharField(help_text='Time frame for old backups to keep.', default='1M', choices=[('1W', '1 Week'), ('2W', '2 Weeks'), ('3W', '3 Weeks'), ('1M', '1 Month'), ('2M', '2 Monthes'), ('3M', '3 Monthes'), ('6M', '6 Monthes'), ('1Y', '1 Year'), ('2Y', '2 Years')], max_length=20, verbose_name='Max age'),
        ),
        migrations.AddField(
            model_name='backupsettings',
            name='volsize',
            field=models.IntegerField(help_text='Volume size in MB', default=25, verbose_name='Volume size'),
        ),
        migrations.AlterField(
            model_name='backupsettings',
            name='schedule',
            field=models.CharField(default='never', choices=[('never', 'Never'), ('daily', 'Daily'), ('weekly', 'Weekly')], max_length=50, verbose_name='Schedule'),
        ),
    ]
