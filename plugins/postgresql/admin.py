# from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from .models import (
    PostgreSQLService,
    PostgreSQLUser,
    PostgreSQLDatabase,
)
from .forms import (
    PostgreSQLServiceAdminForm,
    PostgreSQLUserAdminForm,
)


class PostgreSQLServiceAdmin(admin.ModelAdmin):
    form = PostgreSQLServiceAdminForm
    list_display = ['name', 'host', 'active']

admin.site.register(PostgreSQLService, PostgreSQLServiceAdmin)


class PostgreSQLUserAdmin(admin.ModelAdmin):
    form = PostgreSQLUserAdminForm
    list_display = ['username', 'pg_service', 'webapp']

admin.site.register(PostgreSQLUser, PostgreSQLUserAdmin)


class PostgreSQLDatabaseAdmin(admin.ModelAdmin):
    list_display = ['name', 'username', 'pg_service', 'webapp']

admin.site.register(PostgreSQLDatabase, PostgreSQLDatabaseAdmin)
