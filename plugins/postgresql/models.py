import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
# from django.dispatch import receiver
# from django.db.models.signals import pre_delete
from apps.passwords.mixins import ModelWithPassword
from apps.applications.models import WebApplication


PG_PORT = 5432


class PostgreSQLService(ModelWithPassword):
    name = models.CharField(_('Name'), max_length=255)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_('Host'),
        related_name='postgresql')
    docker_container = models.ForeignKey(
        'containers.Container', verbose_name=_('Container'),
        related_name='postgresql',
        null=True, blank=True)
    active = models.BooleanField(_('Active'), default=False)
    docker_image = models.ForeignKey(
        'containers.DockerImage', verbose_name=_('Docker image'),
        null=True, blank=True)
    added = models.DateTimeField(_('Added'), auto_now_add=True)

    class Meta:
        verbose_name = _('PostgreSQL Service')
        verbose_name_plural = _('PostgreSQL Services')
        unique_together = ('name', 'host')

    def __str__(self):
        return '%s@%s' % (self.name, str(self.host))

    def get_public_pg_port(self):
        return self.docker_container.get_host_port(PG_PORT)

    def add_ssh_key(self, key_object):
        self.docker_container.add_ssh_key(key_object)

    def remove_ssh_key(self, key_object):
        self.docker_container.remove_ssh_key(key_object)

    def get_postgres_connection(self):
        conn = psycopg2.connect(
            database='postgres',
            user='postgres',
            password=self.password,
            host=self.host.ip_address,
            port=self.get_public_pg_port())
        return conn

    def add_user(self, username, password):
        conn = self.get_postgres_connection()
        cur = conn.cursor()
        cur.execute(
            'CREATE ROLE {0} WITH LOGIN;\n'
            'ALTER USER {0} PASSWORD \'{1}\';'.format(username, password))
        conn.commit()
        conn.close()

    def delete_user(self, username):
        conn = self.get_postgres_connection()
        cur = conn.cursor()
        cur.execute('DROP ROLE {0};'.format(username))
        conn.commit()
        conn.close()

    def create_db(self, database, username):
        conn = self.get_postgres_connection()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute(
            'CREATE DATABASE {0} OWNER {1} ENCODING \'UTF-8\';'.format(
                database, username))
        conn.commit()
        conn.close()

    def delete_db(self, database):
        conn = self.get_postgres_connection()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute('''SELECT pg_terminate_backend(pg_stat_activity.pid)
        FROM pg_stat_activity
        WHERE pg_stat_activity.datname = '{0}'
        AND pid <> pg_backend_pid();
        '''.format(database))
        cur.execute('DROP DATABASE {0};'.format(database))
        conn.commit()
        conn.close()


class PostgreSQLUser(ModelWithPassword):
    username = models.CharField(_('User name'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Owner'),
        related_name='pg_users')
    webapp = models.ForeignKey(
        WebApplication,
        verbose_name=_('Web application'),
        related_name='pg_users')
    pg_service = models.ForeignKey(
        PostgreSQLService,
        verbose_name=_('PostgreSQL Service'),
        related_name='pg_users')

    class Meta:
        verbose_name = _('PostgreSQL User')
        verbose_name_plural = _('PostgreSQL Users')
        unique_together = ('username', 'pg_service')

    def __str__(self):
        return u'{0} ({1})'.format(self.username, str(self.pg_service))


class PostgreSQLDatabase(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    username = models.CharField(_('User name'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Owner'),
        related_name='pg_databases')
    webapp = models.ForeignKey(
        WebApplication,
        verbose_name=_('Web application'),
        related_name='pg_databases')
    pg_service = models.ForeignKey(
        PostgreSQLService,
        verbose_name=_('PG Service'),
        related_name='pg_databases')

    class Meta:
        verbose_name = _('PostgreSQL Database')
        verbose_name_plural = _('PostgreSQL Databases')
        unique_together = ('name', 'pg_service')

    def __str__(self):
        return self.name
