# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postgresql', '0004_auto_20151009_0946'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='postgresqldatabase',
            unique_together=set([('name', 'pg_service')]),
        ),
    ]
