# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import apps.passwords.mixins


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('applications', '0004_webapplication_docker_image'),
        ('postgresql', '0003_auto_20151008_1059'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostgreSQLDatabase',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('username', models.CharField(max_length=100, verbose_name='User name')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='pg_databases', verbose_name='Owner')),
                ('pg_service', models.ForeignKey(to='postgresql.PostgreSQLService', related_name='pg_databases', verbose_name='PG Service')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', related_name='pg_databases', verbose_name='Web application')),
            ],
            options={
                'verbose_name_plural': 'PostgreSQL Databases',
                'verbose_name': 'PostgreSQL Database',
            },
        ),
        migrations.CreateModel(
            name='PostgreSQLUser',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('e_password', models.BinaryField(blank=True)),
                ('username', models.CharField(max_length=100, verbose_name='User name')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='pg_users', verbose_name='Owner')),
                ('pg_service', models.ForeignKey(to='postgresql.PostgreSQLService', related_name='pg_users', verbose_name='PostgreSQL Service')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', related_name='pg_users', verbose_name='Web application')),
            ],
            options={
                'verbose_name': 'PostgreSQL User',
                'verbose_name_plural': 'PostgreSQL Users',
            },
            bases=(apps.passwords.mixins.EncryptedMixin, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='postgresqluser',
            unique_together=set([('username', 'pg_service')]),
        ),
    ]
