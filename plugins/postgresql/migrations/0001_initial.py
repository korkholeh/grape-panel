# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0007_auto_20151007_0955'),
        ('containers', '0007_auto_20151007_0948'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostgreSQLService',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('active', models.BooleanField(default=False, verbose_name='Active')),
                ('docker_container', models.ForeignKey(null=True, to='containers.Container', blank=True, verbose_name='Container', related_name='postgresql')),
                ('host', models.ForeignKey(to='hosts.Host', verbose_name='Host', related_name='postgresql')),
            ],
            options={
                'verbose_name': 'PostgreSQL Service',
                'verbose_name_plural': 'PostgreSQL Services',
            },
        ),
    ]
