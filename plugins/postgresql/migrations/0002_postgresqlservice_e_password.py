# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postgresql', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='postgresqlservice',
            name='e_password',
            field=models.BinaryField(blank=True),
        ),
    ]
