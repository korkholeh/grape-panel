# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0007_auto_20151007_0948'),
        ('postgresql', '0002_postgresqlservice_e_password'),
    ]

    operations = [
        migrations.AddField(
            model_name='postgresqlservice',
            name='added',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 8, 8, 59, 2, 576320, tzinfo=utc), verbose_name='Added', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='postgresqlservice',
            name='docker_image',
            field=models.ForeignKey(verbose_name='Docker image', null=True, blank=True, to='containers.DockerImage'),
        ),
        migrations.AlterUniqueTogether(
            name='postgresqlservice',
            unique_together=set([('name', 'host')]),
        ),
    ]
