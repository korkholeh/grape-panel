from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.views.generic import CreateView, DeleteView

from apps.hosts.models import Host
from apps.sshkeys.views import SSHKeyRecordCreateView, SSHKeyRecordDeleteView
from .models import (
    PostgreSQLService,
    PostgreSQLUser,
    PostgreSQLDatabase,
)
from .forms import (
    AddPostgreSQLServiceForm,
    AddPostgreSQLUserForm,
    AddPostgreSQLDatabaseForm,
)


def plugin_dashboard(request, host_pk):
    host = get_object_or_404(Host, pk=host_pk)
    pg_services = PostgreSQLService.objects.filter(
        host=host,
        active=True,
    )
    return render(request, 'postgresql/dashboard.html', {
        'host': host,
        'pg_services': pg_services,
    })


def pg_detail(request, host_pk, pk):
    from apps.passwords.widgets import ShowHidePasswordWidget
    from apps.sshkeys.models import SSHKeyRecord
    host = get_object_or_404(Host, pk=host_pk)
    pg_service = get_object_or_404(
        PostgreSQLService,
        pk=pk,
        host=host,
    )

    # Get SSH keys
    from django.contrib.contenttypes.models import ContentType
    p_type = ContentType.objects.get_for_model(pg_service)
    sk_records = SSHKeyRecord.objects.filter(
        content_type=p_type,
        object_id=pg_service.pk)

    return render(request, 'postgresql/postgresql_detail.html', {
        'host': host,
        'p_object': pg_service,
        'pg_service': pg_service,
        'password_html': ShowHidePasswordWidget().render(
            'pwd', pg_service.password, {'id': 'id_pwd'}),
        'add_key_url': reverse(
            'postgresql_add_sshkey', args=(
                host.pk,
                pg_service.pk,
            )),
        'keys_records': sk_records,
        'remove_key_url_name': 'postgresql_remove_sshkey',
    })


class PostgreSQLServiceCreateView(CreateView):
    model = PostgreSQLService
    form_class = AddPostgreSQLServiceForm
    template_name = 'postgresql/postgresql_add.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        from apps.containers.models import DockerImagePullingLog
        pg_service = form.save(commit=False)
        docker_image = form.cleaned_data['docker_image']

        pg_service.host = self.host
        pg_service.password = form.cleaned_data['password']
        pg_service.save()
        self.pg_service = pg_service

        try:
            _pulling_log = DockerImagePullingLog.objects.get(
                docker_image=docker_image,
                host=pg_service.host)
            # print ('image already pulled')
            pg_init(pg_service)
            messages.success(
                self.request, _('PostgreSQL service is ready to use.'))
        except DockerImagePullingLog.DoesNotExist:
            # print ('image not pulled yet')
            docker_image.pull_to_host(pg_service.host)
            messages.success(
                self.request,
                _('PostgreSQL service setup process started, please wait.'))

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('postgresql_detail', args=(
            self.host.pk,
            self.pg_service.pk,))


def pg_init(pg_service):
    from apps.containers.models import Container
    host = pg_service.host
    docker_image = pg_service.docker_image
    try:
        container = Container.objects.get(
            name__exact=pg_service.name, host=host)
        if container.is_running:
            return 'ready'
        else:
            return 'processing'
    except Container.DoesNotExist:
        container = Container(
            name=pg_service.name,
            host=host,
            docker_image=docker_image,
            is_running=False,
        )
        container.save()
        container.create_and_start(
            env_variables={'PG_PASSWORD': pg_service.password})

        # Read Container instance from DB again because we need "meta"
        container = Container.objects.get(pk=container.pk)
        pg_service.docker_container = container
        pg_service.active = True
        pg_service.save()

        return 'start'


def pg_installation_log(request, pk):
    from apps.containers.models import DockerImagePullingLog
    pg_service = get_object_or_404(PostgreSQLService, pk=pk)
    host = pg_service.host
    docker_image = pg_service.docker_image
    pulling_log = DockerImagePullingLog.objects.get(
        host=host,
        docker_image=docker_image,
    )

    _init_result = 'none'
    _temporary_log = ''
    if pulling_log.finished:
        # Init container
        _init_result = pg_init(pg_service)
        # print (_init_result)
        if _init_result == 'ready':
            messages.success(
                request, _('PostgreSQL service is ready to use.'))
            return HttpResponse(
                '<script>window.location="%s";</script>' %
                reverse('postgresql_detail', args=(host.pk, pg_service.pk,)))
    else:
        _temporary_log = pulling_log.get_temporary_log()

    context = {
        'temporary_log': _temporary_log,
        'image_pulling_log': pulling_log,
        'container_init': _init_result,
    }
    return render(request, 'postgresql/_installation_log.html', context)


class PostgreSQLServiceDeleteView(DeleteView):
    model = PostgreSQLService

    def get_object(self):
        result = get_object_or_404(
            PostgreSQLService,
            host_id=self.kwargs.get('host_pk'),
            pk=self.kwargs.get('pk'))
        self.host = result.host
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'pg_service': self.get_object(),
            'volumes':
            self.get_object().docker_container
            .get_meta_config('Volumes').items(),
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        _container = self.object.docker_container
        # print (_container.id)
        do_remove_volumes = bool(request.POST.get('remove-volumes'))
        # print (do_remove_volumes)

        # Remove SSH key records
        from django.contrib.contenttypes.models import ContentType
        from apps.sshkeys.models import SSHKeyRecord

        c_type = ContentType.objects.get_for_model(self.object)
        # print (c_type)
        sshkey_records = SSHKeyRecord.objects.filter(
            content_type=c_type,
            object_id=self.object.id,
        )
        # print (sshkey_records)
        sshkey_records.delete()

        # Prepare volumes directories
        dir_list = list(_container.get_meta_config('Volumes').values())
        _parent_dirs = []
        for d in dir_list:
            _t = d.split('/')
            if len(_t) > 1 and _t[-1] == '_data':
                _parent_dirs.append('/'.join(_t[:-1]))
        dir_list += _parent_dirs

        host_id = _container.host_id

        # Remove container
        from apps.containers.utils import remove_docker_container
        remove_docker_container(
            container_id=_container.id,
            force_remove=True,
        )
        _container.delete()
        # Remove PostgreSQL service
        self.object.delete()

        if do_remove_volumes:
            # Remove linked volumes
            from apps.hosts.tasks import remove_directories_recursive
            remove_directories_recursive.delay(
                host_id=host_id,
                dir_list=dir_list,
            )

        messages.success(request, _('PostgreSQL server had been deleted.'))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('postgresql_index', args=(self.host.pk,))


class PGSSHKeyCreateView(SSHKeyRecordCreateView):
    template_name = 'postgresql/sshkey_form.html'
    context_parent_object_name = 'pg_service'
    success_url_name = 'postgresql_detail'

    def get_parent_object(self):
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        result = get_object_or_404(
            PostgreSQLService,
            host=self.host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        p_object = self.get_parent_object()
        return reverse(
            self.success_url_name,
            args=(p_object.host_id, p_object.id))


class PGSSHKeyDeleteView(SSHKeyRecordDeleteView):
    context_parent_object_name = 'pg_service'
    success_url_name = 'postgresql_detail'
    template_name = 'postgresql/sshkey_confirm_delete.html'

    def get_parent_object(self):
        result = get_object_or_404(
            PostgreSQLService,
            pk=self.kwargs.get('pk'))
        self.host = result.host
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        p_object = self.get_parent_object()
        return reverse(
            self.success_url_name,
            args=(p_object.host_id, p_object.id))


def databases_list(request, app_pk):
    from apps.applications.models import WebApplication
    from apps.passwords.widgets import ShowHidePasswordWidget
    app = get_object_or_404(WebApplication, pk=app_pk)
    pg_users = PostgreSQLUser.objects.filter(webapp=app)
    for pgu in pg_users:
        pgu.pwd_html = ShowHidePasswordWidget().render(
            'pwd_%s' % pgu.id,
            pgu.password, {'id': 'id_pwd_%s' % pgu.id})

    pg_databases = PostgreSQLDatabase.objects.filter(webapp=app)

    context = {
        'app': app,
        'pg_users': pg_users,
        'pg_databases': pg_databases,
    }
    return render(request, 'postgresql/db_list.html', context)


class PostgreSQLUserCreateView(CreateView):
    model = PostgreSQLUser
    form_class = AddPostgreSQLUserForm

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def form_valid(self, form):
        pg_user = form.save(commit=False)
        pg_user.owner = self.request.user
        pg_user.webapp = self.app
        pg_user.password = form.cleaned_data['password']
        pg_user.save()
        # Sync with DB server
        pg_user.pg_service.add_user(pg_user.username, pg_user.password)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def get_success_url(self):
        return reverse('postgresql_databases_list', args=(self.app.pk,))


class PostgreSQLUserDeleteView(DeleteView):
    model = PostgreSQLUser

    def get_object(self):
        from apps.applications.models import WebApplication
        app = get_object_or_404(WebApplication, pk=self.kwargs.get('app_pk'))
        self.app = app
        result = get_object_or_404(
            PostgreSQLUser,
            webapp=app,
            pk=self.kwargs.get('pguser_pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        # Delete user from DB server
        self.object.pg_service.delete_user(self.object.username)
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('postgresql_databases_list', args=(self.app.pk,))


class PostgreSQLDatabaseCreateView(CreateView):
    model = PostgreSQLDatabase
    form_class = AddPostgreSQLDatabaseForm

    def get_form_kwargs(self):
        from apps.applications.models import WebApplication
        kwargs = super().get_form_kwargs()
        self.app = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['app'] = self.app
        return kwargs

    def form_valid(self, form):
        pg_database = form.save(commit=False)

        pg_user = get_object_or_404(
            PostgreSQLUser,
            webapp=self.app,
            username=form.cleaned_data['pg_user'].username)

        pg_database.owner = self.request.user
        pg_database.pg_service = pg_user.pg_service
        pg_database.username = pg_user.username
        pg_database.webapp = self.app
        pg_database.save()
        # Sync with DB server
        pg_database.pg_service.create_db(
            pg_database.name,
            pg_database.username)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def get_success_url(self):
        return reverse('postgresql_databases_list', args=(self.app.pk,))


class PostgreSQLDatabaseDeleteView(DeleteView):
    model = PostgreSQLDatabase

    def get_object(self):
        from apps.applications.models import WebApplication
        app = get_object_or_404(WebApplication, pk=self.kwargs.get('app_pk'))
        self.app = app
        result = get_object_or_404(
            PostgreSQLDatabase,
            webapp=app,
            pk=self.kwargs.get('pgdb_pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'app': self.app,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        # Delete database from DB server
        self.object.pg_service.delete_db(self.object.name)
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('postgresql_databases_list', args=(self.app.pk,))


def pg_connection(request, app_pk, pgdb_pk):
    from apps.applications.models import WebApplication
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    pg_db = get_object_or_404(PostgreSQLDatabase, webapp=webapp, pk=pgdb_pk)
    postgresql_service = pg_db.pg_service
    pg_user = PostgreSQLUser.objects.get(
        username=pg_db.username,
        pg_service=postgresql_service)

    django_connection = """DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '%s',
        'USER': '%s',
        'PASSWORD': '%s',
        'HOST': '%s',
        'PORT': '%s',
    },
}
    """ % (
        pg_db.name,
        pg_db.username,
        pg_user.password,
        postgresql_service.host.ip_address,
        postgresql_service.get_public_pg_port(),
    )

    flask_connection = \
        "SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s'" % (
            pg_db.username,
            pg_user.password,
            postgresql_service.host.ip_address,
            postgresql_service.get_public_pg_port(),
            pg_db.name,
        )

    psql_connection = \
        'psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
            pg_db.name,
            postgresql_service.host.ip_address,
            pg_db.username,
            pg_user.password,
            postgresql_service.get_public_pg_port(),
        )

    return render(
        request,
        'postgresql/postgresql_connection.html',
        {
            'app': webapp,
            'postgresql_service': postgresql_service,
            'pg_db': pg_db,
            'django_connection': django_connection,
            'flask_connection': flask_connection,
            'psql_connection': psql_connection,
        })


def pg_dumps(request, app_pk, pgdb_pk):
    from apps.applications.models import WebApplication
    import datetime
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    pg_db = get_object_or_404(PostgreSQLDatabase, webapp=webapp, pk=pgdb_pk)
    postgresql_service = pg_db.pg_service
    pg_user = PostgreSQLUser.objects.get(
        # owner=user,
        username=pg_db.username, pg_service=postgresql_service)

    db_sql_dump = \
        'pg_dump "dbname=%s host=%s user=%s password=%s port=%s"' \
        ' > dump-%s.sql' % (
            pg_db.name,
            postgresql_service.host.ip_address,
            pg_db.username,
            pg_user.password,
            postgresql_service.get_public_pg_port(),
            datetime.date.today(),
        )

    db_restore_sql = \
        'cat db_dump.sql |' \
        ' psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
            pg_db.name,
            postgresql_service.host.ip_address,
            pg_db.username,
            pg_user.password,
            postgresql_service.get_public_pg_port(),
        )

    db_compressed_dump = \
        'pg_dump "dbname=%s host=%s user=%s password=%s port=%s"' \
        ' | gzip > dump-%s.gz' % (
            pg_db.name,
            postgresql_service.host.ip_address,
            pg_db.username,
            pg_user.password,
            postgresql_service.get_public_pg_port(),
            datetime.date.today(),
        )

    db_restore_compressed = \
        'gunzip -c db_dump.gz |' \
        ' psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
            pg_db.name,
            postgresql_service.host.ip_address,
            pg_db.username,
            pg_user.password,
            postgresql_service.get_public_pg_port(),
        )

    return render(
        request,
        'postgresql/postgresql_dumps.html',
        {
            'app': webapp,
            'postgresql_service': postgresql_service,
            'pg_db': pg_db,
            'db_sql_dump': db_sql_dump,
            'db_restore_sql': db_restore_sql,
            'db_compressed_dump': db_compressed_dump,
            'db_restore_compressed': db_restore_compressed,
        })
