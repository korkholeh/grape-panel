from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class PostgreSQLPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('PostgreSQL')

    def get_menu_info(self):
        host = self.context.get('host')
        app = self.context.get('app')
        if host:
            return (
                {
                    'menu': 'hosts', 'position': 20,
                    'label': _('PostgreSQL'),
                    'url': reverse('postgresql_index', args=(host.pk,)),
                    'itemname': 'postgres',
                },
            )
        # elif app:
        #     return (
        #         {
        #             'menu': 'apps', 'position': 10,
        #             'label': _('Domains'),
        #             'url': reverse('haproxy_domains_list', args=(app.pk,)),
        #             'itemname': 'domains',
        #         },
        #     )
        else:
            return tuple()

    def get_urls(self):
        from .views import (
            PostgreSQLServiceCreateView,
            PostgreSQLServiceDeleteView,
            PGSSHKeyCreateView,
            PGSSHKeyDeleteView,
        )

        return patterns(
            'plugins.postgresql.views',
            url(
                r'^postgresql/(?P<pk>\d+)/installation-log/$',
                'pg_installation_log',
                name='postgresql_installation_log'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/add-sshkey/$',
                PGSSHKeyCreateView.as_view(),
                name='postgresql_add_sshkey'),
            url(
                r'^postgresql/(?P<pk>\d+)/sshkeys/'
                r'(?P<sshkey_pk>\d+)/remove/$',
                PGSSHKeyDeleteView.as_view(),
                name='postgresql_remove_sshkey'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/remove/$',
                PostgreSQLServiceDeleteView.as_view(),
                name='postgresql_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/$',
                'pg_detail',
                name='postgresql_detail'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/add/$',
                PostgreSQLServiceCreateView.as_view(),
                name='postgresql_add'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/$', 'plugin_dashboard',
                name='postgresql_index'),
        )

    def get_supported_sections(self):
        return ['hosts', 'apps']
