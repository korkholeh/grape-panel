from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _
from apps.passwords.widgets import ShowHidePasswordWidget
from apps.containers.models import DockerImage
from .models import PostgreSQLService, PostgreSQLUser, PostgreSQLDatabase
from .settings import DOCKER_IMAGE_TAG, CONTAINER_NAME_TEMPLATE


class PostgreSQLServiceAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = PostgreSQLService
        fields = [
            'name', 'host',
            'docker_container',
            'docker_image',
            'active',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class AddPostgreSQLServiceForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = PostgreSQLService
        fields = ['name', 'password', 'docker_image']

    def __init__(self, host, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host
        self.fields['docker_image'].queryset = \
            DockerImage.objects.filter(
                container_type='service',
                tags__tag=DOCKER_IMAGE_TAG,
            )

        try:
            max_pg_id = \
                PostgreSQLService.objects.order_by('-id').first().id + 1
        except AttributeError:
            max_pg_id = 1
        self.fields['name'].initial = \
            CONTAINER_NAME_TEMPLATE.format(max_pg_id)


class PostgreSQLUserAdminForm(forms.ModelForm):
    password = forms.CharField(
        widget=ShowHidePasswordWidget())

    class Meta:
        model = PostgreSQLUser
        fields = ['username', 'owner', 'webapp', 'pg_service']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'] is not None:
                self.fields['password'].initial = kwargs['instance'].password

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.password = self.cleaned_data['password']
        if commit:
            instance.save()
        return instance


class AddPostgreSQLUserForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput())

    class Meta:
        model = PostgreSQLUser
        fields = ['pg_service', 'username', 'password']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app


class AddPostgreSQLDatabaseForm(forms.ModelForm):
    pg_user = forms.ModelChoiceField(
        label=_('User name'), queryset=PostgreSQLUser.objects.none())

    class Meta:
        model = PostgreSQLDatabase
        fields = ['name']

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        self.fields['pg_user'].queryset = PostgreSQLUser.objects.filter(
            webapp=app,
        )
