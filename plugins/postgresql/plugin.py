from plugins.base.plugin import BasicPlugin
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy, reverse


class PostgreSQLPlugin(BasicPlugin):
    def __init__(self, **kwargs):
        self.request = kwargs.get('request')
        self.context = kwargs.get('context')

    def get_name(self):
        return _('PostgreSQL')

    def get_menu_info(self):
        host = self.context.get('host')
        app = self.context.get('app')
        if host:
            return (
                {
                    'menu': 'hosts', 'position': 20,
                    'label': _('PostgreSQL'),
                    'url': reverse('postgresql_index', args=(host.pk,)),
                    'itemname': 'postgres',
                },
            )
        elif app:
            return (
                {
                    'menu': 'apps', 'position': 20,
                    'label': _('PostgreSQL'),
                    'url': reverse(
                        'postgresql_databases_list', args=(app.pk,)),
                    'itemname': 'postgres',
                },
            )
        else:
            return tuple()

    def get_urls(self):
        from .views import (
            PostgreSQLServiceCreateView,
            PostgreSQLServiceDeleteView,
            PGSSHKeyCreateView,
            PGSSHKeyDeleteView,
            PostgreSQLUserCreateView,
            PostgreSQLUserDeleteView,
            PostgreSQLDatabaseCreateView,
            PostgreSQLDatabaseDeleteView,
        )

        return patterns(
            'plugins.postgresql.views',
            url(
                r'^postgresql/(?P<pk>\d+)/installation-log/$',
                'pg_installation_log',
                name='postgresql_installation_log'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/add-sshkey/$',
                PGSSHKeyCreateView.as_view(),
                name='postgresql_add_sshkey'),
            url(
                r'^postgresql/(?P<pk>\d+)/sshkeys/'
                r'(?P<sshkey_pk>\d+)/remove/$',
                PGSSHKeyDeleteView.as_view(),
                name='postgresql_remove_sshkey'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/remove/$',
                PostgreSQLServiceDeleteView.as_view(),
                name='postgresql_delete'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/(?P<pk>\d+)/$',
                'pg_detail',
                name='postgresql_detail'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/add/$',
                PostgreSQLServiceCreateView.as_view(),
                name='postgresql_add'),
            url(
                r'^hosts/(?P<host_pk>\d+)/postgresql/$', 'plugin_dashboard',
                name='postgresql_index'),
            url(
                r'^applications/(?P<app_pk>\d+)/postgresql/$',
                'databases_list',
                name='postgresql_databases_list'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgusers/'
                r'(?P<pguser_pk>\d+)/remove/$',
                PostgreSQLUserDeleteView.as_view(),
                name='postgresql_pguser_delete'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgusers/add/$',
                PostgreSQLUserCreateView.as_view(),
                name='postgresql_pguser_add'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgdatabases/'
                r'(?P<pgdb_pk>\d+)/connection/$',
                'pg_connection',
                name='postgresql_pgdatabase_connection'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgdatabases/'
                r'(?P<pgdb_pk>\d+)/dumps/$',
                'pg_dumps',
                name='postgresql_pgdatabase_dumps'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgdatabases/'
                r'(?P<pgdb_pk>\d+)/remove/$',
                PostgreSQLDatabaseDeleteView.as_view(),
                name='postgresql_pgdatabase_delete'),
            url(
                r'^applications/(?P<app_pk>\d+)/pgdatabases/add/$',
                PostgreSQLDatabaseCreateView.as_view(),
                name='postgresql_pgdatabase_add'),
        )

    def get_supported_sections(self):
        return ['hosts', 'apps']
