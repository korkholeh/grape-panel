#!/bin/bash

# Use Python 3.4
alias python='/usr/bin/python3.4'
export PYTHONPATH=/usr/lib/python3.4

# Install git for version control, pip for install python packages
echo 'Installing git and python3-pip...'
sudo apt-get -qq install git wget psmisc mc daemontools curl lynx python3-pip > /dev/null
sudo apt-get -qq install libxml2-dev apache2-utils

# Install virtualenv / virtualenvwrapper
echo 'Installing virtualenv and virtualenvwrapper...'
alias python='/usr/bin/python3.4'
pip3 install --quiet virtualenv
pip3 install --quiet virtualenvwrapper
mkdir ~vagrant/.virtualenvs
chown vagrant:vagrant ~vagrant/.virtualenvs
printf "\n\n# Virtualenv settings\n" >> ~vagrant/.bashrc
printf "export PYTHONPATH=/usr/lib/python3.4" >> ~vagrant/.bashrc
printf "export WORKON_HOME=~vagrant/.virtualenvs\n" >> ~vagrant/.bashrc
printf "export PROJECT_HOME=/vagrant\n" >> ~vagrant/.bashrc
printf "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.4\n" >> ~vagrant/.bashrc
printf "source /usr/local/bin/virtualenvwrapper.sh\n" >> ~vagrant/.bashrc

# Install Redis
sudo add-apt-repository ppa:chris-lea/redis-server
sudo apt-get update
sudo apt-get -qq install redis-server

# Complete
echo ""
echo "Vagrant install complete."
echo "Now try logging in:"
echo "    $ vagrant ssh"