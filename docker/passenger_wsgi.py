import os
import sys

# Change the interpreter
INTERP = "/home/app/.virtualenvs/env_py34/bin/python3"
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)

sys.path = [
    '/home/app/webapp',
    '/home/app/webapp/grpanel',
] + sys.path

import django
import django.core.handlers.wsgi
from distutils.version import LooseVersion

if LooseVersion(django.get_version()) >= LooseVersion('1.7'):
    django.setup()

application = django.core.handlers.wsgi.WSGIHandler()
