# Some initial preparations
source /etc/profile.d/webapp.sh

export PYTHONPATH=/usr/lib/python3.4
export WORKON_HOME=/home/app/.virtualenvs
export PROJECT_HOME=/home/app/webapp/grpanel
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.4
source /usr/local/bin/virtualenvwrapper.sh

su -l app
workon env_py34
cd /home/app/webapp/grpanel
celery --app=grpanel.celery:app worker --beat --loglevel=INFO
