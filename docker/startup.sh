#!/bin/bash
cd /home/app
setfacl -R -m d:u:app:rwx,u:app:rwx webapp
setfacl -R -m d:u:www-data:rwx,u:www-data:rwx webapp
setfacl -R -m d:u:root:rwx,u:root:rwx webapp
